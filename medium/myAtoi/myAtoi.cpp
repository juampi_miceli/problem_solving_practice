#include <bits/stdc++.h>

const std::string MAX_POSITIVE = "2147483647";
const std::string MIN_NEGATIVE = "2147483648";

void skipCustomChar(const std::string &s, char c, size_t &pointer) {
    size_t N = s.size();
    while (pointer < N && s[pointer] == c) {
        pointer++;
    }
}

int getNumberOfDigits(const std::string &s, size_t pointer) {
    size_t N = s.size();
    int res = 0;
    while (pointer < N && isdigit(s[pointer])) {
        res++;
        pointer++;
    }
    return res;
}

void clampString(std::string &s, int sign, const std::string &min, const std::string &max) {
    if (s.size() < max.size()) return;
    if (s.size() > max.size()) {
        if (sign == 1) {
            s = max;
        } else {
            s = min;
        }
        return;
    }
    if (sign == 1 && s >= max) {
        s = max;
    } else if (sign == -1 && s >= min) {
        s = min;
    }
}

int myAtoi(std::string s) {
    if (s.empty()) return 0;

    size_t N = s.size();
    size_t i = 0;
    int sign = 1;

    skipCustomChar(s, ' ', i);

    if (i == N) {
        return 0;
    }
    if (s[i] == '-' || s[i] == '+') {
        sign = s[i] == '-' ? -1 : 1;
        i++;
    }

    skipCustomChar(s, '0', i);

    if (!isdigit(s[i])) {
        return 0;
    }

    int numberDigits = getNumberOfDigits(s, i);
    std::string numberString(s.begin() + i, s.begin() + i + numberDigits);
    clampString(numberString, sign, MIN_NEGATIVE, MAX_POSITIVE);
    numberDigits = numberString.size();

    int resultingNumber = 0;

    for (i = 0; i < numberDigits; i++) {
        int pot = std::pow(10, numberDigits - i - 1);
        resultingNumber += (numberString[i] - '0') * (pot)*sign;
    }

    return resultingNumber;
}

int main() {
    printf("%d", myAtoi("00000-42a1234"));
    return 0;
}