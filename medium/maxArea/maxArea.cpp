#include <stdlib.h>
#include <stdio.h>
#include <vector>

int getWater(const std::vector<int> &v, int L, int R)
{
    return (R - L) * std::min(v[L], v[R]);
}

int maxArea(std::vector<int> &height)
{
    int N = height.size();
    int L = 0;
    int R = N - 1;

    int winner = 0;

    while (L < R)
    {
        winner = std::max(winner, getWater(height, L, R));
        if (height[L] <= height[R])
        {
            L++;
        }
        else
        {
            R--;
        }
    }
    return winner;
}

int main()
{
    std::vector<int> height = {4, 3, 2, 1, 4};

    maxArea(height);
    return 0;
}
