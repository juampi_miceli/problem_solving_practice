#include <stdlib.h>
#include <stdio.h>

typedef struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
} ListNode;

ListNode *addTwoNumbers(ListNode *l1, ListNode *l2)
{
    ListNode *root = new ListNode((l1->val + l2->val) % 10);
    ListNode *current = root;
    int carry = (l1->val + l2->val) >= 10 ? 1 : 0;

    l1 = l1->next;
    l2 = l2->next;

    while (l1 != nullptr || l2 != nullptr)
    {
        //Get value of each node
        int val1 = l1 != nullptr ? l1->val : 0;
        int val2 = l2 != nullptr ? l2->val : 0;

        int sum = val1 + val2 + carry;

        //Do i need to keep carrying?
        carry = sum >= 10 ? 1 : 0;

        current->next = new ListNode(sum % 10);
        current = current->next;

        l1 = l1 != nullptr ? l1->next : nullptr;
        l2 = l2 != nullptr ? l2->next : nullptr;
    }

    if (carry == 1)
    {
        current->next = new ListNode(1);
    }

    return root;
}

int main()
{
    ListNode *l1 = new ListNode(2, new ListNode(4, new ListNode(3)));
    ListNode *l2 = new ListNode(5, new ListNode(6, new ListNode(4)));

    addTwoNumbers(l1, l2);
    return 0;
}