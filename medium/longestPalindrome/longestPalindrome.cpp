#include <stdlib.h>
#include <stdio.h>
#include <string>

int expandFromCenter(std::string s, int i, int j)
{
    while (i >= 0 && j < s.size() && s[i] == s[j])
    {
        i--;
        j++;
    }

    return j - i - 1;
}

std::string longestPalindrome(std::string s)
{
    int longest = 0;
    int left = 0;

    if (s.size() <= 2 && s[0] == s[s.size() - 1])
    {
        return s;
    }

    int N = s.size();

    for (int i = 0; i < N; i++)
    {

        int len1 = expandFromCenter(s, i, i);
        int len2 = expandFromCenter(s, i, i + 1);
        int len = std::max(len1, len2);

        if (len > longest)
        {
            left = i - ((len - 1) / 2);
            longest = len;
        }
    }

    return s.substr(left, longest);
}

int main()
{
    std::string s = "babac";

    longestPalindrome(s);

    return 0;
}