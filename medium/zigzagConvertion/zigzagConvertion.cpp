#include <stdlib.h>
#include <stdio.h>
#include <string>

std::string convert(std::string s, int numRows)
{
    if (numRows == 1)
        return s;

    int luckyNumber = (numRows - 1) * 2;

    std::string res = "";
    int i = 0;
    while (i < numRows)
    {
        int j = 0;
        while (luckyNumber * (j - 1) < (int)s.size())
        {
            if ((luckyNumber * j) - i >= 0 && (luckyNumber * j) - i < (int)s.size())
            {
                if (i < luckyNumber / 2)
                {
                    res += s[(luckyNumber * j) - i];
                }
            }
            if ((luckyNumber * j) + i >= 0 && (luckyNumber * j) + i < (int)s.size())
            {
                if (i > 0)
                {
                    res += s[(luckyNumber * j) + i];
                }
            }
            j++;
        }
        i++;
    }
    return res;
}

int main()
{
    std::string s = "0123456789ABC";
    convert(s, 4);
    return 0;
}