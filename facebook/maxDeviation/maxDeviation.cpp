#include <bits/stdc++.h>

int solution(int A[], int N);
long getMean(int A[], int N);

long longAbs(long x);

int solution(int A[], int N) {
    int winnerIndex = -1;
    long winnerValue = -1;

    if (N == 0) return -1;

    long mean = getMean(A, N);

    for (int i = 0; i < N; i++) {
        long deviation = longAbs((long)(A[i]) - mean);
        if (deviation > winnerValue) {
            winnerValue = deviation;
            winnerIndex = i;
        }
    }

    return winnerIndex;
}

long longAbs(long x) {
    if (x < 0) {
        return -x;
    }
    return x;
}

long getMean(int A[], int N) {
    long auxSum = 0;

    for (int i = 0; i < N; i++) {
        auxSum += A[i];
    }

    auxSum = auxSum / N;

    return auxSum;
}

void test() {
    int A[5] = {2, 1, 3, 5, 4};
    assert(solution(A, 5) == 1);
    int B[4] = {9, 4, -3, -10};
    assert(solution(B, 4) == 3);
    int C[0] = {};
    assert(solution(C, 0) == -1);
    int D[6] = {2147483647, -2147483648, 2147483646, -2147483645, 2147483645, -2147483645};
    assert(solution(D, 6) == 1);
}

int main() {
    test();
    return 0;
}
