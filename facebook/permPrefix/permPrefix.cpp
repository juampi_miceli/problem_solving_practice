#include <bits/stdc++.h>

int solution(int A[], int N) {
    int totalWinners = 0;
    int maxVal = -1;
    for (int i = 0; i < N; i++) {
        maxVal = std::max(maxVal, A[i]);
        if (maxVal <= i + 1) {
            totalWinners++;
        }
    }

    return totalWinners;
}

void test() {
    int A[5] = {2, 1, 3, 5, 4};
    assert(solution(A, 5) == 3);
    int B[3] = {1, 2, 3};
    assert(solution(B, 3) == 3);
    int C[3] = {3, 2, 1};
    assert(solution(C, 3) == 1);
    int D[5] = {2, 1, 3, 4, 5};
    assert(solution(D, 5) == 4);
}

int main() {
    test();
    std::cout << INT_MAX << '\n';
    return 0;
}