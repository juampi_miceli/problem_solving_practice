#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <queue>
#include <vector>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:

    typedef std::pair<int, int> Par;

    ListNode* mergeKLists(std::vector<ListNode*>& lists) {
        std::priority_queue<Par, std::vector<Par>, std::greater<Par>> minHeap;

        for(int i = 0; i < lists.size(); i++){
            if(lists[i] == nullptr){
                continue;
            }
            minHeap.push(Par(lists[i]->val, i));
            lists[i] = lists[i]->next;
        }

        if(minHeap.empty()){
            return nullptr;
        }

        ListNode *mergedList = new ListNode();
        ListNode *current = mergedList;

        while(!minHeap.empty()){
            Par par = minHeap.top();
            minHeap.pop();
            current->val = par.first;

            if(lists[par.second] != nullptr){
                minHeap.push(Par(lists[par.second]->val, par.second));
                lists[par.second] = lists[par.second]->next; 
            }

            if(!minHeap.empty()){
                current->next = new ListNode();
                current = current->next;
            }
        }

        return mergedList;
    }

};