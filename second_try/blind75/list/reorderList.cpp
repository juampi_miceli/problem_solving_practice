#include <stdlib.h>
#include <stdio.h>
#include <unordered_map>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    
    ListNode* fillPrevious(ListNode *head, std::unordered_map<ListNode *, ListNode *> &previousMap, ListNode *previousNode){
        if(head == nullptr){
            return previousNode;
        }
        previousMap[head] = previousNode;
        return fillPrevious(head->next, previousMap, head);
        
    }
    
    void reorderList(ListNode* head) {
        std::unordered_map<ListNode *, ListNode *> previousMap;
        
        ListNode *last = fillPrevious(head, previousMap, nullptr);
        
        
        while(head != nullptr && head->next != nullptr && head->next != last){
            ListNode *auxNext = head->next;
            head->next = last;
            last->next = auxNext; 
            ListNode *prev = previousMap[last];
            prev->next = nullptr;
            head = auxNext;
            last = prev;
        }
        
    }
};