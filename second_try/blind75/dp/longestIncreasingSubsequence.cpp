#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    /**
     * Recursive solution O(2^n)
     * return lengthOfLIS(nums, 0, 0, INT_MIN);
     */
    int lengthOfLIS(std::vector<int>& nums, int currentLength, int index, int maxElem){
        if(index >= nums.size()){
            return currentLength;
        }
        int lengthWithSelf = lengthOfLIS(nums, currentLength + (nums[index] > maxElem), index+1, std::max(nums[index], maxElem));
        int lengthWithoutSelf = lengthOfLIS(nums, currentLength, index+1, maxElem);
        return std::max(lengthWithSelf, lengthWithoutSelf);
    }
    
    /**
     * DP solution O(n^2)
     */
    int maxElem(std::vector<int>& nums){
        int max = nums[0];
        for(int e : nums){
            max = std::max(max, e);
        }
        return max;
    }
    
    int lengthOfLIS(std::vector<int>& nums) {
        std::vector<int> lengths(nums.size(), 1);
        for(int i = nums.size() - 2; i >= 0; i--){
            for(int j = i + 1; j < nums.size(); j++){
                if(nums[i] < nums[j]){
                    lengths[i] = std::max(lengths[i], lengths[j] + 1);
                }
            }
            
        }
        return maxElem(lengths);
    }
};