#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int coinChange(std::vector<int>& coins, int amount) {
        std::vector<int> amounts(amount+1, -1);
        amounts[0] = 0;
        
        for(int i = 1; i <= amount; i++){
            for(int c : coins){
                if(i >= c){
                    if(amounts[i-c] != -1){
                        amounts[i] = amounts[i] == -1 ? amounts[i-c] + 1 : std::min(amounts[i], amounts[i-c] + 1);
                    }
                }
            }
        }
        
        return amounts[amount];
    }
};