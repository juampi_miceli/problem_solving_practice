#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    void transpose(std::vector<std::vector<int>> &matrix){
        for(int i = 0; i < matrix.size(); i++){
            for(int j = i+1; j < matrix[0].size(); j++){
                std::swap(matrix[i][j], matrix[j][i]);
            }
        }
    }
    
    void verticalMirror(std::vector<std::vector<int>> &matrix){
        for(int i = 0; i < matrix.size(); i++){
            for(int j = 0; j < matrix[0].size()/2; j++){
                std::swap(matrix[i][j], matrix[i][matrix[0].size()-1-j]);
            }
        }
    }
    void rotate(std::vector<std::vector<int>>& matrix) {
        transpose(matrix);
        verticalMirror(matrix);
    }
};