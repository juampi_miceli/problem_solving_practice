#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    void setZeroes(std::vector<std::vector<int>>& matrix) {
        bool cleanRow = false;
        bool cleanCol = false;

        for(int i = 0; i < matrix.size(); i++)
        {
            for(int j = 0; j < matrix[0].size(); j++){
                if(matrix[i][j] == 0){
                    if(i == 0){
                        cleanRow = true;
                    }
                    if(j == 0){
                        cleanCol = true;
                    }
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }    
        for(int i = 1; i < matrix.size(); i++){
            for(int j = 1; j < matrix[0].size(); j++){
                if(matrix[i][0] == 0 || matrix[0][j] == 0){
                    matrix[i][j] = 0;   
                }
            }
        }
        if(cleanCol){
            for(int i = 0; i < matrix.size(); i++){
                matrix[i][0] = 0;
            }
        }
        if(cleanRow){
            for(int j = 0; j < matrix[0].size(); j++){
                matrix[0][j] = 0;
            }
        }
    }
};