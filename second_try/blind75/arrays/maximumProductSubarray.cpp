#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    int maximum(std::vector<int>& nums){
        int winner = nums[0];
        
        for(int e : nums){
            winner = std::max(winner, e);
        }
        
        return winner;
    }
    
    int maxProduct(std::vector<int>& nums) {
        int winner = maximum(nums);
        int max = 1;
        int min = 1;
        int N = nums.size();
        for(int i = 0; i < N; i++){
            if(nums[i] == 0){
                min = 1;
                max = 1;
                continue;
            }
            int maxBuffer = max;
            max = std::max(nums[i], std::max(nums[i] * max, nums[i] * min));
            min = std::min(nums[i], std::min(nums[i] * maxBuffer, nums[i] * min));
            winner = std::max(winner, std::max(min, max));
        }
        
        return winner;
    }
};