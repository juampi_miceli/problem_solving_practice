#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <unordered_map>

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        std::unordered_map<int, int> leftUntilTarget;
        
        for(int i = 0; i < nums.size(); i++){
            if(leftUntilTarget.count(nums[i])){
                return {i, leftUntilTarget[nums[i]]};
            }else{
                leftUntilTarget[target-nums[i]] = i;
            }
        }
        return {};
    }
};