#include <stdlib.h>
#include <stdio.h>
#include <vector>

/**
 * O(n) space & time
 */
class Solution {
public:
    std::vector<int> productExceptSelf(std::vector<int>& nums) {
        int N = nums.size();
        std::vector<int> res(N, 0);
        
        std::vector<int> leftProducts(N, 1);
        std::vector<int> rightProducts(N, 1);
        
        leftProducts[0] = nums[0];
        for(int i = 1; i < N - 1; i++){
            leftProducts[i] = leftProducts[i-1] * nums[i];
        }
        
        rightProducts[N - 1] = nums[N - 1];
        for(int i = N - 2; i > 0; i--){
            rightProducts[i] = rightProducts[i+1] * nums[i];
        }
        
        res[0] = rightProducts[1];
        for(int i = 1; i < N - 1; i++){
            res[i] = leftProducts[i-1] * rightProducts[i+1]; 
        }
        res[N-1] = leftProducts[N-2];
        
        return res;
    }
};

/**
 * O(n) time, O(1) space
 */

class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int N = nums.size();
        std::vector<int> res(N, 1);
        
        for(int i = 1; i < N; i++){
            res[i] = res[i-1] * nums[i-1];
        }
        
        int acc = nums[N-1];
        
        for(int i = N - 2; i >= 0; i--){
            res[i] *= acc;
            acc *= nums[i];
        }
        
        return res;
    }
};