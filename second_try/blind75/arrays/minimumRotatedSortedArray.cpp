#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int findMin(std::vector<int>& nums) {
        if(nums[0] <= nums.back()){
            return nums[0];
        }
        int left = 0;
        int right = nums.size()-1;
        int middle = (left + right) / 2;
        
        while(right - left > 1){
            if(nums[left] > nums[middle]){
                right = middle;
            }else{
                left = middle;
            }
            
            middle = (left + right) / 2;
        }
        
        return nums[right];
    }
};