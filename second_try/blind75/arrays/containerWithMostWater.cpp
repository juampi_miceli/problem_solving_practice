#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    int area(std::vector<int>& heights, int left, int right){
        return std::min(heights[left], heights[right]) * (right-left);
    }
    int maxArea(std::vector<int>& heights) {
        int left = 0;
        int right = heights.size() - 1;
        
        int winner = 0;
        
        while(left < right){
            winner = std::max(winner, area(heights, left, right));
            if(heights[left] >= heights[right]){
                right--;
            }else{
                left++;
            }
        }
        return winner;
        
    }
};