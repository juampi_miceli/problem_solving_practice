class Solution {
public:
    
    std::vector<std::vector<int>> sortedTwoSum(int targetPosition, std::vector<int> &nums){
        std::vector<std::vector<int>> triplets;
        int goal = nums[targetPosition] * -1;
        
        int pLeft = targetPosition + 1;
        int pRight = nums.size()-1;
        
        while(pLeft < pRight){
            int sum = nums[pLeft] + nums[pRight];
            
            if(sum == goal){
                triplets.push_back({-goal, nums[pLeft], nums[pRight]});
                int currentLeft = nums[pLeft];
                while(pLeft < nums.size() && nums[pLeft] == currentLeft){
                    pLeft++;
                }
                int currentRight = nums[pRight];
                while(pRight >= 0 && nums[pRight] == currentRight){
                    pRight--;
                }
            }else if(sum < goal){
                pLeft++;
            }else{
                pRight--;
            }
        }
        return triplets;
    }
    
    void addAll(std::vector<std::vector<int>> &triplets, std::vector<std::vector<int>> &newTriplets){
        for(std::vector<int> &newTriplet : newTriplets){
            triplets.push_back(newTriplet);
        }
    }
    
    std::vector<std::vector<int>> threeSum(std::vector<int>& nums) {
        std::sort(nums.begin(), nums.end());
        std::vector<std::vector<int>> triplets;
        
        int i = 0;
        
        while(i < nums.size()){
            std::vector<std::vector<int>> newTriplets = sortedTwoSum(i, nums);
            addAll(triplets, newTriplets);
            int currentElem = nums[i]; 
            while(i < nums.size() && currentElem == nums[i]){
                i++;
            }
        }
        return triplets;
    }
};