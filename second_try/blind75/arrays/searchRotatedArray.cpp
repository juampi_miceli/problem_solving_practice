#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int search(std::vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;
        int middle = (left + right) / 2;
        
        while(right - left > 1){
            if(nums[left] <= nums[middle]){
                // Left side
                if(nums[left] <= target && target <= nums[middle]){
                    right = middle;
                }else{
                    left = middle;
                }
            }else{
                // Right side
                if(nums[middle] <= target && target <= nums[right]){
                    left = middle;
                }else{
                    right = middle;
                }
                
            }
            middle = (left + right) / 2;
        }
        
        if(nums[left] == target){
            return left;
        }
        if(nums[right] == target){
            return right;
        }
        return -1;
    }
};