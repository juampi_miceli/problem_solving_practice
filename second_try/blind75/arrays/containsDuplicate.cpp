#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <unordered_set>

class Solution {
public:
    bool containsDuplicate(std::vector<int>& nums) {
        std::unordered_set<int> elems;
        
        for(int e : nums){
            if(elems.count(e)){
                return true;
            }
            elems.insert(e);
        }
        return false;
    }
};