#include <stdlib.h>
#include <stdio.h>
#include <vector>

/**
 * DP
 */

class Solution {
public:
    int maxSubArray(std::vector<int>& nums) {
        int acc = nums[0];
        int winner = nums[0];
        
        for(int i = 1; i < nums.size(); i++){
            acc = std::max(acc + nums[i], nums[i]);
            winner = std::max(acc, winner);
        }
        return winner;
    }
};

/**
 * Divide & Conquer
 */

class Solution {
public:
    
    int conquer(std::vector<int>& left, std::vector<int>& right){
        int acc = right[0];
        int winnerRight = acc;
        
        for(int i = 1; i < right.size(); i++){
            acc += right[i];
            winnerRight = std::max(acc, winnerRight);
        }
        
        acc = left[left.size()-1];
        int winnerLeft = acc;
        
        for(int i = left.size() - 2; i >= 0; i--){
            acc += left[i];
            winnerLeft = std::max(acc, winnerLeft);
        }
        
        return winnerRight + winnerLeft;
        
    }
    
    int maxSubArray(std::vector<int>& nums) {
        if(nums.size() <= 1){
            return nums[0];
        }
        
        int half = nums.size() / 2;
        std::vector<int> leftNums = std::vector<int>(nums.begin(), nums.begin() + half);
        std::vector<int> rightNums = std::vector<int>(nums.begin() + half, nums.end());
        
        int winnerLeft = maxSubArray(leftNums);
        int winnerRight = maxSubArray(rightNums);
        
        return std::max(conquer(leftNums, rightNums), std::max(winnerLeft, winnerRight));
    }
};