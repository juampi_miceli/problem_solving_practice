#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int maxProfit(std::vector<int>& prices) {
        int winner = 0;
        
        int buyPrice = prices[0];
        
        for(int i = 0; i < prices.size(); i++){
            winner = std::max(winner, prices[i] - buyPrice);
            buyPrice = std::min(buyPrice, prices[i]);
        }
        return winner;
    }
};