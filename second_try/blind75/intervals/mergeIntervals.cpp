#include <stdlib.h>
#include <stdio.h>
#include <vector>


class Solution {
public:
    std::vector<std::vector<int>> merge(std::vector<std::vector<int>>& intervals) {
        
        std::sort(intervals.begin(), intervals.end());
        std::vector<std::vector<int>> newIntervals;
        int i = 0;

        while(i < intervals.size()){
            std::vector<int> newInterval = intervals[i];
            while(i < intervals.size() && newInterval[1] >= intervals[i][0] && newInterval[0] <= intervals[i][1]){
                newInterval[0] = std::min(newInterval[0], intervals[i][0]);
                newInterval[1] = std::max(newInterval[1], intervals[i][1]);
                i++;
            }
            newIntervals.push_back(newInterval);
        }

        return newIntervals;
    }
};