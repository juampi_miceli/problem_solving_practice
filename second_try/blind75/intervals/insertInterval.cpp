#include <stdlib.h>
#include <stdio.h>
#include <vector>


class Solution {
public:
    std::vector<std::vector<int>> insert(std::vector<std::vector<int>>& intervals, std::vector<int>& newInterval) {
        std::vector<std::vector<int>> newIntervals;

        int i = 0;
        while(i < intervals.size() && intervals[i][1] < newInterval[0]){
            newIntervals.push_back(intervals[i]);
            i++;
        }
        while(i < intervals.size() && (newInterval[0] <= intervals[i][1] && newInterval[1] >= intervals[i][0])){
            newInterval[0] = std::min(newInterval[0], intervals[i][0]);
            newInterval[1] = std::max(newInterval[1], intervals[i][1]);
            i++;
        }
        newIntervals.push_back(newInterval);

        while(i < intervals.size()){
            newIntervals.push_back(intervals[i]);
            i++;
        }

        return newIntervals;
        
    }
};