#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int eraseOverlapIntervals(std::vector<std::vector<int>>& intervals) {
        std::sort(intervals.begin(), intervals.end());
        
        int nonOverlapping = 1;
        
        std::vector<int> lastToStart = intervals.back();
        int i = intervals.size() - 2;
        while(i >= 0){
            if(intervals[i][1] <= lastToStart[0]){
                nonOverlapping++;
                lastToStart = intervals[i];
            }
            i--;
        }
        return intervals.size() - nonOverlapping;
    }
};