#include <stdlib.h>
#include <stdio.h>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    bool LEFT = false;
    bool RIGHT = true;
    
    int inorder(TreeNode* root, int k, int current, int &result, bool direction){
        if(root == nullptr){
            if(current == 0){
                return 1;
            }
            if(direction == LEFT){
                return current;
            }
            return current - 1;
        }
        int lookLeft = inorder(root->left, k, current, result, LEFT);
        if(lookLeft == k){
            result = root->val;
        }
        int lookRight = inorder(root->right, k, lookLeft+1, result, RIGHT);
        
        if(direction == LEFT){
            return lookRight + 1;
        }
        return lookRight;
        
        
    }
    int kthSmallest(TreeNode* root, int k) {
        int res = -1;
        inorder(root, k, 0, res, LEFT);
        return res;
        
    }
};