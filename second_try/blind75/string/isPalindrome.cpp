#include <stdlib.h>
#include <stdio.h>
#include <string>

class Solution {
public:
    
    bool isAlphanumeric(char c){
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'); 
    }
    
    bool isPalindrome(std::string s) {
        int left = 0;
        int right = s.size() - 1;
        
        while(right > left){
            char leftChar = s[left];
            if(!isAlphanumeric(leftChar)){
                left++;
                continue;
            }
            char rightChar = s[right];
            if(!isAlphanumeric(rightChar)){
                right--;
                continue;
            }
            if(std::tolower(leftChar) == std::tolower(rightChar)){
                left++;
                right--;
                continue;
            }
            return false;
        }
        
        return true;
    }
};