#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>

class Solution {
public:
    
    bool isOpenBracket(char s){
        return s == '[' || s == '{' || s == '(';
    }
    
    bool isValidClosing(char open, char close){
        return (close == '}' && open == '{') || (close == ']' && open == '[') || (close == ')' && open == '(');
    }
    bool isValid(std::string str) {
        std::vector<char> openBrackets;
        
        for(int i = 0; i < str.size(); i++){
            char s = str[i];
            if(isOpenBracket(s)){
                openBrackets.push_back(s);
                continue;
            }
            if(!openBrackets.empty() && isValidClosing(openBrackets.back(), s)){
                openBrackets.pop_back();
                continue;
            }
            
            return false;
        }
        
        return openBrackets.empty();
    }
};
