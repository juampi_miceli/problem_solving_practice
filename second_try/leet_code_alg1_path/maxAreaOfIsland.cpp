#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <queue>

class Solution {
public:
    
    typedef pair<int, int> Spot;
    
    bool isInGrid(const Spot &spot, const std::vector<std::vector<int>> &grid){
        return spot.first >= 0 && spot.second >= 0 && spot.first < grid.size() && spot.second < grid[0].size();
    }
    
    bool isLand(const Spot &spot, const std::vector<std::vector<int>> &grid){
        return isInGrid(spot, grid) && grid[spot.first][spot.second] == 1;
    }
    
    void addAdjacentLand(const Spot &currentSpot, std::queue<Spot> &nextSpots, std::vector<std::vector<int>> &grid){
        std::vector<Spot> possibleLandSpots = {
            Spot(currentSpot.first-1, currentSpot.second),
            Spot(currentSpot.first+1, currentSpot.second),
            Spot(currentSpot.first, currentSpot.second-1),
            Spot(currentSpot.first, currentSpot.second+1),
            };
        
        for(Spot &possibleLandSpot : possibleLandSpots){
            if(isLand(possibleLandSpot, grid)){
                nextSpots.push(possibleLandSpot);
                grid[possibleLandSpot.first][possibleLandSpot.second] = 0; 
            }
        }
    }
    
    int getIslandSize(const Spot &initialSpot, std::vector<std::vector<int>> &grid){
        int size = 0;
        std::queue<Spot> nextSpots;
        nextSpots.push(initialSpot);
        grid[initialSpot.first][initialSpot.second] = 0; 
        
        
        while(!nextSpots.empty()){
            Spot currentSpot = nextSpots.front();
            nextSpots.pop();
            size++;
            addAdjacentLand(currentSpot, nextSpots, grid);
        }
        
        return size;
    }
    
    int maxAreaOfIsland(std::vector<std::vector<int>>& grid) {
        int maxIslandSize = 0;
        for(int row = 0; row < grid.size(); row++){
            for(int col = 0; col < grid[0].size(); col++){
                if(grid[row][col] == 1){
                    maxIslandSize = std::max(maxIslandSize, getIslandSize(Spot(row, col), grid));
                }
            }
        }
        
        return maxIslandSize;
        
    }
};