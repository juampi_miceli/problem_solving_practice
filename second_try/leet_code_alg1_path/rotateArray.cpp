#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    void shiftOne(std::vector<int> &nums){
        std::vector<int> buffer(2);
        int i = 0;
        int f;
        int pBuffer = 0;
        buffer[pBuffer] = nums[i];
        while(i < nums.size()){
            f = (i+1) % nums.size();
            buffer[1 - pBuffer] = nums[f];
            nums[f] = buffer[pBuffer];
            pBuffer = 1 - pBuffer;
            i++;
        }
    }
    void rotate(std::vector<int>& nums, int k) {
        k = k % nums.size();
        std::vector<int> copy = std::vector<int>(nums.begin(), nums.end());
        for(int i = 0; i < nums.size(); i++){
            nums[(i + k)%nums.size()] = copy[i];
        }
    }
};