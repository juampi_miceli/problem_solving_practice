#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    void recursivePermute(const std::vector<int> &nums, std::vector<std::vector<int>> &permutations, std::vector<int> currentPermutation, std::vector<bool> &availableNumbers){
        if(nums.size() == currentPermutation.size()){
            permutations.push_back(currentPermutation);
            return;
        }
        for(int i = 0; i < availableNumbers.size(); i++){
            if(availableNumbers[i]){
                currentPermutation.push_back(nums[i]);
                availableNumbers[i] = false;
                recursivePermute(nums, permutations, currentPermutation, availableNumbers);
                currentPermutation.pop_back();
                availableNumbers[i] = true;
            }
        }
        
        
    }
    
    std::vector<std::vector<int>> permute(std::vector<int>& nums) {
        std::vector<std::vector<int>> permutations;
        std::vector<int> currentPermutation;
        std::vector<bool> availableNumbers(nums.size(), true);
        recursivePermute(nums, permutations, currentPermutation, availableNumbers);
        return permutations;
    }
};