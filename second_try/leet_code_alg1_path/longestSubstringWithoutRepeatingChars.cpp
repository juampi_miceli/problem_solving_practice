#include <stdlib.h>
#include <stdio.h>
#include <string>

class Solution {
public:
    int lengthOfLongestSubstring(std::string s) {
        std::unordered_set<char> distinctChars;
        int winner = 0;
        
        int left = 0;
        int right = 0;
        
        while(right < s.size()){
            char rightChar = s[right];
            if(distinctChars.count(rightChar)){
                char leftChar = s[left];
                while(leftChar != rightChar){
                    distinctChars.erase(leftChar);
                    left++;
                    leftChar = s[left];
                }
                left++;
                right++;
            }else{
                distinctChars.insert(rightChar);
                winner = std::max(winner, (int)distinctChars.size());
                right++;
            }
        }
        
        return winner;
    }
};