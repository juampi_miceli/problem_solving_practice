#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    void getCombinations(int n, int k, std::vector<std::vector<int>> &combinations, std::vector<int> &currentCombination, int currentNumber){
        if(k == 0){
            combinations.push_back(currentCombination);
            return;
        }
        if(currentNumber > n){
            return;
        }
        
        currentCombination.push_back(currentNumber);
        getCombinations(n, k-1, combinations, currentCombination, currentNumber+1);
        currentCombination.pop_back();
        getCombinations(n, k, combinations, currentCombination, currentNumber+1);
    }
    
    
    std::vector<std::vector<int>> combine(int n, int k) {
        std::vector<std::vector<int>> combinations;
        std::vector<int> currentCombination;
        getCombinations(n, k, combinations, currentCombination, 1);
        return combinations;
    }
};