#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <queue>

class Solution {
public:
    
    typedef std::pair<int, int> Spot;
    typedef std::pair<Spot, int> SpotInfo;
    
    int distance(const SpotInfo &spotInfo){
        return spotInfo.second;
    }
    
    bool isInMat(const Spot &possibleNeighborSpot, int rows, int cols){
        return possibleNeighborSpot.first >= 0 && possibleNeighborSpot.first < rows &&  possibleNeighborSpot.second >= 0 && possibleNeighborSpot.second < cols;
    }
    
    
    bool isNeighbor(const Spot &possibleNeighborSpot, const std::vector<std::vector<bool>> &visited, int rows, int cols){
        return isInMat(possibleNeighborSpot, rows, cols) && !visited[possibleNeighborSpot.first][possibleNeighborSpot.second];
    }
    
    void addNeighbors(std::queue<SpotInfo> &nextMoves, const SpotInfo &currentSpotInfo, std::vector<std::vector<bool>> &visited, int rows, int cols){
        Spot currentSpot = currentSpotInfo.first;
        std::vector<Spot> possibleNeighborSpots = {
            Spot(currentSpot.first+1, currentSpot.second),
            Spot(currentSpot.first-1, currentSpot.second),
            Spot(currentSpot.first, currentSpot.second+1),
            Spot(currentSpot.first, currentSpot.second-1)
        };
        
        for(Spot possibleNeighborSpot : possibleNeighborSpots){
            if(isNeighbor(possibleNeighborSpot, visited, rows, cols)){
                nextMoves.push(SpotInfo(possibleNeighborSpot, distance(currentSpotInfo)+1));
                visited[possibleNeighborSpot.first][possibleNeighborSpot.second] = true;
            }
        }
    }

    
    std::vector<std::vector<int>> updateMatrix(std::vector<std::vector<int>>& mat) {
        int rows = mat.size();
        int cols = mat[0].size();
        std::vector<std::vector<int>> distances(rows, std::vector<int>(cols, 0));
        std::vector<std::vector<bool>> visited(rows, std::vector<bool>(cols, false));
        std::queue<SpotInfo> nextMoves;
        
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(mat[i][j] == 0){
                    nextMoves.push(SpotInfo(Spot(i, j), 0));
                    visited[i][j] = true;
                }
            }
        }
        
        while(!nextMoves.empty()){
            SpotInfo currentSpotInfo = nextMoves.front();
            nextMoves.pop();
            distances[currentSpotInfo.first.first][currentSpotInfo.first.second] = currentSpotInfo.second;
            addNeighbors(nextMoves, currentSpotInfo, visited, rows, cols);
        }
        
        return distances;
    }
};