#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <queue>

class Solution {
public:
    
    typedef std::pair<int, int> Spot; 
    typedef std::pair<Spot, int> SpotInfo;
    
    int EMPTY = 0;
    int FRESH = 1;
    int ROTTEN = 2;
    
    bool orangeIsInGrid(const Spot &currentSpot, int rows, int cols){
        return currentSpot.first >= 0 && currentSpot.first < rows && currentSpot.second >= 0 && currentSpot.second < cols;
    }
    
    bool orangeIsNotVisited(const std::vector<std::vector<bool>> &visited, const Spot &currentSpot){
        return !visited[currentSpot.first][currentSpot.second];
    }
    
    bool orangeIsFresh(const std::vector<std::vector<int>>& grid, const Spot &currentSpot){
        return grid[currentSpot.first][currentSpot.second] == FRESH;
    }
    
    bool isValidNewOrange(const std::vector<std::vector<int>>& grid, const std::vector<std::vector<bool>> &visited, const Spot &currentSpot, int rows, int cols){
        return orangeIsInGrid(currentSpot, rows, cols) && orangeIsNotVisited(visited, currentSpot) && orangeIsFresh(grid, currentSpot);
    }
    
    void addNextOranges(const std::vector<std::vector<int>>& grid, std::queue<SpotInfo> &nextMoves, std::vector<std::vector<bool>> &visited, const SpotInfo &currentSpotInfo, int rows, int cols){
        Spot currentSpot = currentSpotInfo.first;
        int row = currentSpot.first;
        int col = currentSpot.second;
        std::vector<Spot> possibleNewOranges = {
            Spot(row+1, col), // down
            Spot(row-1, col), // up
            Spot(row, col+1), // right
            Spot(row, col-1)  // left
        };
        
        for(Spot possibleNewOrange : possibleNewOranges){
            if(isValidNewOrange(grid, visited, possibleNewOrange, rows, cols)){
                nextMoves.push(SpotInfo(possibleNewOrange, currentSpotInfo.second+1));
                visited[possibleNewOrange.first][possibleNewOrange.second];
            }
        }
    }
    
    int orangesRotting(std::vector<std::vector<int>>& grid) {
        int rows = grid.size();
        int cols = grid[0].size();
        
        std::queue<SpotInfo> nextMoves;
        std::vector<std::vector<bool>> visited(rows, std::vector<bool>(cols, false));
        
        int daysUntilEveryOrangeIsRotten = 0;
        
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(grid[i][j] == ROTTEN){
                    nextMoves.push(SpotInfo(Spot(i, j), 0));
                    visited[i][j] = true;
                }
            }
        }
        
        while(!nextMoves.empty()){
            SpotInfo currentSpotInfo = nextMoves.front();
            Spot currentSpot = currentSpotInfo.first;
            int currentDays = currentSpotInfo.second;
            nextMoves.pop();
            
            if(grid[currentSpot.first][currentSpot.second] == FRESH){
                daysUntilEveryOrangeIsRotten = std::max(daysUntilEveryOrangeIsRotten, currentDays);
                grid[currentSpot.first][currentSpot.second] = ROTTEN;
            }
            
            addNextOranges(grid, nextMoves, visited, currentSpotInfo, rows, cols);
        }
        
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(grid[i][j] == FRESH){
                    return -1;
                }
            }
        }
        
        return daysUntilEveryOrangeIsRotten;
    }
};