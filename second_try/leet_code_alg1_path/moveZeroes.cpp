#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    void shiftRight(std::vector<int> &v, int i){
        int N = v.size();
        while(i < N-1 && v[i+1] != 0){
            std::swap(v[i], v[i+1]);
            i++;
        }
    }
    
    void moveZeroes(std::vector<int>& nums) {
        int N = nums.size();
        for(int i = N-1; i >= 0; i--){
            if(nums[i] == 0){
                shiftRight(nums, i);
            }
        }
    }
};