#include <stdlib.h>
#include <stdio.h>

class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size();
        while(right > left){
            int middle = (left + right) / 2;
            if(nums[middle] == target){
                return middle;
            }
            if(nums[middle] < target){
                left = middle + 1;
            }
            if(nums[middle] > target){
                right = middle;
            }
        }
        return right;
    }
};