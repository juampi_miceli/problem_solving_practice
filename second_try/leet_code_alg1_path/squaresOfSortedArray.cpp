#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    void invert(std::vector<int> &v){
        int left = 0;
        int right = v.size()-1;
        while(right > left){
            std::swap(v[left], v[right]);
            left++;
            right--;
        }
    }
    
    std::vector<int> sortedSquares(std::vector<int>& nums) {
        int left = 0;
        int right = nums.size()-1;
        std::vector<int> res;
        
        while(right >= left){
            int vLeft = nums[left];
            int vRight = nums[right];
            if(std::abs(vLeft) > std::abs(vRight)){
                res.push_back(vLeft*vLeft);
                left++;
            }else{
                res.push_back(vRight*vRight);
                right--;
            }
        }
        invert(res);
        return res;
    }
};