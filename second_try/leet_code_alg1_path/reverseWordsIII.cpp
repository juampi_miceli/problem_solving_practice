#include <stdlib.h>
#include <stdio.h>
#include <string>

class Solution {
public:
    
    int findWordEnd(std::string s, int i){
        int N = s.size();
        while(i < N && s[i] != ' '){
            i++;
        }
        return i-1;
    }
    
    void reverseString(std::string &s, int start, int end){
        while(end > start){
            std::swap(s[start], s[end]);
            end--;
            start++;
        }
    }
    
    std::string reverseWords(std::string s) {
        int N = s.size();
        int i = 0;
        while(i < N){
            int wordEnd = findWordEnd(s, i);
            reverseString(s, i, wordEnd);
            i = wordEnd + 2;
        }
        return s;
    }
};