#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int N = nums.size();
        int left = 0;
        int right = N;
        while(right > left){
            int middle = (left + right) / 2;
            if(nums[middle] == target){
                return middle;
            }
            if(nums[middle] < target){
                left = middle+1;
                continue;
            }
            if(nums[middle] > target){
                right = middle;
            }
        }
        return -1;
    }
};