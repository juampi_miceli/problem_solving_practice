#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>

class Solution {
public:
    
    bool equals(std::vector<int> &v1, std::vector<int> &v2){
        if(v1.size() != v2.size()){
            return false;
        }
        for(int i = 0; i < v1.size(); i++){
            if(v1[i] != v2[i]){
                return false;
            }
        }
        return true;
    }
    
    std::vector<int> getFrecuencies(std::string s, int low, int high){
        std::vector<int> frecuencies('z'-'a'+1, 0);
        for(int i = low; i < high; i++){
            frecuencies[s[i]-'a']++;
        }
        return frecuencies;
    }
    
    bool checkInclusion(std::string s1, std::string s2) {
        if(s1.size() > s2.size()){
            return false;
        }
        int left = 0;
        int right = s1.size()-1;
        
        std::vector<int> s1Frecuencies = getFrecuencies(s1, 0, s1.size());
        std::vector<int> windowFrecuencies = getFrecuencies(s2, 0, right+1);
        
        while(right < s2.size()){
            if(equals(s1Frecuencies, windowFrecuencies)){
                return true;
            }
            windowFrecuencies[s2[left]-'a']--;
            left++;
            right++;
            if(right == s2.size()){
                return false;
            }
            windowFrecuencies[s2[right]-'a']++;
        }
        return false;
    }
};