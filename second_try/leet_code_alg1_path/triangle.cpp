#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:

    int minimumTotal(std::vector<std::vector<int>>& triangle) {
        int levels = triangle.size();
        std::vector<std::vector<int>> memo(levels);
        
        for(int i = 0; i < triangle.size()-1; i++){
            memo[i] = std::vector<int>(triangle[i].size(), -20000);
        }
        memo[levels - 1] = std::vector<int>(triangle[levels - 1].begin(), triangle[levels - 1].end());
        
        for(int i = levels - 2; i >= 0; i--){
            for(int j = 0; j < triangle[i].size(); j++){
                memo[i][j] = std::min(memo[i+1][j], memo[i+1][j+1]) + triangle[i][j];
            }
        }
        
        return memo[0][0];
    }
};