#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <queue>

class Solution {
public:
    typedef std::pair<int, int> Pos;
    
    bool isInsideImage(const std::vector<std::vector<int>>& image, const Pos &pos){
        return pos.first >= 0 && pos.first < image.size() && pos.second >= 0 && pos.second < image[0].size();
    }
    
    bool isNeighbor(const std::vector<std::vector<int>>& image, const Pos &pos, int oldColor, int newColor){
        return isInsideImage(image, pos) && image[pos.first][pos.second] == oldColor && image[pos.first][pos.second] != newColor;
    }
    
    void pushAllNeighbors(const std::vector<std::vector<int>>& image, std::queue<Pos> &q, const Pos &currentPos, int oldColor, int newColor){
        int x = currentPos.second;
        int y = currentPos.first;
        std::vector<Pos> possibleNeighbors = {Pos(y-1, x), Pos(y+1, x), Pos(y, x-1), Pos(y, x+1)};
        for(Pos &possibleNeighbor : possibleNeighbors){
            if(isNeighbor(image, possibleNeighbor, oldColor, newColor)){
                q.push(possibleNeighbor);
            }
        }
    }
    
    std::vector<std::vector<int>> floodFill(std::vector<std::vector<int>>& image, int sr, int sc, int newColor) {
        std::queue<Pos> nextMoves;
        int oldColor = image[sr][sc];
        nextMoves.push(Pos(sr, sc));
        
        while(!nextMoves.empty()){
            Pos currentPos = nextMoves.front();
            nextMoves.pop();
            image[currentPos.first][currentPos.second] = newColor;
            pushAllNeighbors(image, nextMoves, currentPos, oldColor, newColor);
        }
        
        return image;
    }
};