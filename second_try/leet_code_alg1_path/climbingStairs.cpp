#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int climbStairs(int n) {
        std::vector<int> steps(n+1, 0);
        steps[n] = 1;
        
        for(int i = n; i >= 0; i--){
            if(i+1 < n+1){
                steps[i] += steps[i+1];
            }
            if(i+2 < n+1){
                steps[i] += steps[i+2];
            }
        }
        return steps[0];
    }
};