#include <stdlib.h>
#include <stdio.h>

class Solution {
public:

    bool isBadVersion(int i){
        return i >= 3;
    }
    
    int firstBadVersion(int n) {
        long left = 0;
        long right = n;
        while(right - left > 1){
            long middle = (left + right) / 2;
            if(isBadVersion(middle)){
                right = middle;
            }else{
                left = middle;
            }
        }
        return right;
    }
};