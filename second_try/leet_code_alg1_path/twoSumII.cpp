#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    std::vector<int> twoSum(std::vector<int>& nums, int target) {
        int N = nums.size();
        int left = 0;
        int right = N-1;
        int sum = nums[left] + nums[right];
        while(sum != target){
            if(sum > target){
                right--;
            }else{
                left++;
            }
            sum = nums[left] + nums[right];
        }
        return {left+1, right+1};
    }
};