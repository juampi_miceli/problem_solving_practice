#include <stdlib.h>
#include <stdio.h>
#include <vector>

// Great explanation of DP: https://leetcode.com/problems/house-robber/discuss/156523/From-good-to-great.-How-to-approach-most-of-DP-problems.
class Solution {
public:
    
    int rob(std::vector<int>& nums) {
        std::vector<int> memo(nums.size()+1, -1);
        memo[0] = 0;
        memo[1] = nums[0];
        for(int i = 1; i < nums.size(); i++){
            memo[i+1] = std::max(memo[i], memo[i-1] + nums[i]);
        }
        return memo[nums.size()];
    }
};