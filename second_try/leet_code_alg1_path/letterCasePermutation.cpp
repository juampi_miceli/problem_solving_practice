#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vector>

class Solution {
public:
    
    void recursiveLetterCasePermutation(std::string &s, std::vector<std::string> &permutations, std::string currentPermutation, int currentStep){
        if(currentStep == s.size()){
            permutations.push_back(currentPermutation);
            return;
        }
        char c = s[currentStep];
        currentPermutation.push_back(c);
        recursiveLetterCasePermutation(s, permutations, currentPermutation, currentStep+1);
        currentPermutation.pop_back();
        if(c >= 'a' && c <= 'z'){
            currentPermutation.push_back(std::toupper(c));
            recursiveLetterCasePermutation(s, permutations, currentPermutation, currentStep+1);
            currentPermutation.pop_back();
        }else if(c >= 'A' && c <= 'Z'){
            currentPermutation.push_back(std::tolower(c));
            recursiveLetterCasePermutation(s, permutations, currentPermutation, currentStep+1);
            currentPermutation.pop_back();
        }
    }
    
    std::vector<std::string> letterCasePermutation(std::string s) {
        std::vector<std::string> permutations;
        std::string currentPermutation;
        recursiveLetterCasePermutation(s, permutations, currentPermutation, 0);
        return permutations;
    }
};