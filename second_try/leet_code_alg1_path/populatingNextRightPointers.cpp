#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <vector>


class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};


class Solution {
public:
    
    void connectInnerLevel(std::vector<Node *> &level){
        int N = level.size();
        for(int i = 0; i < N - 1; i++){
            level[i]->next = level[i+1];
        }
        return;
    }
    
    void addNextLevel(std::queue<std::vector<Node *>> &nextLevels, const std::vector<Node *> currentLevel){
        if(currentLevel[0]->left == nullptr){
            return;
        }
        std::vector<Node *> newLevel;
        for(int i = 0; i < currentLevel.size(); i++){
            newLevel.push_back(currentLevel[i]->left);
            newLevel.push_back(currentLevel[i]->right);
        }
        nextLevels.push(newLevel);
    }
    
    Node* connect(Node* root) {
        if(root == nullptr){
            return root;
        }
        std::queue<std::vector<Node *>> nextLevels;
        nextLevels.push({root});
        while(!nextLevels.empty()){
            std::vector<Node *> currentLevel = nextLevels.front();
            nextLevels.pop();
            connectInnerLevel(currentLevel);
            addNextLevel(nextLevels, currentLevel);
        }
        
        return root;
        
    }
};