#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <iostream>

void dfs(std::vector<std::vector<int>> &edges, int node){

    std::vector<int> nextMoves;
    nextMoves.push_back(node);

    std::vector<bool> visited(edges.size(), false);
    visited[node-1] = true;

    while(!nextMoves.empty()){
        int currentNode = nextMoves.back();
        nextMoves.pop_back();

        std::cout <<  currentNode << "\n";

        for(std::vector<int> &edge : edges){
            int self;
            int neighbor;
            if(edge[0] == currentNode){
                self = edge[0];
                neighbor = edge[1];
            }else if(edge[1] == currentNode){
                self = edge[1];
                neighbor = edge[0];
            }else{
                continue;
            }
            if(visited[neighbor-1]){
                continue;
            }
            nextMoves.push_back(neighbor);
            visited[neighbor-1] = true;
        }
    }
}

int main(){
    std::vector<std::vector<int>> edges = {
        {1,2},
        {1,3},
        {2,3},
        {3,4},
        {4,5},
        {5,6},
        {4,7}
    };
    dfs(edges, 1);
    return 0;
}