#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <iostream>

class UnionFind{

public:
    UnionFind(int numberOfNodes) : _parents(std::vector<int>(numberOfNodes, -1)), _rank(std::vector<int>(numberOfNodes, 1)) {};

    int find(int elem){
        if(_parents[elem] == -1){
            return -1;
        }
        while(elem != _parents[elem]){
            _parents[elem] = _parents[_parents[elem]];
            elem = _parents[elem];
        }
        return elem;
    }

    bool join(int e1, int e2){

        if(!hasBeenAdded(e1)){
            add(e1);
        }

        if(!hasBeenAdded(e2)){
            add(e2);
        }

        int leader1 = find(e1);
        int leader2 = find(e2);

        if(leader1 == leader2){
            return false;
        }

        if(_rank[leader1] <= _rank[leader2]){
            _parents[leader2] = leader1;
            _rank[leader1] += _rank[leader2];
        }else{
            _parents[leader1] = leader2;
            _rank[leader2] += _rank[leader1];
        }
        return true;
    }

    bool hasBeenAdded(int elem){
        return _parents[elem] != -1;
    }

    void add(int elem){
        _parents[elem] = elem;
    }
private:
    std::vector<int> _parents;
    std::vector<int> _rank;
};

std::vector<std::vector<int>> merge(std::vector<std::vector<int>> t1, std::vector<std::vector<int>> t2){
    int p1 = 0;
    int p2 = 0;

    std::vector<std::vector<int>> res;

    while(p1 < t1.size() && p2 < t2.size()){
        if(t1[p1][2] <= t2[p2][2]){
            res.push_back(t1[p1]);
            p1++;
        }else{
            res.push_back(t2[p2]);
            p2++;
        }
    }

    while(p1 < t1.size()){
        res.push_back(t1[p1]);
        p1++;
    }
    while(p2 < t2.size()){
        res.push_back(t2[p2]);
        p2++;
    }

    return res;
}

std::vector<std::vector<int>> sortTriplas(std::vector<std::vector<int>> triples){
    if(triples.size() <= 1){
        return triples;
    }
    int half = triples.size()/2;
    std::vector<std::vector<int>> sortedLeft = sortTriplas(std::vector<std::vector<int>>(triples.begin(), triples.begin()+half));
    std::vector<std::vector<int>> sortedRight = sortTriplas(std::vector<std::vector<int>>(triples.begin()+half, triples.end()));

    return merge(sortedLeft, sortedRight);
}

int getNumberOfNodes(std::vector<std::vector<int>> &edges){
    int max = 0;

    for(std::vector<int> &edge : edges){
        max = std::max(max, std::max(edge[0], edge[1]));
    }

    return max+1;
}

std::vector<bool> solve(std::vector<std::vector<int>> &edges, std::vector<std::vector<int>> &queries){
    std::vector<bool> res;
    int numberOfNodes = getNumberOfNodes(edges);

    UnionFind unionFind = UnionFind(numberOfNodes);
    std::map<std::vector<int>, bool> queriesResults;

    std::vector<std::vector<int>> sortedQueries = sortTriplas(queries);
    std::vector<std::vector<int>> sortedEdges = sortTriplas(edges);


    int pEdges = 0;

    for(std::vector<int> &query : sortedQueries){
        while(pEdges < sortedEdges.size() && sortedEdges[pEdges][2] <= query[2]){
            unionFind.join(sortedEdges[pEdges][0], sortedEdges[pEdges][1]);
            pEdges++;
        }
        queriesResults[query] = unionFind.find(query[0]) != -1 && unionFind.find(query[0]) == unionFind.find(query[1]);
    }

    for(int i = 0; i < queries.size(); i++){
        res.push_back(queriesResults[queries[i]]);
    }
    return res;
}

int main(){
    std::vector<std::vector<int>> edges = {
        {0, 1, 5},
        {1, 2, 6},
        {2, 3, 7},
        {0, 3, 4}
    };
    std::vector<std::vector<int>> queries = {
        {0, 3, 5},
        {1, 0, 3}
    };
    std::vector<bool> res = solve(edges, queries);
    for(int i = 0; i < res.size(); i++){
        std::cout << res[i] << "\n";
    }

    return 0;
}