#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

class UnionFind{

public:
    UnionFind(int numberOfNodes) : _rank(std::vector<int>(numberOfNodes, 1)), _parents(std::vector<int>(numberOfNodes, -1)) {};

    int find(int elem){
        int res = elem;
        while(res != _parents[res]){
            _parents[res] = _parents[_parents[res]];
            res = _parents[res];
        }
        return res;
    }
    
    bool join(int e1, int e2){
        int leader1 = find(e1);
        int leader2 = find(e2);

        if(leader1 == leader2){
            return false;
        }

        if(_rank[leader1] <= _rank[leader2]){
            _parents[leader2] = leader1;
            _rank[leader1] += _rank[leader2];
        }
        return true;
    }

    bool isLand(int elem){
        return _parents[elem] != -1;
    }
    
    void set(int elem){
        _parents[elem] = elem;
    }

private:
    std::vector<int> _parents;
    std::vector<int> _rank;
};

int id(std::vector<int> &pos, int cols){
    return pos[0] * cols + pos[1]; 
}

std::vector<int> numIslands2(int rows, int cols, std::vector<std::vector<int>> &positions){
    int numberOfDays = positions.size();

    UnionFind unionFind = UnionFind(rows * cols);

    std::vector<int> numberOfIslands(numberOfDays, 0);


    for(int day = 0; day < numberOfDays; day++){
        if(day > 0){
            numberOfIslands[day] = numberOfIslands[day-1];
        }
        std::vector<int> newLandPosition = positions[day];
        if(unionFind.isLand(id(newLandPosition, cols))){
            continue;
        }
        unionFind.set(id(newLandPosition, cols));
        numberOfIslands[day]++;
        std::vector<std::vector<int>> possibleNeighbors = {
            {newLandPosition[0]-1, newLandPosition[1]},
            {newLandPosition[0]+1, newLandPosition[1]},
            {newLandPosition[0], newLandPosition[1]-1},
            {newLandPosition[0], newLandPosition[1]+1}
        };

        for(std::vector<int> &possibleNeighbor : possibleNeighbors){
            if(possibleNeighbor[0] < 0 || possibleNeighbor[1] < 0 || possibleNeighbor[0] >= rows || possibleNeighbor[1] >= cols){
                continue;
            }
            if(unionFind.isLand(id(possibleNeighbor, cols))){
                if(unionFind.join(id(newLandPosition, cols), id(possibleNeighbor, cols))){
                    numberOfIslands[day]--;
                }
            }
        }
    }
    return numberOfIslands;
    
}

int main(){
    std::vector<std::vector<int>> positions = {
        {0,0},
        {0,1},
        {1,2},
        {2,1},
        {0,0}
    };
    std::vector<int> res = numIslands2(3, 3, positions);
    for(int i = 0; i < res.size(); i++){
        std::cout << res[i] << ",";
    }
    std::cout << "\b"; 
    return 0;
}