#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    void addNextMove(std::vector<std::vector<int>>& grid, std::vector<int> &possibleMove, std::vector<std::vector<int>> &nextMoves, std::vector<std::vector<bool>> &lookedAt, int currentLevel){
        int N = lookedAt.size();
        if(possibleMove[0] < 0 || possibleMove[1] < 0 || possibleMove[0] >= N || possibleMove[1] >= N){
            return;
        }
        if(lookedAt[possibleMove[0]][possibleMove[1]]){
            return;
        }
        if(grid[possibleMove[0]][possibleMove[1]] > currentLevel){
            return;
        }
        nextMoves.push_back({possibleMove[0], possibleMove[1]});
        lookedAt[possibleMove[0]][possibleMove[1]] = true;
    }
    
    void addNextMoves(std::vector<std::vector<int>>& grid, std::vector<int> &currentCell, std::vector<std::vector<int>> &nextMoves, std::vector<std::vector<bool>> &lookedAt, int currentLevel){
        std::vector<std::vector<int>> possibleMoves = {
            {currentCell[0]-1, currentCell[1]},
            {currentCell[0]+1, currentCell[1]},
            {currentCell[0], currentCell[1]-1},
            {currentCell[0], currentCell[1]+1},
        };
        for(std::vector<int> &possibleMove : possibleMoves){
            addNextMove(grid, possibleMove, nextMoves, lookedAt, currentLevel);
        }
    }
    
    bool trySwimming(std::vector<std::vector<int>>& grid, int currentLevel){
        if(grid[0][0] > currentLevel){
            return false;
        }
        int N = grid.size();
        
        std::vector<std::vector<int>> nextMoves;
        nextMoves.push_back({0,0});
        
        std::vector<std::vector<bool>> lookedAt(N, std::vector<bool>(N, false));
        lookedAt[0][0] = true;
        
        while(!nextMoves.empty()){
            std::vector<int> currentCell = nextMoves[nextMoves.size()-1];
            nextMoves.pop_back();
            if(currentCell[0] == N-1 && currentCell[1] == N-1){
                //Reached the end
                return true;
            }
            addNextMoves(grid, currentCell, nextMoves, lookedAt, currentLevel);
        }
        return false;
    }
    
    int swimInWater(std::vector<std::vector<int>>& grid) {
        int N = grid.size();
        int min = 0;     // 2
        int max = (N * N) - 1; // 3
        while(max - min > 0){
            int currentLevel = (max + min) / 2; // 2
            bool success = trySwimming(grid, currentLevel);
            if(success){
                max = currentLevel;
            }else{
                min = currentLevel + 1;
            }
        }
        return min;
    }
};