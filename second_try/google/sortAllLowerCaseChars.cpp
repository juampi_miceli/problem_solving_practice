#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

int advancePointer(std::vector<int> occurrences, int currentPointer){
    while(currentPointer < occurrences.size() && occurrences[currentPointer] == 0){
        currentPointer++;
    }
    return currentPointer;
}

std::string sortAllLowerCaseChars(std::string s){
    std::vector<int> lettersOccurrences('z'-'a'+1, 0);

    for(char c : s){
        if(c >= 'a' && c <= 'z'){
            lettersOccurrences[c - 'a']++;
        }
    }

    int letterPointer = advancePointer(lettersOccurrences, 0);
    for(int i = 0; i < s.size(); i++){
        if(s[i] >= 'a' && s[i] <= 'z'){
            s[i] = 'a' + letterPointer;
            lettersOccurrences[letterPointer]--;
            letterPointer = advancePointer(lettersOccurrences, letterPointer);
            if(letterPointer >= lettersOccurrences.size()){
                break;
            }
        }
    }
    return s;
}

int main(){
    std::cout << sortAllLowerCaseChars("Test@123 Google") << "\n";
}