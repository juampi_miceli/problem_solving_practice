#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    typedef std::pair<int, int> Node;
    
    
    void addNextMove(std::vector<Node> &nextMoves, std::vector<std::vector<char>>& grid, std::vector<std::vector<bool>> &lookedAt, Node &nextMove){
        if(nextMove.first < 0 || nextMove.second < 0 || nextMove.first >= grid.size() || nextMove.second >= grid[0].size()){
            return;
        }
        if(lookedAt[nextMove.first][nextMove.second]){
            return;
        }
        if(grid[nextMove.first][nextMove.second] == '0'){
            return;
        }
        nextMoves.push_back(nextMove);
        lookedAt[nextMove.first][nextMove.second] = true;
    }
    
    void addNextMoves(std::vector<Node> &nextMoves, std::vector<std::vector<char>>& grid, std::vector<std::vector<bool>> &lookedAt, Node &currentNode){

        std::vector<Node> possibleNextMoves = {
            Node(currentNode.first-1, currentNode.second),
            Node(currentNode.first+1, currentNode.second),
            Node(currentNode.first, currentNode.second-1),
            Node(currentNode.first, currentNode.second+1)
        };
        
        for(Node &possibleNextMove : possibleNextMoves){
            addNextMove(nextMoves, grid, lookedAt, possibleNextMove);
        }
    }
    
    int numIslands(std::vector<std::vector<char>>& grid) {
        
        int numberOfIslands = 0;
        
        std::vector<std::vector<bool>> lookedAt(grid.size(), std::vector<bool>(grid[0].size(), false));
        
        for(int i = 0; i < grid.size(); i++){
            for(int j = 0; j < grid[0].size(); j++){
                if(lookedAt[i][j] || grid[i][j] == '0'){
                    continue;
                }
                numberOfIslands++;
                std::vector<Node> nextMoves;
                nextMoves.push_back(Node(i, j));
                lookedAt[i][j] = true;
                while(!nextMoves.empty()){
                    Node currentNode = nextMoves[nextMoves.size()-1];
                    nextMoves.pop_back();
                    addNextMoves(nextMoves, grid, lookedAt, currentNode);
                }
            }
        }
        return numberOfIslands;
    }
};


