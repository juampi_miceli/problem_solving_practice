#include <stdlib.h>
#include <stdio.h>
#include <queue>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    typedef std::pair<TreeNode *, int> NodeInfo;
    
    int maxLevelSum(TreeNode* root) {
        int currentLevel = 0;
        
        long acc = INT_MIN;
        long winner = root->val;
        int winnerLevel = 1;
        
        std::queue<NodeInfo> nextMoves;
    
        nextMoves.push(NodeInfo(root, 1));
        
        while(!nextMoves.empty()){
            NodeInfo nodeInfo = nextMoves.front();
            nextMoves.pop();
            
            if(nodeInfo.second != currentLevel){
                if(acc > winner){
                    winner = acc;
                    winnerLevel = currentLevel;
                }
                acc = 0;
                currentLevel++;
            }
            
            if(nodeInfo.second == currentLevel){
                acc += nodeInfo.first->val;
            }
            
            if(nodeInfo.first->left != nullptr){
                nextMoves.push(NodeInfo(nodeInfo.first->left, currentLevel+1));
            }
            if(nodeInfo.first->right != nullptr){
                nextMoves.push(NodeInfo(nodeInfo.first->right, currentLevel+1));
            }
        }
        
        if(acc > winner){
            winner = acc;
            winnerLevel = currentLevel;
        }
        
        return winnerLevel;
    }
};
                    