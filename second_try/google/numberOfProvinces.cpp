#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    int find(std::vector<int> &parents, int elem){
        while(parents.at(elem) != elem){
            elem = parents.at(elem);
        }
        return elem;
    }
    
    void optimizedJoin(std::vector<int> &parents, std::vector<int> &rank, int e1, int e2){
        int leader1 = find(parents, e1);
        int leader2 = find(parents, e2);
        parents.at(leader2) = leader1;
        rank.at(leader1) += rank.at(leader2);
    }
    
    void join(std::vector<int> &parents, std::vector<int> &rank, int e1, int e2){
        if(rank.at(e1) <= rank.at(e2)){
            optimizedJoin(parents, rank, e1, e2);
            return;
        }
        optimizedJoin(parents, rank, e2, e1);
    }
    
    int findCircleNum(std::vector<std::vector<int>>& isConnected) {
        int numberOfProvinces = isConnected.size();
        
        std::vector<int> parents(isConnected.size());
        std::vector<int> rank(isConnected.size(), 1);
        for(int i = 0; i < isConnected.size(); i++){
            parents.at(i) = i;
        }
        
        for(int i = 0; i < isConnected.size(); i++){
            for(int j = 0; j < isConnected.size(); j++){
                if(isConnected.at(i).at(j) == 0){
                    continue;
                }
                int leaderI = find(parents, i);
                int leaderJ = find(parents, j);
                if(leaderI == leaderJ){
                    continue;
                }
                numberOfProvinces--;
                join(parents, rank, leaderI, leaderJ);
            }
        }
        return numberOfProvinces;
    }
};