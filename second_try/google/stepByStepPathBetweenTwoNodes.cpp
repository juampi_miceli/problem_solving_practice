#include <stdlib.h>
#include <stdio.h>
#include <string>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    
    bool findPath(TreeNode *root, int target, std::string &current){
        if(root == nullptr){
            return false;
        }
        if(root->val == target){
            return true;
        }
        
        current.push_back('L');
        bool resLeft = findPath(root->left, target, current);
        if(resLeft){
            return true;
        }
        current.pop_back();
        current.push_back('R');
        bool resRight = findPath(root->right, target, current);
        if(resRight){
            return true;
        }
        current.pop_back();
        return false;
    }
    
    string getDirections(TreeNode* root, int startValue, int destValue) {
        
        std::string pathStart;
        findPath(root, startValue, pathStart);
        std::string pathDest;
        findPath(root, destValue, pathDest);
        
        int pStart = 0;
        int pDest = 0;
        while(pStart < pathStart.size() && pDest < pathDest.size() && pathStart[pStart] == pathDest[pDest]){
            pStart++;
            pDest++;
        }
        std::string resultPath = "";
        while(pStart < pathStart.size()){
            resultPath += "U";
            pStart++;
        }
        while(pDest < pathDest.size()){
            resultPath += pathDest[pDest];
            pDest++;
        }
        
        return resultPath;
    }
};