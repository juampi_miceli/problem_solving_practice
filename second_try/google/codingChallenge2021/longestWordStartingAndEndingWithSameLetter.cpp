#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>

typedef std::pair<int, int> Par;

std::string longestWordStartingAndEndingWithSameLetter(std::string &s){
    int MAX = 10001;
    int MIN = -1;
    std::vector<Par> letterOcurrences('z'-'a'+1, Par(MAX, MIN));

    for(int i = 0; i < s.size(); i++){
        letterOcurrences[s[i]-'a'].first = std::min(letterOcurrences[s[i]-'a'].first, i);
        letterOcurrences[s[i]-'a'].second = std::max(letterOcurrences[s[i]-'a'].second, i);
    }

    Par winnerPar = letterOcurrences[s[0]];
    for(Par &par : letterOcurrences){
        if(par.first == MAX){
            continue;
        }
        if(par.second - par.first > winnerPar.second - winnerPar.first){
            winnerPar = par;
        }
    }

    return std::string(s.begin()+winnerPar.first, s.begin()+winnerPar.second+1);
}

int main(){
    std::string s1 = "cbaabaab";
    std::string s2 = "performance";
    std::string s3 = "cat";

    printf("%s: %s\n", s1.c_str(), longestWordStartingAndEndingWithSameLetter(s1).c_str());
    printf("%s: %s\n", s2.c_str(), longestWordStartingAndEndingWithSameLetter(s2).c_str());
    printf("%s: %s\n", s3.c_str(), longestWordStartingAndEndingWithSameLetter(s3).c_str());
    return 0;
}