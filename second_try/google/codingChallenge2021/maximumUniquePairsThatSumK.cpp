#include <stdlib.h>
#include <stdio.h>
#include <vector>

int pairsThatSumK(int k, std::vector<int> &nums){
    int left = 0;
    int right = nums.size() - 1;

    int pairs = 0;

    while(right > left){
        int currentSum = nums[left] + nums[right];
        if(currentSum == k){
            pairs++;
            right--;
            left++;
        }else if(currentSum > k){
            right--;
        }else{
            left++;
        }
    }

    return pairs;
}

int solution(std::vector<int> &nums){
    if(nums.size() <= 1){
        return 0;
    }
    std::sort(nums.begin(), nums.end());
    int N = nums.size();
    int minSum = nums[0] + nums[1];
    int maxSum = nums[N-1] + nums[N-2];

    int maximumPairs = 0;

    for(int k = minSum; k <= maxSum; k++){
        maximumPairs = std::max(maximumPairs, pairsThatSumK(k, nums));
    }

    return maximumPairs;
}

int main(){

    std::vector<int> v1 = {1, 9, 8, 100, 2};
    std::vector<int> v2 = {2, 2, 2, 3};
    std::vector<int> v3 = {2, 2, 2, 2, 2};

    printf("v1: %d\n", solution(v1));
    printf("v2: %d\n", solution(v2));
    printf("v3: %d\n", solution(v3));

    return 0;
}