#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>

bool greater(int index1, int index2, const std::string &s){
    int N = s.size();
    while(index1 < N && index2 < N){
        if(s[index1] > s[index2]){
            return true;
        }else if(s[index1] < s[index2]){
            return false;
        }
        index1++;
        index2++;
    }
    return index2 == N;
}

std::vector<int> suffixArrayMerge(const std::vector<int> &v1, const std::vector<int> &v2, const std::string &s){
    std::vector<int> mergedArray;

    int p1 = 0;
    int p2 = 0;
    int N1 = v1.size();
    int N2 = v2.size();

    while(p1 < N1 && p2 < N2){
        if(greater(v1[p1], v2[p2], s)){
            mergedArray.push_back(v2[p2]);
            p2++;
        }else{
            mergedArray.push_back(v1[p1]);
            p1++;
        }
    }

    while(p1 < N1){
        mergedArray.push_back(v1[p1]);
        p1++;
    }

    while(p2 < N2){
        mergedArray.push_back(v2[p2]);
        p2++;
    }

    return mergedArray;
}

void suffixArraySort(std::vector<int> &indices, const std::string &s){
    if(indices.size() <= 1){
        return;
    }

    int half = indices.size() / 2;
    std::vector<int> leftHalf = std::vector<int>(indices.begin(), indices.begin() + half);
    std::vector<int> rightHalf = std::vector<int>(indices.begin() + half, indices.end());
    suffixArraySort(leftHalf, s);
    suffixArraySort(rightHalf, s);

    indices = suffixArrayMerge(leftHalf, rightHalf, s);
    return;
}

std::vector<int> suffixArray(const std::string &s){
    std::vector<int> indices(s.size());
    for(int i = 0; i < s.size(); i++){
        indices[i] = i;
    }
    suffixArraySort(indices, s);

}

int main(){
    return 0;
}