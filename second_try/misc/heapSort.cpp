#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <vector>

void minHeapSort(std::vector<int> &numbers){
    std::priority_queue<int, std::vector<int>, std::greater<int> > minHeap;
    for(int e : numbers){
        minHeap.push(e);
    }
    for(int i = 0; i < numbers.size(); i++){
        numbers[i] = minHeap.top();
        minHeap.pop();
    }
}

void maxHeapSort(std::vector<int> &numbers){
    std::priority_queue<int> maxHeap;
    for(int e : numbers){
        maxHeap.push(e);
    }
    for(int i = 0; i < numbers.size(); i++){
        numbers[i] = maxHeap.top();
        maxHeap.pop();
    }
}

int main(){

    std::vector<int> numbers = {1,3,5,2,3,8,7,4,6};
    minHeapSort(numbers);
    maxHeapSort(numbers);
    for(int e : numbers){
        printf("%d\t", e);
    }
    return 0;
}