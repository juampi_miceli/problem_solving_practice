#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    std::vector<int> runningSum(std::vector<int>& nums) {
        std::vector<int> res = {nums[0]};
        for(int i = 1; i < nums.size(); i++){
            res.push_back(res[i-1] + nums[i]);
        }
        
        return res;
    }
};