#include <stdlib.h>
#include <stdio.h>

class Solution {
public:
    
    bool isPowerOfTwoAux(long current, int goal){
        if(current > goal){
            return false;
        }
        if(current == goal){
            return true;
        }
        return isPowerOfTwoAux(2*current, goal);
    }
    
    bool isPowerOfTwo(int n) {
        return isPowerOfTwoAux(1, n);
    }
    
    bool isPowerOfTwoFollowUp(int n){
        return n > 0 && (n & (n-1)) == 0;
    }
};