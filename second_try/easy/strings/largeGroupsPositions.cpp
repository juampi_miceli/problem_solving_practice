#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>

class Solution {
public:
    
    int getNextTokenSize(std::string s, int start){
        char c = s[start];
        int N = s.size();
        int res = 0;
        while(start < N && s[start] == c){
            res++;
            start++;
        }
        return res;
    }
    
    std::vector<std::vector<int>> largeGroupPositions(std::string s) {
        int i = 0;
        int N = s.size();
        std::vector<std::vector<int>> res;
        while(i < N){
            int tokenSize = getNextTokenSize(s, i);
            if(tokenSize >= 3){
                res.push_back({i, i+tokenSize-1});
            }
            i += tokenSize;
        }
        return res;
    }
};