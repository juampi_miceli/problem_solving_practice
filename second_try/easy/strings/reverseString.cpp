#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    void reverseStringIterative(std::vector<char>& s) {
        int pL = 0;
        int pR = s.size()-1;
        while(pL < pR){
            std::swap(s[pL], s[pR]);
            pR--;
            pL++;
        }
    }

    void reverseStringAux(std::vector<char> &s, int pLeft, int pRight) {
        if(pRight <= pLeft){
            return;
        }
        std::swap(s[pLeft], s[pRight]);
        reverseStringAux(s, pLeft+1, pRight-1);
    }
    
    void reverseString(std::vector<char> &s){
        reverseStringAux(s, 0, s.size()-1);
    }
    
};