
#include <stdlib.h>
#include <stdio.h>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
 
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
        if(list1 == nullptr){
            return list2;
        }
        if(list2 == nullptr){
            return list1;
        }
        if(list1->val <= list2->val){
            ListNode* nextNode = list1->next;
            list1->next = mergeTwoLists(nextNode, list2);
            return list1;
        }
        ListNode* nextNode = list2->next;
        list2->next = mergeTwoLists(nextNode, list1);
        return list2;
    }
};