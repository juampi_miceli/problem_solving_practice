#include <stdlib.h>
#include <stdio.h>
#include <unordered_set>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode *getIntersectionNodeEasy(ListNode *headA, ListNode *headB) {
        std::unordered_set<ListNode*> nodes;
        
        ListNode *current = headA;
        while(current != nullptr){
            nodes.insert(current);
            current = current->next;
        }
        current = headB;
        while (current != nullptr && !nodes.count(current))
        {
            current = current->next;
        }
        return current;
    }
    
    int getLength(ListNode *head){
        int res = 0;
        
        while(head != nullptr){
            res++;
            head = head->next;
        }
        return res;
    }
    
    ListNode *getIntersectionNodeMedium(ListNode *headA, ListNode *headB){
        int N1 = getLength(headA);
        int N2 = getLength(headB);
        
        ListNode *pA = headA;
        ListNode *pB = headB;
        
        while(N1 > N2){
            pA = pA->next;
            N1--;
        }
        
        while(N2 > N1){
            pB = pB->next;
            N2--;
        }
        
        while(pA != pB){
            pA = pA->next;
            pB = pB->next;
        }
        return pA;
    }
};