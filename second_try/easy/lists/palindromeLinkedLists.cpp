#include <stdlib.h>
#include <stdio.h>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    bool equals(ListNode* l1, ListNode* l2){
        ListNode* currentL1 = l1;
        ListNode* currentL2 = l2;

        while(currentL1 != nullptr && currentL2 != nullptr){
            if(currentL1->val != currentL2->val){
                return false;
            }
            currentL1 = currentL1->next;
            currentL2 = currentL2->next;
        }
        return currentL2 == nullptr;
    }
    
    ListNode* reverseList(ListNode* head) {
        if(head == nullptr){
            return head;
        }
        ListNode* prevNode = nullptr;
        ListNode* nextNode = head->next;
        while(nextNode != nullptr){
            head->next = prevNode;
            prevNode = head;
            head = nextNode;
            nextNode = head->next;
        }
        head->next = prevNode;
        return head;
    }

    ListNode* half(ListNode* head){
        ListNode* slow = head;
        ListNode* fast = head;

        while(fast != nullptr && fast->next != nullptr){
            slow = slow->next;
            fast = fast->next->next;
        }
        if(fast != nullptr){
            return slow->next;
        }
        return slow;
    }

    bool isPalindrome(ListNode* head) {
        return equals(head, reverseList(half(head)));
    }
};