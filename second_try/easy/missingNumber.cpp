#include <stdlib.h>
#include <stdio.h>

class Solution {
public:
    int sum(vector<int>& nums){
        int res = 0;
        for(int e : nums){
            res += e;
        }
        return res;
    }
    int missingNumber(vector<int>& nums) {
        int N = nums.size();
        int goal = ((N+1) * N)/2;
        return goal - sum(nums);
        
    }
};