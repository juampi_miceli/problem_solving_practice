#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <unordered_map>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    int countOccurrences(TreeNode* root, std::unordered_map<int, int> &occurrences, int maxTimes){
        if(root == nullptr){
            return maxTimes;
        }
        int val = root->val;
        if(occurrences.count(val)){
            occurrences[val]++;
            if(occurrences[val] > maxTimes){
                maxTimes = occurrences[val];
            }
        }else{
            occurrences[val] = 1;
        }
        int maxLeft = countOccurrences(root->left, occurrences, maxTimes);
        int maxRight = countOccurrences(root->right, occurrences, maxTimes);
        return std::max(maxTimes, std::max(maxLeft, maxRight));
    }
    
    std::vector<int> maxOccurrences(std::unordered_map<int, int> occurrences, int maxTimes){
        std::vector<int> res;
        for(std::pair<int, int> p : occurrences){
            if(p.second == maxTimes){
                res.push_back(p.first);
            }
        }
        return res;
    }
    
    std::vector<int> findMode(TreeNode* root) {
        std::unordered_map<int, int> valOccurrences;
        int maxTimes = countOccurrences(root, valOccurrences, 1);
        return maxOccurrences(valOccurrences, maxTimes);
    }
};