#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    bool hasTwoSons(TreeNode* root){
        return root != nullptr && root->left != nullptr && root->right != nullptr;
    }
    
    bool hasOnlyLeftSon(TreeNode* root){
        return root != nullptr && root->left != nullptr && root->right == nullptr;
    } 
    
    bool hasOnlyRightSon(TreeNode* root){
        return root != nullptr && root->left == nullptr && root->right != nullptr;
    }
    
    int minDepthAux(TreeNode* root, int currentHeight){
        if(hasTwoSons(root)){
            return std::min(minDepthAux(root->left, currentHeight+1), minDepthAux(root->right, currentHeight+1));
        }else if(hasOnlyLeftSon(root)){
            return minDepthAux(root->left, currentHeight+1);
        }else if(hasOnlyRightSon(root)){
            return minDepthAux(root->right, currentHeight+1);
        }else{
            return currentHeight+1;
        }
        
    }
    
    int minDepth(TreeNode* root) {
        if(root == nullptr){
            return 0;
        }
        return minDepthAux(root, 0);
    }
};