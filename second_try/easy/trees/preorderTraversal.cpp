#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    void preorderTraversalAux(TreeNode* root, std::vector<int> &v){
        if(root == nullptr){
            return;
        }
        v.push_back(root->val);
        preorderTraversalAux(root->left, v);
        preorderTraversalAux(root->right, v);
        return;
    }
    
    std::vector<int> preorderTraversal(TreeNode* root) {
        std::vector<int> res;
        preorderTraversalAux(root, res);
        return res;
    }
};