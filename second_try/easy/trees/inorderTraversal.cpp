#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 
class Solution {
public:
    
    void inorderTraversalAux(std::vector<int>& v, TreeNode* root){
        if(root == nullptr){
            return;
        }
        inorderTraversalAux(v, root->left);
        v.push_back(root->val);
        inorderTraversalAux(v, root->right);
        return;
    }
    
    std::vector<int> inorderTraversal(TreeNode* root) {
        std::vector<int> res;
        inorderTraversalAux(res, root);
        return res;
        
    }
};