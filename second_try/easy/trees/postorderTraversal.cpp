#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    void postorderTraversalAux(TreeNode* root, std::vector<int> &v){
        if(root == nullptr){
            return;
        }
        
        postorderTraversalAux(root->left, v);
        postorderTraversalAux(root->right, v);
        v.push_back(root->val);
        return;
    }
    
    std::vector<int> postorderTraversal(TreeNode* root) {
        std::vector<int> res;
        postorderTraversalAux(root, res);
        return res;
    }
};