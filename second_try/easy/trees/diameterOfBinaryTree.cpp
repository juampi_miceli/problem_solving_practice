#include <stdlib.h>
#include <stdio.h>
#include <unordered_map>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    std::unordered_map<TreeNode *, int> nodesHeight;
    
    int getHeight(TreeNode *node){
        if(node == 0){
            return 0;
        }
        
        if(!nodesHeight.count(node)){
            nodesHeight[node] = std::max(getHeight(node->left), getHeight(node->right)) + 1;
        }
        
        return nodesHeight[node];
    }
    
    int diameterOfBinaryTree(TreeNode* root) {
        if(root == nullptr){
            return 0;
        }
        
        int leftHeight = getHeight(root->left);
        int rightHeight = getHeight(root->right);
        int leftDiameter = diameterOfBinaryTree(root->left);
        int rightDiameter = diameterOfBinaryTree(root->right);
        
        return std::max(std::max(leftDiameter, rightDiameter), leftHeight + rightHeight);
    }
};