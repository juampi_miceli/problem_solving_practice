#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    void invertTreeAux(TreeNode* &newTree, TreeNode *oldTree){
        if(oldTree == nullptr){
            return;
        }
        newTree = new TreeNode(oldTree->val);
        invertTreeAux(newTree->left, oldTree->right);
        invertTreeAux(newTree->right, oldTree->left);
    }
    
    TreeNode* invertTree(TreeNode* root) {
        TreeNode* res = nullptr;
        invertTreeAux(res, root);
        return res;
    }
};