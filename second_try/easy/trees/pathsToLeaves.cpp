#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    void binaryTreePathsAux(TreeNode* root, std::vector<std::string> &paths, std::string currentPath){
        if(root == nullptr){
            return;
        }
        std::string newPath = currentPath + std::to_string(root->val);
        if(root->left == nullptr && root->right == nullptr){
            paths.push_back(newPath);
            return;
        }
        binaryTreePathsAux(root->left, paths, newPath+"->");
        binaryTreePathsAux(root->right, paths, newPath+"->");
    }
    
    std::vector<std::string> binaryTreePaths(TreeNode* root) {
        std::vector<std::string> res;
        binaryTreePathsAux(root, res, "");
        return res;
    }
};