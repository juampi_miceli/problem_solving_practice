#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    bool between(TreeNode *root, int min, int max){
        return min <= root->val && root->val <= max;
    }
    
    TreeNode* lowestCommonAncestorAux(TreeNode* root, TreeNode* p, TreeNode* q, int min, int max){
        if(root == nullptr){
            return nullptr;
        }
        if(between(root, min, max)){
            return root;
        }
        if(root->val < min){
            return lowestCommonAncestorAux(root->right, p, q, min, max);
        }
        return lowestCommonAncestorAux(root->left, p, q, min, max);  
    }
    
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        int minVal = std::min(p->val, q->val);
        int maxVal = std::max(p->val, q->val);
        return lowestCommonAncestorAux(root, p, q, minVal, maxVal);
    }
};