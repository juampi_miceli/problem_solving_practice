#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    int RIGHT = 1;
    int LEFT = 0;
    
    int sumOfLeftLeavesAux(TreeNode* root, int parent){
        if(root == nullptr){
            return 0;
        }
        if(root->left == nullptr && root->right == nullptr){
            if(parent == LEFT){
                return root->val;
            }
            return 0;
        }
        return sumOfLeftLeavesAux(root->left, LEFT) + sumOfLeftLeavesAux(root->right, RIGHT);
    }
    
    int sumOfLeftLeaves(TreeNode* root) {
        return sumOfLeftLeavesAux(root, RIGHT);
    }
};