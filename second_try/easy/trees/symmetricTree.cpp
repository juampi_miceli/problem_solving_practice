#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    bool isSymmetricAux(TreeNode* left, TreeNode* right){
        if(left == nullptr){
            return right == nullptr;
        }
        if(right == nullptr){
            return false;
        }
        return left->val == right->val && isSymmetricAux(left->left, right->right) && isSymmetricAux(left->right, right->left);
    }
    
    bool isSymmetric(TreeNode* root) {
        if(root == nullptr){
            return true;
        }
        return isSymmetricAux(root->left, root->right);
    }
};