#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    int maxDepthAux(TreeNode *root, int currentHeight){
        if(root == nullptr){
            return currentHeight;
        }
        return std::max(maxDepthAux(root->left, currentHeight+1), maxDepthAux(root->right, currentHeight+1));
    }
    
    int maxDepth(TreeNode* root) {
        return maxDepthAux(root, 0);
    }
    
    bool isBalanced(TreeNode* root) {
        if(root == nullptr){
            return true;
        }
        return std::abs(maxDepth(root->left) - maxDepth(root->right)) <= 1 && isBalanced(root->left) && isBalanced(root->right); 
    }
};