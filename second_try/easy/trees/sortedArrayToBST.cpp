#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    void sortedArrayToBSTAux(TreeNode* &root, vector<int>& nums, int low, int high){
        if(low == high){
            return;
        }
        int middle = (low + high) / 2;
        root = new TreeNode(nums[middle]);
        sortedArrayToBSTAux(root->left, nums, low, middle);
        sortedArrayToBSTAux(root->right, nums, middle+1, high);
    }
        
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        TreeNode* res = nullptr;
        sortedArrayToBSTAux(res, nums, 0, nums.size());
        return res;
    }
};
