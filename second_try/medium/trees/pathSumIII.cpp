#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    int numberOfPathsFromRoot(TreeNode *root, long targetSum){
        if(root == nullptr){
            return 0;
        }
        
        int current = targetSum - root->val == 0 ? 1 : 0;
        
        return numberOfPathsFromRoot(root->left, targetSum - root->val) + numberOfPathsFromRoot(root->right, targetSum - root->val) + current;
        
    }
    
    int pathSum(TreeNode* root, int targetSum) {
        
        std::vector<TreeNode *> nextMoves;
        
        nextMoves.push_back(root);
        
        int numberOfPaths = 0;
        
        while(!nextMoves.empty()){
            TreeNode *currentNode = nextMoves.back();
            nextMoves.pop_back();
            if(currentNode == nullptr){
                continue;
            }
            
            numberOfPaths += numberOfPathsFromRoot(currentNode, targetSum);
            
            nextMoves.push_back(currentNode->left);
            nextMoves.push_back(currentNode->right);
        }
        
        return numberOfPaths;
    }
};