#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <queue>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    
    typedef std::pair<TreeNode *, int> NodeWithLevel; // (node, level)
    
    std::vector<int> rightSideView(TreeNode* root) {
        /*if(root != nullptr){
            return {};
        }*/
        std::vector<int> rightSideNodes;
        std::queue<NodeWithLevel> nodesQueue;
        
        nodesQueue.push(NodeWithLevel(root, 0));
        
        while(!nodesQueue.empty()){
            NodeWithLevel currentNodeWithLevel = nodesQueue.front();
            TreeNode *currentNode = currentNodeWithLevel.first;
            int currentLevel = currentNodeWithLevel.second;
            nodesQueue.pop();
            
            if(currentNode == nullptr){
                continue;
            }
            
            if(rightSideNodes.size() > currentLevel){
                rightSideNodes[currentLevel] = currentNode->val;
            }else{
                rightSideNodes.push_back(currentNode->val);
            }
            nodesQueue.push(NodeWithLevel(currentNode->left, currentLevel + 1));
            nodesQueue.push(NodeWithLevel(currentNode->right, currentLevel + 1));
        }
        
        return rightSideNodes;
    }
};