#include <stdlib.h>
#include <stdio.h>
#include <unordered_map>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    
    bool fillAncestors(TreeNode* root, int target, std::unordered_map<TreeNode *, bool> &ancestors){
        if(root == nullptr){
            return false;
        }
        if(root->val == target){
            ancestors[root] = true;
            return true;
        }
        
        if(fillAncestors(root->left, target, ancestors) || fillAncestors(root->right, target, ancestors)){
            ancestors[root] = true;
            return true;
        }
        return false;
    }
    
    bool findLCA(TreeNode* root, int target, TreeNode* &lca, std::unordered_map<TreeNode *, bool> &ancestors){
        if(root == nullptr){
            return false;
        }
        if(root->val == target){
            if(lca == nullptr && ancestors.count(root)){
                lca = root;
            }
            return true;
        }
        if(findLCA(root->left, target, lca, ancestors) || findLCA(root->right, target, lca, ancestors)){
            if(lca == nullptr && ancestors.count(root)){
                lca = root;
            }
            return true;
        }
        return false;
    }
    
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        std::unordered_map<TreeNode *, bool> pAncestors;
        
        fillAncestors(root, p->val, pAncestors);
        
        TreeNode *lca = nullptr;
        findLCA(root, q->val, lca, pAncestors);
        return lca;
        
    }
};