#include <stdlib.h>
#include <stdio.h>
#include <vector>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class BSTIterator {
public:
    BSTIterator(TreeNode* root) {
        while(root != nullptr){
            nodes.push_back(root);
            root = root->left;
        }
    }
    
    int next() {
        TreeNode *node = nullptr;
        if(nodes.size() > 0){
            node = nodes.back();
            nodes.pop_back();
        }
        int val = node->val;
        node = node->right;
        while(node != nullptr){
            nodes.push_back(node);
            node = node->left;
        }
        return val;
    }
    
    bool hasNext() {
        return !nodes.empty();
    }
    
private:
    std::vector<TreeNode *> nodes;
};