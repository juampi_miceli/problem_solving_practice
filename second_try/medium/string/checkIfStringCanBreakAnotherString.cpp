#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>

class Solution {
public:
        
    bool checkIfCanBreak(std::string s1, std::string s2) {
        std::vector<int> s1Freq('z' - 'a' + 1, 0);
        std::vector<int> s2Freq('z' - 'a' + 1, 0);
        
        for(int i = 0; i < s1.size(); i++){
            char c1 = s1[i];
            char c2 = s2[i];
            s1Freq[c1 - 'a']++;
            s2Freq[c2 - 'a']++;
        }
        
        int p1 = 0;
        int p2 = 0;
        
        while(p1 < s1Freq.size() && s1Freq[p1] == 0){
            p1++;
        }
        
        while(p2 < s2Freq.size() && s2Freq[p2] == 0){
            p2++;
        }
        
        bool smaller1 = false;
        bool smaller2 = false;
        int sum1 = 0;
        int sum2 = 0;
        
        for(int i = 0; i < s1Freq.size(); i++){
            sum1 += s1Freq[i];
            sum2 += s2Freq[i];
            
            if(sum1 < sum2){
                if(smaller2){
                    return false;
                }
                smaller1 = true;
            }else if(sum2 < sum1){
                if(smaller1){
                    return false;
                }
                smaller2 = true;
            }
        }
        return true;
    }
};