#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <unordered_map>

class Solution {
public:
    string getHint(string secret, string guess) {
        std::unordered_map<char, int> lettersCountInSecret;
        
        for(char c : secret){
            if(lettersCountInSecret.count(c)){
                lettersCountInSecret[c]++;
            }else{
                lettersCountInSecret[c] = 1;
            }
        }
        
        int bulls = 0;
        int cows = 0;
        
        for(int i = 0; i < guess.size(); i++){
            if(secret[i] == guess[i]){
                bulls++;
                lettersCountInSecret[secret[i]]--;
            }
        }
        for(int i = 0; i < guess.size(); i++){
            if(secret[i] != guess[i] && lettersCountInSecret[guess[i]] > 0){
                cows++;
                lettersCountInSecret[guess[i]]--;
            }
        }
        return to_string(bulls) + "A" + to_string(cows) + "B"; 
    }
};