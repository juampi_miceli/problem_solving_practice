#include <stdlib.h>
#include <stdio.h>
#include <vector>


class Solution {
public:
    
    int vectorSum(std::vector<int>& nums){
        int sum = 0;
        for(int e : nums){
            sum += e;
        }

        return sum;
    }

    bool canPartition(std::vector<std::vector<int>> &memo, const std::vector<int>& nums, int target, int i){
        if(target < 0){
            return false;
        }
        if(memo[target][i] == -1){
            memo[target][i] = canPartition(memo, nums, target-nums[i], i+1) || canPartition(memo, nums, target, i+1);
        }

        return memo[target][i];
    }

    bool canPartition(const std::vector<int>& nums, int target){
        std::vector<std::vector<int>> memo(target+1, std::vector<int>(nums.size() + 1, -1));

        for(int i = 0; i < memo.size(); i++){
            memo[i][memo[0].size() - 1] = 0;
        }

        for(int i = 0; i < memo[0].size(); i++){
            memo[0][i] = 1;
        }

        return canPartition(memo, nums, target, 0);
    }

    bool canPartition(std::vector<int>& nums) {
        int sum = vectorSum(nums);
        if((sum % 2) == 1){
            return false;
        }

        return canPartition(nums, sum/2);

    }
};