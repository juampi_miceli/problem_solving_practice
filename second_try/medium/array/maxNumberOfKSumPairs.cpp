#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int maxOperations(std::vector<int>& nums, int k) {
        std::sort(nums.begin(), nums.end());
        
        int pairs = 0;
        
        int left = 0;
        int right = nums.size() - 1;
        while(right > left){
            int currentSum = nums[right] + nums[left];
            if(currentSum == k){
                pairs++;
                right--;
                left++;
            }else if(currentSum > k){
                right--;
            }else{
                left++;
            }
        }
        return pairs;
    }
};