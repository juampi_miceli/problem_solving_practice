#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    int RIGHT = 1;
    int LEFT = -1;
    int followBall(std::vector<std::vector<int>> &grid, int ballPosition){
        for(int i = 0; i < grid.size(); i++){
            if(grid[i][ballPosition] == RIGHT){
                if((ballPosition == grid[0].size() - 1) || (grid[i][ballPosition + 1] == LEFT)){
                    return -1;
                }
                ballPosition++;
                continue;
            }
            if((ballPosition == 0) || (grid[i][ballPosition - 1] == RIGHT)){
                return -1;
            }
            ballPosition--;
        }
        
        return ballPosition;
    }
    
    std::vector<int> findBall(std::vector<std::vector<int>>& grid) {
        std::vector<int> ballPositions(grid[0].size());
        
        for(int i = 0; i < grid[0].size(); i++){
            ballPositions[i] = followBall(grid, i);
        }
        
        return ballPositions;
    }
};