#include <stdlib.h>
#include <stdio.h>
#include <vector>

class Solution {
public:
    
    int id(int M, std::vector<int> pos){
        return pos[0] * M + pos[1];
    }
    
    std::vector<int> pos(int M, int position){
        return {position / M, position % M};
    }
    bool searchMatrix(std::vector<std::vector<int>>& matrix, int target) {
        int M = matrix[0].size();
        int N = matrix.size();
        int left = 0;
        int right = id(M, {N, 0});
        
        while(right > left){
            int middle = (right + left) / 2;
            std::vector<int> position = pos(M, middle);
            int elem = matrix[position[0]][position[1]];
            if(elem == target){
                return true;
            }else if(elem > target){
                right = middle;
            }else{
                left = middle + 1;
            }
        }
        
        return false;
    }
};