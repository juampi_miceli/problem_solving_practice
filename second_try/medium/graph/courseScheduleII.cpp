#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <queue>
#include <unordered_set>
class Solution {
public:
    
    void updateInDegree(int currentCourse, const std::vector<std::vector<int>> &graph, std::vector<int> &inDegree){
        for(int child : graph[currentCourse]){
            inDegree[child]--;
        }
        return;
    }
    
    void addNextCourses(int currentCourse, const std::vector<std::vector<int>> &graph, std::queue<int> &coursesQueue, const std::vector<int> &inDegree){
        for(int child : graph[currentCourse]){
            if(inDegree[child] == 0){
                coursesQueue.push(child);
            }
        }
        return;
    }
        
    std::vector<int> findOrder(int numCourses, std::vector<std::vector<int>>& prerequisites) {
        std::vector<int> inDegree(numCourses, 0);
        std::vector<std::vector<int>> graph(numCourses);
        
        for(int i = 0; i < prerequisites.size(); i++){
            inDegree[prerequisites[i][0]]++;
            graph[prerequisites[i][1]].push_back(prerequisites[i][0]);
        }
        
        std::vector<int> coursesInOrder;
        std::queue<int> coursesQueue;
        
        for(int i = 0; i < inDegree.size(); i++){
            if(inDegree[i] == 0){
                coursesQueue.push(i);
            }
        }
        
        while(!coursesQueue.empty()){
            int currentCourse = coursesQueue.front();
            coursesQueue.pop();
            
            coursesInOrder.push_back(currentCourse);
            updateInDegree(currentCourse, graph, inDegree);
            addNextCourses(currentCourse, graph, coursesQueue, inDegree);
        }
        
        if(coursesInOrder.size() == numCourses){
            return coursesInOrder;
        }
        return {};
    }
};