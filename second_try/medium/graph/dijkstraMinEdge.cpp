#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <vector>

class Solution {
public:
    
    typedef std::vector<int> Pos;
    typedef std::pair<int, Pos> Pair;

    std::vector<std::vector<int>> getNeighbors(int N, int M, std::vector<int> &current){
        std::vector<std::vector<int>> neighbors;
        std::vector<std::vector<int>> possibleNeighbors = {
            {current[0] + 1, current[1]},
            {current[0] - 1, current[1]},
            {current[0], current[1] + 1},
            {current[0], current[1] - 1}
        };

        for(std::vector<int> &possibleNeighbor : possibleNeighbors){
            if(possibleNeighbor[0] >= 0 && possibleNeighbor[1] >= 0 && possibleNeighbor[0] < N && possibleNeighbor[1] < M){
                neighbors.push_back(possibleNeighbor);
            }
        }

        return neighbors;
    }

    int distance(std::vector<Pos> &graph, Pos &from, Pos &to){
        std::priority_queue<Pair, std::vector<Pair>, std::greater<Pair>> minHeap;
        int N = graph.size();
        int M = graph[0].size();

        std::vector<Pos> maxMinEdge(N, Pos(M, INT_MIN));
        maxMinEdge[from[0]][from[1]] = graph[from[0]][from[1]];

        std::vector<std::vector<bool>> visited(N, std::vector<bool>(M, false));
        visited[from[0]][from[1]] = true;

        std::vector<Pos> neighbors = getNeighbors(N, M, from);

        for(Pos &neighbor : neighbors){
            minHeap.push(Pair(std::max(graph[from[0]][from[1]], graph[neighbor[0]][neighbor[1]]), neighbor));
        }

        while(!minHeap.empty()){
            Pair current = minHeap.top();
            int currentMaxMinEdge = current.first;
            Pos currentPos = current.second;
            minHeap.pop();

            if(visited[currentPos[0]][currentPos[1]]){
                continue;
            }
            visited[currentPos[0]][currentPos[1]] = true;
            maxMinEdge[currentPos[0]][currentPos[1]] = currentMaxMinEdge;
            neighbors = getNeighbors(N, M, currentPos);
            for(Pos &neighbor : neighbors){
                minHeap.push(Pair(std::max(currentMaxMinEdge, graph[neighbor[0]][neighbor[1]]), neighbor));
            }
        }

        return maxMinEdge[to[0]][to[1]];
    }
    
    int swimInWater(std::vector<std::vector<int>>& grid) {
        int N = grid.size();
        int M = grid[0].size();
        std::vector<int> from = {0,0};
        std::vector<int> to = {N-1,M-1};
        return distance(grid, from, to);
    }
};