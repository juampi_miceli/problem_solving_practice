#include <stdlib.h>
#include <stdio.h>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        if(head == nullptr || head->next == nullptr){
            return head;
        }
        ListNode *slow = head;
        ListNode *fast = slow->next;
        head = fast;
        
        while(slow != nullptr && slow->next != nullptr){
            if(fast->next != nullptr && fast->next->next != nullptr){
                slow->next = fast->next->next;
            }else{
                slow->next = fast->next;
            }
            ListNode *aux = fast->next;
            fast->next = slow;
            fast = slow->next;
            slow = aux;
        }
        
        return head;
    }
};