#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <unordered_map>
#include <unordered_set>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    
    typedef std::pair<TreeNode *, int> NodeInformation;
    
    void addParents(std::unordered_map<TreeNode *, TreeNode *> &parents, TreeNode *node, TreeNode * parent){
        if(node == nullptr){
            return;
        }
        parents[node] = parent;
        addParents(parents, node->left, node);
        addParents(parents, node->right, node);
    }
    
    void addNeighbor(std::queue<NodeInformation> &nextMoves, TreeNode *node, int distance, std::unordered_set<TreeNode *> &visited){
        if(node == nullptr){
            return;
        }
        if(visited.count(node)){
            return;
        }
        nextMoves.push(NodeInformation(node, distance+1));
    }
    
    void addNeighbors(std::queue<NodeInformation> &nextMoves, std::unordered_map<TreeNode *, TreeNode *> &parents, TreeNode *node, int distance, std::unordered_set<TreeNode *> &visited){
        addNeighbor(nextMoves, parents[node], distance, visited);
        addNeighbor(nextMoves, node->left, distance, visited);
        addNeighbor(nextMoves, node->right, distance, visited);
    }
    
    std::vector<int> distanceK(TreeNode* root, TreeNode* target, int k) {
        std::unordered_map<TreeNode *, TreeNode *> parents;
        
        addParents(parents, root, nullptr);
        
        std::queue<NodeInformation> nextMoves;
        nextMoves.push(NodeInformation(target, 0));
        
        std::vector<int> distanceKValues;
        std::unordered_set<TreeNode *> visited;
        
        
        while(!nextMoves.empty()){
            NodeInformation nodeInformation = nextMoves.front();
            int currentDistance = nodeInformation.second;
            TreeNode *currentNode = nodeInformation.first;
            nextMoves.pop();
            visited.insert(currentNode);
            
            if(currentDistance > k){
                break;
            }
            if(currentDistance == k){
                distanceKValues.push_back(currentNode->val);
            }
            
            addNeighbors(nextMoves, parents, currentNode, currentDistance, visited);
            
        }
        
        return distanceKValues;
    }
};