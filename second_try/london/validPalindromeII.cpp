#include <stdlib.h>
#include <stdio.h>
#include <string>

// https://leetcode.com/discuss/interview-question/1895419/Meta-or-Phone-Screen-or-London

class Solution {
public:
    
    bool validPalindrome(std::string s, int start, int end){
        while(start < end){
            if(s[start] == s[end]){
                start++;
                end--;
                continue;
            }
            return false;
        }
        
        return true;
        
    }
    
    bool validPalindrome(string s) {        
        int left = 0;
        int right = s.size()-1;
        
        while(left < right){
            if(s[left] == s[right]){
                left++;
                right--;
                continue;
            }
            return validPalindrome(s, left+1, right) || validPalindrome(s, left, right-1);
        }
        return true;
    }
};