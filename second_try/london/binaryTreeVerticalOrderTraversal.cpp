#include <stdlib.h>
#include <stdio.h>
#include <vector>

typedef struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
} TreeNode;

using namespace std;

typedef std::pair<int, int> NodeData;

void fillNodesData(std::vector<NodeData> &nodesData, TreeNode *root, int column, std::vector<int> &width){
    NodeData nodeData = NodeData(root->val, column);
    nodesData.push_back(nodeData);
    if(root->left != nullptr){
        width[0] = std::min(column-1, width[0]);
        fillNodesData(nodesData, root->left, column-1, width);
    }
    if(root->right != nullptr){
        width[1] = std::max(column+1, width[1]);
        fillNodesData(nodesData, root->right, column+1, width); 
    }

}

std::vector<std::vector<int>> verticalOrder(TreeNode *root){
    std::vector<NodeData> nodesData;
    std::vector<int> borders = {0,0};
    fillNodesData(nodesData, root, 0, borders);
    int width = borders[1] - borders[0] + 1;
    std::vector<std::vector<int>> res(width, std::vector<int>());
    int offset = -1 * borders[0];
    for(int i = 0; i < nodesData.size(); i++){
        int column = nodesData[i].second;
        int value = nodesData[i].first;
        res[offset + column].push_back(value); 
    }

    return res;
}
