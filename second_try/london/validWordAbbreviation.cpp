#include <stdlib.h>
#include <stdio.h>
#include <string>

class Solution{

public:

    int getNumber(std::string &s){
        int N = s.size();
        int numberSize = 0;
        while(numberSize < N && std::isdigit(s[N-1-numberSize])){
            numberSize++;
        }

        int res = std::stoi(std::string(s.begin() + N - numberSize, s.end()));
        while(numberSize > 0){
            s.pop_back();
        }
        return res;
    }

    bool validWordAbbreviation(std::string word, std::string abbr){
        int wordSize = word.size()-1;
        int abbrSize = abbr.size()-1;

        while(!word.empty() && !abbr.empty()){
            char currentChar = abbr[abbrSize];
            if(currentChar == word[wordSize]){
                word.pop_back();
                abbr.pop_back();
                wordSize--;
                abbrSize--;
            }else if(std::isdigit(currentChar)){
                int totalSkips = getNumber(abbr);
                while(totalSkips > 0){
                    word.pop_back();
                }
                wordSize -= totalSkips;
                abbrSize = abbr.size()-1;
            }else{
                return false;
            }
        }
        return word.empty() && abbr.empty();
    }

};