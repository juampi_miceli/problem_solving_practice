#include <stdlib.h>
#include <stdio.h>

//https://leetcode.com/discuss/interview-question/1926418/Meta-or-Phone-screen-or-London

class Solution {
public:
    
    double positivePow(double x, long n){
        if(n == 0){
            return 1;
        }
        if(n % 2 == 0){
            return positivePow(x*x, n/2);
        }
        return positivePow(x*x, n/2) * x;
    }
    
    double negativePow(double x, long n){
        return 1/positivePow(x, n * -1);
    }
    
    double myPow(double x, int n) {
        return n >= 0 ? positivePow(x, n) : negativePow(x, n);
    }
};