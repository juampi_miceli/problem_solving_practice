#include <stdlib.h>
#include <stdio.h>
#include <string>

//https://leetcode.com/discuss/interview-question/1926418/Meta-or-Phone-screen-or-London

class Solution {
public:
    std::string removeDuplicates(std::string s) {
        std::string res = "";
        
        for(int i = 0; i < s.size(); i++){
            if(!res.empty() && res[res.size()-1] == s[i]){
                res.pop_back();
                continue;
            }
            res.push_back(s[i]);
        }
        return res;
    }
};