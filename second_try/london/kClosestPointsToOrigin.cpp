#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <queue>

// https://leetcode.com/discuss/interview-question/1895419/Meta-or-Phone-Screen-or-London


class Solution {
public:
    
    typedef std::vector<int> Point;
    
    typedef std::pair<int, Point> PointInformation;
    
    class Compare{
    public:
        bool operator() (PointInformation pointInfo1, PointInformation pointInfo2){
            return pointInfo1.first < pointInfo2.first; 
        }
    };
    
    
    
    std::vector<std::vector<int>> kClosest(std::vector<std::vector<int>>& points, int k) {
        std::priority_queue<PointInformation,std::vector<PointInformation>,Compare> maxHeap;
        
        for(std::vector<int> &point : points){
            int pseudoDistance = point[0]*point[0] + point[1]*point[1];
            maxHeap.push(PointInformation(pseudoDistance, point));
            if(maxHeap.size() == k+1){
                maxHeap.pop();
            }
        }
        
        std::vector<std::vector<int>> res;
        
        for(int i = 0; i < k; i++){
            PointInformation pointInfo = maxHeap.top();
            maxHeap.pop();
            res.push_back(pointInfo.second);
        }
        
        return res;
    }
};