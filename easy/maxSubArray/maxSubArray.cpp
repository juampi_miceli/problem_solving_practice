#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <random>
#include <bits/stdc++.h>
#include <limits.h>
#include <string.h>
#include <stack>

int maxSubArray(std::vector<int> &nums)
{
    int winner = nums[0];
    int currentSum = 0;

    for (int i = 0; i < nums.size(); i++)
    {
        currentSum = std::max(nums[i], currentSum + nums[i]);
        winner = std::max(winner, currentSum);
    }

    return winner;
}

int main()
{
    std::vector<int> nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    maxSubArray(nums);
    return 0;
}