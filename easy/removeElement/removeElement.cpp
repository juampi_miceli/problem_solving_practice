#include <stdlib.h>
#include <vector>

int nextTrashPointer(const std::vector<int> nums, int val, int current)
{
    current--;

    while (current >= 0)
    {
        if (nums[current] != val)
        {
            break;
        }
        current--;
    }

    return current;
}

int removeElement(std::vector<int> &nums, int val)
{
    if (nums.empty())
    {
        return 0;
    }
    int N = nums.size();

    int trashPointer = nextTrashPointer(nums, val, N);

    for (int i = 0; i < trashPointer; i++)
    {
        if (nums[i] == val)
        {
            int aux = nums[i];
            nums[i] = nums[trashPointer];
            nums[trashPointer] = aux;
            trashPointer = nextTrashPointer(nums, val, trashPointer);
        }
    }

    return trashPointer + 1;
}