#include <stdlib.h>
#include <string>

int lengthOfLastWord(std::string s)
{
    if (s.empty())
    {
        return 0;
    }
    int res = 0;
    int lastNonSpacePos = s.size() - 1;

    while (lastNonSpacePos >= 0 && s[lastNonSpacePos] == ' ')
    {
        lastNonSpacePos--;
    }

    while (lastNonSpacePos >= 0 && s[lastNonSpacePos] != ' ')
    {
        res++;
        lastNonSpacePos--;
    }

    return res;
}