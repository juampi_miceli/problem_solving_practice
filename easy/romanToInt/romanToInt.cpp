#include <bits/stdc++.h>

std::unordered_map<char, int> romanMapping = {
    {'I', 1},
    {'V', 5},
    {'X', 10},
    {'L', 50},
    {'C', 100},
    {'D', 500},
    {'M', 1000}};

std::string getNextToken(std::string s, int pointer) {
    size_t N = s.size();
    std::string res = "";

    while (pointer < N) {
        if (res.empty()) {
            res.push_back(s[pointer++]);
            continue;
        }
        if (romanMapping[s[pointer - 1]] > romanMapping[s[pointer]]) {
            break;
        }
        res.push_back(s[pointer++]);
    }

    return res;
}

bool allSameDigit(std::string token) {
    char baseChar = token[0];
    for (char c : token) {
        if (c != baseChar) {
            return false;
        }
    }

    return true;
}

int tokenToValue(std::string token) {
    if (allSameDigit(token)) {
        return romanMapping[token[0]] * token.size();
    }
    return romanMapping[token[1]] - romanMapping[token[0]];
}

int romanToInt(std::string s) {
    size_t N = s.size();
    int result = 0;
    size_t pointer = 0;

    while (pointer < N) {
        std::string token = getNextToken(s, pointer);
        int value = tokenToValue(token);
        result += value;
        printf("token: %s\tvalue: %d\n", token.c_str(), value);
        pointer += token.size();
    }
    return result;
}

int main() {
    printf("result: %d", romanToInt("MMMDCCXXIV"));
    return 0;
}