#include <stdlib.h>
#include <stdio.h>

int mySqrt(int x)
{

    if (x == 1)
    {
        return x;
    }

    long left = 0;
    long right = x;
    long mid = (right + left) / 2;

    while (left < right - 1)
    {
        if (mid * mid < x)
        {
            left = mid;
            mid = (right + left) / 2;
        }
        else if (mid * mid > x)
        {
            right = mid;
            mid = (right + left) / 2;
        }
        else
        {
            return mid;
        }
    }

    return left;
}