#include <stdlib.h>
#include <stdio.h>

typedef struct ListNode
{
    int val;
    ListNode *next;

    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
} ListNode;

ListNode *deleteDuplicates(ListNode *head)
{
    if (head == nullptr || head->next == nullptr)
    {
        return head;
    }

    if (head->val == head->next->val)
    {
        ListNode *aux = head->next;
        head->next = aux->next;
        deleteDuplicates(head);
    }
    else
    {
        deleteDuplicates(head->next);
    }

    return head;
}