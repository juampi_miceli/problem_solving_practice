#include <bits/stdc++.h>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

int recursiveMaxDepth(TreeNode *root, int level) {
    if (root == nullptr) return level;
    return std::max(recursiveMaxDepth(root->left, level + 1), recursiveMaxDepth(root->right, level + 1));
}

int maxDepth(TreeNode *root) {
    return recursiveMaxDepth(root, 0);
}

int main() {
    TreeNode *seven = new TreeNode(7);
    TreeNode *fifteen = new TreeNode(15);
    TreeNode *twenty = new TreeNode(20, fifteen, seven);
    TreeNode *nine = new TreeNode(9);
    TreeNode *three = new TreeNode(3, nine, twenty);
    printf("%d", maxDepth(three));
    return 0;
}