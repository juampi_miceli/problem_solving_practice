#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <random>
#include <bits/stdc++.h>
#include <limits.h>

int getMagnitud(int x)
{
    int magnitud = 0;

    int resto = x / pow(10, magnitud);

    while (resto > 0)
    {
        magnitud++;

        resto = x / pow(10, magnitud);
    }

    return magnitud;
}

int getIthDigitFromRight(int x, int i)
{
    return (x / (int)pow(10, i)) % 10;
}

bool isPalindrome(int x)
{
    if (x < 0)
    {
        return false;
    }
    int magnitud = getMagnitud(x);

    int pLeft = 0;
    int pRight = magnitud - 1;

    while (pLeft < pRight)
    {
        int nRight = getIthDigitFromRight(x, pLeft);
        int nLeft = getIthDigitFromRight(x, pRight);
        if (nLeft != nRight)
        {
            return false;
        }
        pLeft++;
        pRight--;
    }

    return true;
}

int main()
{
    std::vector<int> numbers = {333, 10, 154, 545, 50005, -121, 5000, 90704004};
    for (int n : numbers)
    {
        if (isPalindrome(n))
        {
            printf("number: %d, palindrome: TRUE\n", n);
            continue;
        }
        printf("number: %d, palindrome: FALSE\n", n);
    }
    return 0;
}