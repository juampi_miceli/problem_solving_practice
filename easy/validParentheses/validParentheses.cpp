#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <random>
#include <bits/stdc++.h>
#include <limits.h>
#include <string.h>
#include <stack>

bool isOpening(char c)
{
    switch (c)
    {
    case '(':
    case '[':
    case '{':
        return true;
    default:
        return false;
    }
}

bool isClosing(char c)
{
    switch (c)
    {
    case ')':
    case ']':
    case '}':
        return true;
    default:
        return false;
    }
}

bool isValidClosing(const std::stack<char> &chars, char c)
{
    if (chars.empty())
    {
        return false;
    }

    switch (c)
    {
    case ')':
        return chars.top() == '(';
    case ']':
        return chars.top() == '[';
    case '}':
        return chars.top() == '{';
    default:
        return false;
    }
}

bool isValid(std::string s)
{
    std::stack<char> chars;

    for (char c : s)
    {
        if (isOpening(c))
        {
            chars.push(c);
        }
        else if (isClosing(c))
        {
            if (isValidClosing(chars, c))
            {
                chars.pop();
                continue;
            }
            return false;
        }
        else
        {
            fprintf(stderr, "Invalid input");
            exit(EXIT_FAILURE);
        }
    }

    return chars.empty();
}

int main()
{
    return 0;
}