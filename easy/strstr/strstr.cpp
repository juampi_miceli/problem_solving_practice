#include <stdlib.h>
#include <stdio.h>
#include <string>

bool isPrefix(std::string s1, std::string s2, int j)
{
    if (s1.size() > s2.size() - j)
    {
        return false;
    }

    for (int i = 0; i < s1.size(); i++)
    {
        if (s1[i] != s2[i + j])
        {
            return false;
        }
    }
    return true;
}

int strStr(std::string haystack, std::string needle)
{
    if (needle.empty())
    {
        return 0;
    }

    int diff = haystack.size() - needle.size();

    for (int i = 0; i < diff; i++)
    {
        if (isPrefix(needle, haystack, i))
        {
            return i;
        }
    }

    return -1;
}

int main()
{
    printf("%d", strStr("", "a"));
    return 0;
}