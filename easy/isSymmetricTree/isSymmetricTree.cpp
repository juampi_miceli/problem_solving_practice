#include <stdlib.h>
#include <stdio.h>

typedef struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
} TreeNode;

bool isSimmetricRecursive(TreeNode *left, TreeNode *right)
{
    if (left == nullptr && right == nullptr)
    {
        return true;
    }
    if (left == nullptr || right == nullptr)
    {
        return false;
    }

    return left->val == right->val && isSimmetricRecursive(left->left, right->right) && isSimmetricRecursive(left->right, right->left);
}

bool isSymmetric(TreeNode *root)
{
    return (isSimmetricRecursive(root->left, root->right));
}