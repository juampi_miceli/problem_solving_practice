#include <stdlib.h>
#include <stdio.h>
#include <vector>

typedef struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
} TreeNode;

void recursiveSortedArrayToBST(TreeNode *root, const std::vector<int> &nums, int i, int j)
{

    if (i == j)
    {
        return;
    }

    if (i + 1 >= j)
    {
        TreeNode *newNode = new TreeNode(nums[i]);

        if (newNode->val <= root->val)
        {
            root->left = newNode;
        }
        else
        {
            root->right = newNode;
        }
        return;
    }

    int mid = (i + j) / 2;

    TreeNode *newNode = new TreeNode(nums[mid]);

    if (newNode->val <= root->val)
    {
        root->left = newNode;
    }
    else
    {
        root->right = newNode;
    }

    recursiveSortedArrayToBST(newNode, nums, i, mid);
    recursiveSortedArrayToBST(newNode, nums, mid + 1, j);
}

TreeNode *sortedArrayToBST(std::vector<int> &nums)
{
    int mid = nums.size() / 2;
    TreeNode *root = new TreeNode(nums[mid]);

    recursiveSortedArrayToBST(root, nums, 0, mid);
    recursiveSortedArrayToBST(root, nums, mid + 1, nums.size());

    return root;
}

int main()
{
    std::vector<int> nums = {-10, -3, 0, 5, 9};

    sortedArrayToBST(nums);
    return 0;
}