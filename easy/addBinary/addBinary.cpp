#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <algorithm>

std::string addBinary(std::string a, std::string b)
{

    std::string res = "";

    int pA = a.size() - 1;
    int pB = b.size() - 1;
    int carry = 0;

    while (pA >= 0 || pB >= 0)
    {
        int nA = pA >= 0 ? a[pA--] - '0' : 0;
        int nB = pB >= 0 ? b[pB--] - '0' : 0;

        int sum = nA + nB + carry;

        if (sum >= 2)
        {
            carry = 1;
        }
        else
        {
            carry = 0;
        }

        res += sum % 2 + '0';
    }

    if (carry == 1)
    {
        res += '1';
    }

    std::reverse(res.begin(), res.end());
    return res;
}