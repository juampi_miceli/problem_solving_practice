#include <stdlib.h>
#include <stdio.h>
#include <unordered_map>
#include <vector>

using namespace std;

vector<int> twoSum(vector<int>& nums, int target) {
        
        unordered_map<int, size_t> missing; 
        vector<int> answer = vector<int>(2); 


        for(int i = 0; i < nums.size(); i++){
            int currentNumber = nums[i];
            if(missing.count(currentNumber)){
                //I have a partner
                answer[0] = missing[currentNumber];
                answer[1] = i;
                break;
            }
            //I do not have a partner
            missing[target-currentNumber] = i; 
        }

        return answer;
        
    }