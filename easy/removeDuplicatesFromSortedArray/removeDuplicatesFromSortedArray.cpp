#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <random>
#include <bits/stdc++.h>
#include <limits.h>
#include <string.h>
#include <stack>

int removeDuplicates(std::vector<int> &nums)
{
    if (nums.empty())
    {
        return 0;
    }

    bool mustSwap = false;
    int swapPointer = 0;
    int N = nums.size();

    int k = 1;

    for (int i = 0; i < N - 1; i++)
    {
        if (nums[i] < nums[i + 1])
        {
            k++;
            if (mustSwap)
            {
                nums[swapPointer] = nums[i + 1];
                swapPointer++;
            }
            continue;
        }
        if (!mustSwap)
        {
            mustSwap = true;
            swapPointer = i + 1;
        }
    }
    return k;
}
void printVector(const std::vector<int> &v)
{
    printf("[");
    for (int i = 0; i < v.size(); i++)
    {
        printf("%d", v[i]);
        if (i == v.size() - 1)
        {
            printf("]");
            return;
        }
        printf(", ");
    }
}
int main()
{
    std::vector<int> nums = {1, 2, 2, 3, 3, 3, 4, 4, 4, 4};
    printf("k: %d\n", removeDuplicates(nums));
    printVector(nums);
    return 0;
}