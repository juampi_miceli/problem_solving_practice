#include <stdlib.h>
#include <vector>
#include <stdio.h>

std::vector<int> plusOne(std::vector<int> &digits)
{

    int carry = 0;

    for (int i = digits.size() - 1; i >= 0; i--)
    {
        if (i == digits.size() - 1)
        {
            digits[i] = (digits[i] + 1) % 10;
        }
        else
        {
            digits[i] = (digits[i] + carry) % 10;
        }

        if ((digits[i] == 0 && carry == 1) || (digits[i] == 0 && i == digits.size() - 1))
        {
            carry = 1;
        }
        else
        {
            carry = 0;
        }
    }
    if (carry == 1)
    {
        digits.insert(digits.begin(), 1);
    }

    return digits;
}