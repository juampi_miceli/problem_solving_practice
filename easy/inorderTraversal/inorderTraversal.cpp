#include <stdlib.h>
#include <stdio.h>
#include <vector>

typedef struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
} TreeNode;

void recursiveInorderTraversal(TreeNode *root, std::vector<int> &currentVector)
{
    if (root == nullptr)
    {
        return;
    }
    if (root->left != nullptr)
    {

        recursiveInorderTraversal(root->left, currentVector);
    }

    currentVector.push_back(root->val);

    if (root->right != nullptr)
    {
        recursiveInorderTraversal(root->right, currentVector);
    }

    return;
}

std::vector<int> inorderTraversal(TreeNode *root)
{
    std::vector<int> res;
    recursiveInorderTraversal(root, res);

    return res;
}