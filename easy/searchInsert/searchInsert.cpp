#include <stdlib.h>
#include <vector>
#include <stdio.h>

int searchInsert(std::vector<int> &nums, int target)
{
    int pLeft = 0;
    int pRight = nums.size() - 1;
    int mid = (pRight + pLeft) / 2;

    while (pLeft < pRight - 1)
    {

        if (nums[mid] < target)
        {
            pLeft = mid;
            mid = (pRight + pLeft) / 2;
        }
        else if (nums[mid] > target)
        {
            pRight = mid;
            mid = (pRight + pLeft) / 2;
        }
        else
        {
            return mid;
        }
    }

    if (target <= nums[pLeft])
    {
        return pLeft;
    }
    if (target <= nums[pRight])
    {
        return pRight;
    }
    return pRight + 1;
}

int main()
{

    std::vector<int> nums = {1, 3, 5, 6};
    printf("res: %d\n", searchInsert(nums, 5));
    return 0;
}