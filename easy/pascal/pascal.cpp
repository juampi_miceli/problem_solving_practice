#include <bits/stdc++.h>

void recursiveGenerate(int numRows, std::vector<std::vector<int>> &parcialPascal) {
    if (numRows <= 0) return;
    if (parcialPascal.empty()) {
        parcialPascal.push_back({1});
        recursiveGenerate(numRows - 1, parcialPascal);
        return;
    }

    std::vector<int> currentLine = {1};

    int N = parcialPascal.size();
    int M = parcialPascal[N - 1].size();
    for (int i = 1; i < M; i++) {
        currentLine.push_back(parcialPascal[N - 1][i - 1] + parcialPascal[N - 1][i]);
    }
    currentLine.push_back(1);
    parcialPascal.push_back(currentLine);
    recursiveGenerate(numRows - 1, parcialPascal);
}

std::vector<std::vector<int>> generate(int numRows) {
    std::vector<std::vector<int>> res;
    recursiveGenerate(numRows, res);
    return res;
}

int main() {
    return 0;
}