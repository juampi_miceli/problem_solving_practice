#include <stdlib.h>
#include <string>
#include <vector>

char letterAt(const std::string &s, int i)
{
    if (i >= s.size() || i < 0)
    {
        return '\0';
    }
    return s[i];
}

char findCommonLetterAt(const std::vector<std::string> &strs, int i)
{
    char common = '\0';

    for (const std::string &s : strs)
    {
        char currentLetter = letterAt(s, i);
        if (currentLetter == '\0')
        {
            return currentLetter;
        }
        if (common == '\0')
        {
            common = currentLetter;
        }
        if (common != currentLetter)
        {
            return '\0';
        }
    }
    return common;
}

std::string longestCommonPrefix(std::vector<std::string> &strs)
{
    std::string res = "";

    if (strs.empty())
    {
        return res;
    }

    for (int i = 0; i < strs[0].size(); i++)
    {
        char commonLetter = findCommonLetterAt(strs, i);
        if (commonLetter == '\0')
        {
            return res;
        }
        res += commonLetter;
    }
    return res;
}