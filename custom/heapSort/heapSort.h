#include <bits/stdc++.h>

void test(int N);
std::vector<int> getRandomVector(int N, int low, int high);
int getRandom(int low, int high);

int father(int index);
int leftSon(int index);
int rightSon(int index);
void siftDown(std::vector<int> &v, int index);
void siftUp(std::vector<int> &v, int index);
//Making a minHeap
void heapify(std::vector<int> &v);
void heapSort(std::vector<int> &v, int index);

bool isMinHeap(const std::vector<int> &v, int index);
bool isSorted(const std::vector<int> &v);
