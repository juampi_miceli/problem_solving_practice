#include "heapSort.h"

std::vector<int> heapSort(std::vector<int> v)
{
    heapify(v);
    int N = v.size();
    std::vector<int> res;
    while (N > 0)
    {
        std::swap(v[0], v[N - 1]);
        res.push_back(v[N - 1]);
        v.pop_back();
        siftDown(v, 0);
        N--;
    }
    return res;
}

//Making a minHeap
void heapify(std::vector<int> &v)
{
    for (int i = v.size() / 2; i >= 0; i--)
    {
        siftDown(v, i);
    }
}

bool isMinHeap(const std::vector<int> &v, int index)
{
    int N = v.size();
    int leftSonIndex = leftSon(index);
    int rightSonIndex = rightSon(index);

    if (leftSonIndex >= N && rightSonIndex >= N)
    {
        return true;
    }
    if (leftSonIndex >= N)
    {
        return v[index] <= v[rightSonIndex] && isMinHeap(v, rightSonIndex);
    }
    if (rightSonIndex >= N)
    {
        return v[index] <= v[leftSonIndex] && isMinHeap(v, leftSonIndex);
    }
    return v[index] <= v[leftSonIndex] && v[index] <= v[rightSonIndex] && isMinHeap(v, leftSonIndex) && isMinHeap(v, rightSonIndex);
}

int main()
{
    test(10000);
    return 0;
}

int father(int index)
{
    return (index - 1) / 2;
}

int leftSon(int index)
{
    return (index * 2) + 1;
}

int rightSon(int index)
{
    return (index * 2) + 2;
}

void siftDown(std::vector<int> &v, int index)
{
    int N = v.size();
    int leftSonIndex = leftSon(index);
    int rightSonIndex = rightSon(index);
    if (leftSonIndex >= N && rightSonIndex >= N)
    {
        //No more childs
        return;
    }

    int minValue;
    int minValueIndex;

    if (leftSonIndex >= N)
    {
        //No more left childs
        minValue = v[rightSonIndex];
        minValueIndex = rightSonIndex;
    }
    else if (rightSonIndex >= N)
    {
        //No more right childs
        minValue = v[leftSonIndex];
        minValueIndex = leftSonIndex;
    }
    else
    {
        minValue = std::min(v[leftSonIndex], v[rightSonIndex]);
        if (v[leftSonIndex] < v[rightSonIndex])
        {
            minValue = v[leftSonIndex];
            minValueIndex = leftSonIndex;
        }
        else
        {
            minValue = v[rightSonIndex];
            minValueIndex = rightSonIndex;
        }
    }

    if (v[index] > minValue)
    {
        std::swap(v[index], v[minValueIndex]);
        siftDown(v, minValueIndex);
    }
}

void siftUp(std::vector<int> &v, int index)
{
    int N = v.size();
    int fatherIndex = father(index);
    if (fatherIndex < 0)
    {
        //No more parents
        return;
    }

    if (v[index] < v[fatherIndex])
    {
        std::swap(v[index], v[fatherIndex]);
        siftUp(v, fatherIndex);
    }
}

bool isSorted(const std::vector<int> &v)
{
    int N = v.size();
    for (int i = 0; i < N - 1; i++)
    {
        if (v[i] > v[i + 1])
        {
            return false;
        }
    }

    return true;
}

void test(int N)
{
    int low = -50;
    int high = 50;
    for (int i = 0; i < N; i++)
    {
        std::vector<int> backupVector = getRandomVector(getRandom(0, 50), low, high);
        std::vector<int> randVector = heapSort(backupVector);
        if (!isSorted(randVector))
        {
            fprintf(stderr, "Not sorted ====\ti: %d\n", i);
            for (int i = 0; i < randVector.size(); i++)
            {
                printf("%d, ", randVector[i]);
            }
            printf("\n");
            for (int i = 0; i < backupVector.size(); i++)
            {
                printf("%d, ", backupVector[i]);
            }
            printf("\n");
            exit(EXIT_FAILURE);
        }
    }
    printf("Tests passed!!!\n");
}

std::vector<int> getRandomVector(int N, int low, int high)
{
    std::vector<int> resVector;
    for (int i = 0; i < N; i++)
    {
        resVector.push_back(getRandom(low, high));
    }
    return resVector;
}

int getRandom(int low, int high)
{
    int delta = high - low;
    return (rand() % (delta + 1)) + low;
}