#include <stdlib.h>
#include <stdio.h>

#ifndef C_STACK_H
#define C_STACK_H

/**==============================
 * =         STACK NODE         =
 * ==============================
*/

struct stackNode_t
{
    char *value;
    struct stackNode_t *next;
};

struct stackNode_t *newStackNode(char *c);

/**=============================
 * =           STACK           =
 * =============================
*/

struct stack_t
{
    struct stackNode_t *first;
    u_int16_t size;
};

struct stack_t *newStack();

char *stackTop(struct stack_t *stack);
u_int16_t stackPush(struct stack_t *stack, char *value);
u_int16_t stackPop(struct stack_t *stack);
void deleteStack(struct stack_t *stack);
void stackPrint(struct stack_t *stack);

#endif //C_STACK_H