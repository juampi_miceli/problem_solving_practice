#include <stdlib.h>
#include <stdio.h>
#include "cStack.h"

// int main()
// {
//     struct stack_t *stack = newStack();
//     stackPush(stack, "5\0");
//     stackPush(stack, "2\0");
//     stackPush(stack, "3123\0");
//     stackPrint(stack);
//     // stackPop(stack);
//     // stackPop(stack);
//     // stackPrint(stack);
//     // stackPop(stack);
//     // stackPrint(stack);
//     // stackPop(stack);
//     // stackPop(stack);
//     // stackPop(stack);
//     // stackPrint(stack);
//     deleteStack(stack);
//     return 0;
// }

/**=============================
 * =           STACK           =
 * =============================
*/

struct stack_t *newStack()
{
    struct stack_t *stack = (struct stack_t *)(malloc(sizeof(struct stack_t)));
    stack->first = NULL;
    stack->size = 0;
    return stack;
}

char *stackTop(struct stack_t *stack)
{
    if (stack->size == 0)
    {
        return NULL;
    }
    return stack->first->value;
}

u_int16_t stackPush(struct stack_t *stack, char *value)
{
    struct stackNode_t *newNode = newStackNode(value);
    newNode->next = stack->first;
    stack->first = newNode;
    return ++(stack->size);
}

u_int16_t stackPop(struct stack_t *stack)
{
    if (stack->size == 0)
    {
        return 0;
    }
    struct stackNode_t *newFirst = stack->first->next;
    free(stack->first->value);
    free(stack->first);
    stack->first = newFirst;
    return --(stack->size);
}

void deleteStack(struct stack_t *stack)
{
    while (stack->size > 0)
    {
        stackPop(stack);
    }
    free(stack);
}

void stackPrint(struct stack_t *stack)
{
    u_int16_t N = stack->size;
    struct stackNode_t *node = stack->first;
    while (node != NULL)
    {
        printf("%s ", node->value);
        node = node->next;
    }
    printf("\n");
}

/**==============================
 * =         STACK NODE         =
 * ==============================
*/

struct stackNode_t *newStackNode(char *c)
{
    struct stackNode_t *newNode = (struct stackNode_t *)(malloc(sizeof(struct stackNode_t)));
    newNode->value = c;
    newNode->next = NULL;

    return newNode;
}