#include <stdlib.h>
#include <stdio.h>

char *strrev(char *s);
void inplaceStrrev(char s[], int start, int end);
void memcopy(char *p1, char *p2, int n);
char *wordrev(char *s);
void inplaceWordrev(char s[]);
int min(int a, int b);
/**
 * Let N be the size of the string
 * 
 * To reverse the string we will use two pointers, one pointer (pRight) will be at the end of the input string (just before '\0') and 
 * the other one (pLeft) at the start of the resulting string. Then we will read the value pointed by pRight and copy that into the 
 * position pointed by pLeft. After that we will increment pLeft and decrement pRight. This will end once pLeft has reached the size
 * of the original string. To leave a valid string, before returning the new string we must set its last element to '\0'.
 * 
 * As in a first moment we don't know the size of the string, we need to traverse it from left to right until we find a '\0'. That 
 * will take us O(N) time, then the process of copying every character will also take us O(N). Therefore, the overall time complexity
 * is O(N).
 * We advance in O(N)
*/
char *strrev(char *s)
{

    //Get the size of the string
    int N = 1;
    while (s[N - 1] != '\0')
    {
        N++;
    }

    //Allocate memory for the new string
    char *res = (char *)malloc(N);

    //Copy every character to the new string in reverse order
    int pLeft = 0;
    int pRight = N - 2;
    while (pLeft < N - 1)
    {
        res[pLeft++] = s[pRight--];
    }

    //Set final char to '\0'
    res[N - 1] = '\0';

    return res;
}

void inplaceStrrev(char s[], int start, int end)
{

    int pLeft;
    int pRight;
    if (start == -1 && end == -1)
    {
        int N = 0;
        while (s[N] != '\0')
        {
            N++;
        }
        pLeft = 0;
        pRight = N - 1;
    }
    else
    {
        pLeft = start;
        pRight = end - 1;
    }

    while (pLeft < pRight)
    {
        char aux = s[pLeft];
        s[pLeft] = s[pRight];
        s[pRight] = aux;
        pLeft++;
        pRight--;
    }
}

void memcopy(char *p1, char *p2, int n)
{
    for (int i = 0; i < n; i++)
    {
        p2[i] = p1[i];
    }
}

/**
 * Let N be the size of the string 
 * 
 * The idea of this algorithm is similar to the one discussed in strrev, we will set a pointer at the end of the input string, and one at the 
 * start of the new string. The difference is that in this case we will not copy each character bi its own, instead we will need to copy the 
 * entire word. For that reason, pRight will move back until it finds an empty space, each step storing the word size. Then, we will use the
 * function memcpy (I did not imported it because i didn't know if it was allowed), to copy all the word in order to the new string. This 
 * process will continue until we have no words left. In this occasion we will have O(N) time to get the size of the strings, and another 
 * O(2N) to copy all the words from one string to the other. This is because the back and forward that we need to make for every word.
 * All in all, the time complexity is O(N).
 * 
*/
char *wordrev(char *s)
{
    //Get the size of the string
    int N = 1;
    while (s[N - 1] != '\0')
    {
        N++;
    }

    //Allocate memory for the new string
    char *res = (char *)malloc(N);

    //Copy every word to the new string in reverse order
    int pLeft = 0;
    int pRight = N - 2;
    while (pLeft < N - 1)
    {
        //Get word size
        int wordSize = 0;
        while (pRight >= 0 && s[pRight] != ' ')
        {
            //Back the pointer until a space is reached or until the string is done
            pRight--;
            wordSize++;
        }

        //Copy the word from s to res
        memcopy(s + pRight + 1, res + pLeft, wordSize);

        //Advance pLeft to new word position
        pLeft += wordSize;

        //Back pRight to new word position
        pRight--;

        if (pLeft < N - 1)
        {
            //There are more words, so I need to add an empty space
            res[pLeft++] = ' ';
        }
    }

    //Set final char to '\0'
    res[N - 1] = '\0';

    return res;
}

/**
 * If we use inplaceStrrev we will end up with the words in a reverse order, but unfortunately the order of the letters inside the words will also be
 * reverse. We can fix this in an easy way by using inplaceStrrev for every word. That would take us O(N) for the initial sorting and another O(N)
 * for sorting every word. Then, the total complexity is O(N).
*/
void inplaceWordrev(char s[])
{
    //Get the size of the string and reverse every word inplace
    int N = 0;
    int wordStart = -1;
    while (s[N] != '\0')
    {
        if (s[N] != ' ')
        {
            if (wordStart == -1)
            {
                wordStart = N;
            }
            if (s[N + 1] == '\0')
            {
                inplaceStrrev(s, wordStart, N + 1);
            }
        }
        else if (s[N] == ' ')
        {
            inplaceStrrev(s, wordStart, N);
            wordStart = -1;
        }

        N++;
    }
    inplaceStrrev(s, -1, -1);
}

int getRandom(int low, int high)
{
    return rand() % (high - low) + low;
}

char getRandomChar()
{
    char firstChar = ' ';
    char lastChar = '~';
    return rand() % (lastChar - firstChar) + firstChar;
}

void getRandomWord(int N, char *word)
{
    for (int i = 0; i < N; i++)
    {
        char c = getRandomChar();
        word[i] = c != ' ' ? c : c + 1;
    }
}

int strcomp(char *s1, char *s2)
{
    int i = 0;

    while (s1[i] != '\0' && s2[i] != '\0')
    {
        if (s1[i] != s2[i])
        {
            return 0;
        }
        i++;
    }
    return (s1[i] == s2[i]) ? 1 : 0;
}

void strrevTest(int N)
{
    char *inputString = (char *)(malloc(100));

    char *expectedString = (char *)(malloc(100));
    int step = 0;
    while (step < N)
    {
        int randomSize = getRandom(1, 100);

        for (int i = 0; i < randomSize - 1; i++)
        {
            char c = getRandomChar();
            inputString[i] = c;
            expectedString[randomSize - i - 2] = c;
        }
        inputString[randomSize - 1] = '\0';
        expectedString[randomSize - 1] = '\0';

        inplaceStrrev(inputString, -1, -1);
        if (strcomp(inputString, expectedString) == 0)
        {
            printf("i: %d, input: %s, actual: %s, expected: %s\n", step, inputString, inputString, expectedString);
            exit(EXIT_FAILURE);
        }
        step++;
    }
    free(inputString);

    free(expectedString);
    printf("String tests passed!!!\n");
}

/**
 * We basically generate words of random size between 1 and 10 and we paste them in the strings "expected" and "input".
 * In "input" we paste the words from left to right and in "expected" we paste them from right to left. For the last
 * word, we make sure that the word fills the string without writing in an invalid location. After that we just compare
 * the expected string with the string obtained using wordrev.
*/
void wordrevTest(int N)
{
    int stringSize = 30;
    char *inputString = (char *)(malloc(stringSize));
    inputString[stringSize - 1] = '\0';
    char *expectedString = (char *)(malloc(stringSize));
    expectedString[stringSize - 1] = '\0';
    int step = 0;

    char *word = (char *)malloc(20);
    while (step < N)
    {
        int pInput = 0;
        int pExpected = stringSize - 2;

        while (pInput < stringSize - 1)
        {

            int wordSize;
            if (pInput < stringSize - 10)
            {
                wordSize = getRandom(1, 5);
            }
            else
            {
                wordSize = stringSize - pInput - 1;
            }

            getRandomWord(wordSize, word);
            memcopy(word, inputString + pInput, wordSize);
            memcopy(word, expectedString + pExpected - wordSize + 1, wordSize);
            pExpected -= wordSize;
            pInput += wordSize;

            if (pInput < stringSize - 1)
            {
                inputString[pInput++] = ' ';
                expectedString[pExpected--] = ' ';
            }
        }

        inplaceWordrev(inputString);
        if (strcomp(inputString, expectedString) == 0)
        {
            printf("i: %d\n", step);
            printf("input:    '%s'\n", inputString);
            printf("expected: '%s'\n", expectedString);
            exit(EXIT_FAILURE);
        }
        step++;
    }

    free(word);
    free(inputString);
    free(expectedString);

    printf("Word tests passed!!!\n");
}

int main()
{
    wordrevTest(10000);
    strrevTest(10000);
    return 0;
}

int min(int a, int b)
{
    if (a <= b)
    {
        return a;
    }
    return b;
}