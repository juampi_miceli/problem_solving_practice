#include <bits/stdc++.h>
#include "knightsTour.h"

/**
 * We basically want a closed hamiltonian path. We can see the board as a graph where each cell is a node and
 * two cells are connected if a horse is able to move to one cell to the other one. That leaves us with an 8x8
 * nodes graph that have all nodes with 8 or less neighbors. Hamiltonian path is a NP problem so there is not any 
 * polinomial algorithm. Instead we can try doing backtracking with some pruning. In the worst case this algorithm
 * will take O(8^(n*n)) (because of the 8 neighbors and the n*n nodes).
*/
tour knightsTour(int i, int j)
{
    std::vector<std::vector<bool>> visited(BOARD_SIZE, std::vector<bool>(BOARD_SIZE, false));
    boardPos initPos(i, j);
    tour initialTour = {initPos};
    visited[i][j] = true;
    return recursiveKnightsTour(initialTour, visited, 1, initPos, initPos);
}

tour recursiveKnightsTour(tour &currentTour, std::vector<std::vector<bool>> &visited, int steps, const boardPos &currentPos, const boardPos &initPos)
{
    //Are we done with the tour?
    if (steps >= BOARD_SIZE * BOARD_SIZE)
    {
        //Can i close the tour?
        if (contains(getAllMoves(currentPos), initPos))
        {
            return currentTour;
        }

        //I could not close the tour. This is an invalid tour, return empty
        return tour();
    }

    //Is home bloqued?
    if (getAvailableNeighbors(visited, initPos).empty())
    {
        return tour();
    }

    //Possible moves
    std ::vector<boardPos> availableNeighbors = getAvailableNeighbors(visited, currentPos);

    //Are we blocked?
    if (availableNeighbors.empty())
    {
        return tour();
    }

    //Is anyone blocked?
    if (anyoneIsUnreachableAndUnvisited(visited, currentPos, initPos))
    {
        return tour();
    }

    for (const boardPos &neighbor : availableNeighbors)
    {
        //Visit the new position
        visited[std::get<0>(neighbor)][std::get<1>(neighbor)] = true;
        currentTour.push_back(neighbor);
        steps++;

        //Try all posibilities with this tour
        tour resultingTour = recursiveKnightsTour(currentTour, visited, steps, neighbor, initPos);

        //Unvisit the position
        visited[std::get<0>(neighbor)][std::get<1>(neighbor)] = false;
        currentTour.pop_back();
        steps--;

        if (resultingTour.size() > 0)
        {
            //Tour found
            return resultingTour;
        }
    }

    //Every tour was unsuccessful
    return tour();
}

std::vector<boardPos> getAvailableNeighbors(const std::vector<std::vector<bool>> &visited, const boardPos &pos)
{
    std::vector<boardPos> allMoves = getAllMoves(pos);
    std::vector<boardPos> availableNeighbors;

    for (const boardPos &move : allMoves)
    {
        if (isValidAndAvailable(visited, move))
        {
            availableNeighbors.push_back(move);
        }
    }
    return availableNeighbors;
}

bool isValidAndAvailable(const std::vector<std::vector<bool>> &visited, const boardPos &pos)
{
    if (std::get<0>(pos) < 0 || std::get<1>(pos) < 0 || std::get<0>(pos) >= BOARD_SIZE || std::get<1>(pos) >= BOARD_SIZE)
    {
        //position out of bounds
        return false;
    }

    return !visited[std::get<0>(pos)][std::get<1>(pos)];
}

std::vector<boardPos> getAllMoves(const boardPos &pos)
{
    std::vector<boardPos> allMoves;

    allMoves.push_back(boardPos(std::get<0>(pos) + 1, std::get<1>(pos) + 2));
    allMoves.push_back(boardPos(std::get<0>(pos) + 1, std::get<1>(pos) - 2));
    allMoves.push_back(boardPos(std::get<0>(pos) - 1, std::get<1>(pos) + 2));
    allMoves.push_back(boardPos(std::get<0>(pos) - 1, std::get<1>(pos) - 2));
    allMoves.push_back(boardPos(std::get<0>(pos) + 2, std::get<1>(pos) + 1));
    allMoves.push_back(boardPos(std::get<0>(pos) + 2, std::get<1>(pos) - 1));
    allMoves.push_back(boardPos(std::get<0>(pos) - 2, std::get<1>(pos) + 1));
    allMoves.push_back(boardPos(std::get<0>(pos) - 2, std::get<1>(pos) - 1));

    return allMoves;
}

bool contains(const std::vector<boardPos> &positions, const boardPos &pos)
{
    for (const boardPos &currentPos : positions)
    {
        if ((std::get<0>(currentPos) == std::get<0>(pos)) && (std::get<0>(currentPos) == std::get<0>(pos)))
        {
            return true;
        }
    }

    return false;
}

int manhattan(const boardPos &p1, const boardPos &p2)
{
    return abs(std::get<0>(p1) - std::get<0>(p2)) + abs(std::get<1>(p1) - std::get<1>(p2));
}

bool isValidMove(const boardPos &p1, const boardPos &p2)
{
    if (std::get<0>(p1) == std::get<0>(p2) || std::get<1>(p1) == std::get<1>(p2))
    {
        return false;
    }

    return manhattan(p1, p2) == 3;
}

///////O(n*n)
//Could be done in constant time
bool anyoneIsUnreachableAndUnvisited(std::vector<std::vector<bool>> &visited, const boardPos &currentPos, const boardPos &initPos)
{
    visited[std::get<0>(currentPos)][std::get<1>(currentPos)] = false;
    for (int i = 0; i < visited.size(); i++)
    {
        for (int j = 0; j < visited.size(); j++)
        {
            if (!visited[i][j])
            {
                if (std::get<0>(currentPos) == i && std::get<1>(currentPos) == j)
                {
                    continue;
                }
                std::vector<boardPos> neighbors = getAvailableNeighbors(visited, boardPos(i, j));
                //If it is unvisited and I can't reach it, solution is not possible
                if (neighbors.empty())
                {
                    visited[std::get<0>(currentPos)][std::get<1>(currentPos)] = true;

                    return true;
                }
                //If it is not a final position and it has only one available neighbor
                //Sooner or later I will get stuck
                if (!isValidMove(boardPos(i, j), initPos) && neighbors.size() == 1)
                {
                    visited[std::get<0>(currentPos)][std::get<1>(currentPos)] = true;
                    return true;
                }
            }
        }
    }
    visited[std::get<0>(currentPos)][std::get<1>(currentPos)] = true;
    return false;
}

int main()
{
    for (int i = 0; i < BOARD_SIZE; i++)
    {
        for (int j = 0; j < BOARD_SIZE; j++)
        {
            knightsTour(i, j);
        }
    }
    // tour resultingTour = knightsTour(0, 0);
    // for (const boardPos &pos : resultingTour)
    // {
    //     printf("(%d,%d)\n", std::get<0>(pos), std::get<1>(pos));
    // }

    // for (int i = 0; i < resultingTour.size() - 1; i++)
    // {
    //     if (!isValidMove(resultingTour[i], resultingTour[i + 1]))
    //     {
    //         fprintf(stderr, "INVALID PATH\n");
    //         exit(EXIT_FAILURE);
    //     }
    // }

    // if (!isValidMove(resultingTour[0], resultingTour[resultingTour.size() - 1]))
    // {
    //     fprintf(stderr, "INVALID PATH\n");
    //     exit(EXIT_FAILURE);
    // }

    printf("Your path was perfect :')\n");
}