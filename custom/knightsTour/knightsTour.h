#include <bits/stdc++.h>

#define BOARD_SIZE 8

typedef std::pair<int, int> boardPos;
typedef std::vector<std::pair<int, int>> tour;

bool isValidAndAvailable(const std::vector<std::vector<bool>> &visited, const boardPos &pos);

std::vector<boardPos> getAllMoves(const boardPos &pos);

bool contains(const std::vector<boardPos> &positions, const boardPos &pos);

std::vector<boardPos> getAvailableNeighbors(const std::vector<std::vector<bool>> &visited, const boardPos &pos);

tour recursiveKnightsTour(tour &currentTour, std::vector<std::vector<bool>> &visited, int steps, const boardPos &currentPos, const boardPos &initPos);

tour knightsTour(int i, int j);
int manhattan(const boardPos &p1, const boardPos &p2);
bool isValidMove(const boardPos &p1, const boardPos &p2);
bool anyoneIsUnreachableAndUnvisited(std::vector<std::vector<bool>> &visited, const boardPos &currentPos, const boardPos &initPos);