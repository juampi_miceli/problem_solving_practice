#include <bits/stdc++.h>

int binarySearch(const std::vector<int> &v, int elem);
void test(int N);
std::vector<int> getRandomVector(int N, int low, int high);
int getRandom(int low, int high);