#include <bits/stdc++.h>
#include "binarySearch.h"

/**
 * Returns the index of element searched, not necessarily the first appearance.-1 if this element is not present
*/
int binarySearch(const std::vector<int> &v, int elem)
{

    if (v.empty())
    {
        return -1;
    }

    int left = 0;
    int right = v.size() - 1;

    while (right - left > 1)
    {
        int mid = (left + right) / 2;
        if (v[mid] <= elem)
        {
            left = mid;
        }
        else
        {
            right = mid;
        }
    }

    if (v[left] == elem)
    {
        return left;
    }
    if (v[right] == elem)
    {
        return right;
    }

    return -1;
}

int main()
{
    test(100000);
    return 0;
}

void test(int N)
{
    int low = -50;
    int high = 50;
    for (int i = 0; i < N; i++)
    {
        std::vector<int> randVector = getRandomVector(getRandom(0, 50), low, high);
        int randNumber = getRandom(-100, 100);
        int index = binarySearch(randVector, randNumber);
        if (index == -1)
        {
            for (int i = 0; i < randVector.size(); i++)
            {
                if (randVector[i] == randNumber)
                {
                    fprintf(stderr, "Not found ====\ti: %d, elem: %d\n", i, randNumber);
                    for (int i = 0; i < randVector.size(); i++)
                    {
                        printf("%d, ", randVector[i]);
                    }
                    printf("\n");
                    exit(EXIT_FAILURE);
                }
            }
            continue;
        }
        if (randVector[index] != randNumber)
        {
            fprintf(stderr, "Wrongly found ====\ti: %d, index: %d, elem: %d\n", i, index, randNumber);
            for (int i = 0; i < randVector.size(); i++)
            {
                printf("%d, ", randVector[i]);
            }
            printf("\n");
            exit(EXIT_FAILURE);
        }
    }
    printf("Tests passed!!!\n");
}

std::vector<int> getRandomVector(int N, int low, int high)
{
    std::vector<int> resVector;
    for (int i = 0; i < N; i++)
    {
        resVector.push_back(getRandom(low, high));
    }
    sort(resVector.begin(), resVector.end());
    return resVector;
}

int getRandom(int low, int high)
{
    int delta = high - low;
    return (rand() % (delta + 1)) + low;
}
