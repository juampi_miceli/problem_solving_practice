#include <bits/stdc++.h>

/**
 * For solving this problem the first thing we can do is to build every possible way of adding coins up to the goal. We have to be careful
 * not to repeat ourselves as we count, as if we are not careful we can end counting solutions that use the same amount of coins but in
 * different order. For this, our solutions will only use a coin of the same value as the one used before, or use one of higher value.
 * We will know that solution can be pruned if we reach exactly the goal, in where we know the solution is valid, or if we pass from
 * the goal, where we will know that the solution is invalid and that we can't get to a valid solution.
 * 
 * With this said, if we call N the goal to sum, and K the number of different coins, we can expect to have a time complexity of O(N^K)
 * as we can have at most N steps, and each step we have to visit at most K different ways.
 * 
 * We will write this algorithm recursively and we will keep track of how far we are from the goal, and what coin have we used last.
 * We will have two base cases, goal < 0 that means we passed our goal so the solution is invalid, and goal == 0 that means we are in
 * a valid solution.
 * 
 * In the recursive call we will substract to the goal the value of the coin that we are adding and we will pass the currentCoinIndex
 * so that we can't use a smaller coin again.
*/
int coinSumsBT(const std::vector<int> &coins, int goal, int currentCoinIndex)
{
    int N = coins.size();
    if (goal < 0)
    {
        return 0;
    }
    if (goal == 0)
    {
        return 1;
    }
    int ways = 0;
    for (int i = currentCoinIndex; i < N; i++)
    {
        ways += coinSumsBT(coins, goal - coins[i], i);
    }
    return ways;
}

int main()
{
    std::vector<int> coins = {1, 2, 5, 10, 20, 50, 100, 200};
    int goal = 200;

    auto start = std::chrono::steady_clock::now();
    int res = coinSumsBT(coins, goal, 0);
    auto end = std::chrono::steady_clock::now();
    long time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    printf("goal: %d, ways: %d, %ld\n", goal, res, time);
    return 0;
}