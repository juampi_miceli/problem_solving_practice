#include <bits/stdc++.h>

bool jollyJumper(const std::vector<int> &values)
{
    int N = values.size();
    std::vector<bool> visited(N - 1, false);

    for (int i = 0; i < N - 1; i++)
    {
        int diff = abs(values[i] - values[i + 1]) - 1;
        if (diff < 0 || diff >= N - 1)
        {
            return false;
        }
        visited[diff] = true;
    }

    for (bool elem : visited)
    {
        if (!elem)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    int N;
    while (std::cin >> N)
    {
        std::vector<int> v;
        for (int i = 0; i < N; i++)
        {
            int val;
            std::cin >> val;
            v.push_back(val);
        }
        if (jollyJumper(v))
        {
            printf("Jolly\n");
        }
        else
        {
            printf("Not jolly\n");
        }
    }
    return 0;
}