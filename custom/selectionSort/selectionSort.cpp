#include "selectionSort.h"

int argMin(const std::vector<int> v, int low)
{
    int argWinner = low;
    int winner = v[low];
    for (int i = low; i < v.size(); i++)
    {
        if (winner > v[i])
        {
            argWinner = i;
            winner = v[i];
        }
    }
    return argWinner;
}

void selectionSort(std::vector<int> &v)
{
    for (int i = 0; i < v.size(); i++)
    {
        std::swap(v[i], v[argMin(v, i)]);
    }
}
int main()
{
    test(1000);
    return 0;
}

void test(int N)
{
    int low = -50;
    int high = 50;
    for (int i = 0; i < N; i++)
    {
        std::vector<int> randVector = getRandomVector(getRandom(0, 50), low, high);
        selectionSort(randVector);
        for (int i = 0; i < (int)randVector.size() - 1; i++)
        {
            if (randVector[i] > randVector[i + 1])
            {
                fprintf(stderr, "Wrongly sorted ====\ti: %d\n", i);
                for (int i = 0; i < randVector.size(); i++)
                {
                    printf("%d, ", randVector[i]);
                }
                printf("\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    printf("Tests passed!!!\n");
}

std::vector<int> getRandomVector(int N, int low, int high)
{
    std::vector<int> resVector;
    for (int i = 0; i < N; i++)
    {
        resVector.push_back(getRandom(low, high));
    }
    return resVector;
}

int getRandom(int low, int high)
{
    int delta = high - low;
    return (rand() % (delta + 1)) + low;
}