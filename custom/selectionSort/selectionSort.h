#include <bits/stdc++.h>

int argMin(const std::vector<int> v, int low);
void selectionSort(std::vector<int> &v);
void test(int N);
std::vector<int> getRandomVector(int N, int low, int high);
int getRandom(int low, int high);