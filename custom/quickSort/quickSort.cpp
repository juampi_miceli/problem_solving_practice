#include "quickSort.h"

int pivotDivision(std::vector<int> &v, int low, int high)
{

    int pLeft = low;
    int pRight = high - 1;

    int pivotIndex = pRight;
    int pivotValue = v[pRight];

    do
    {
        while (pLeft < high && v[pLeft] <= pivotValue)
        {
            pLeft++;
        }
        while (pRight >= low && v[pRight] >= pivotValue)
        {
            pRight--;
        }
        if (pLeft > pRight)
        {
            break;
        }
        std::swap(v[pLeft], v[pRight]);
    } while (pLeft < pRight);

    if (pLeft < high)
    {
        std::swap(v[pLeft], v[pivotIndex]);
        return pLeft;
    }

    return pivotIndex;
}

void quickSort(std::vector<int> &v, int low, int high)
{
    if (high - low > 1)
    {
        int pivotIndex = pivotDivision(v, low, high);
        quickSort(v, low, pivotIndex);
        quickSort(v, pivotIndex + 1, high);
    }
}

int main()
{
    test(10000);
    return 0;
}

void test(int N)
{
    int low = -50;
    int high = 50;
    for (int i = 0; i < N; i++)
    {
        std::vector<int> randVector = getRandomVector(getRandom(0, 50), low, high);
        std::vector<int> backupVector = randVector;
        quickSort(randVector, 0, randVector.size());
        for (int i = 0; i < (int)randVector.size() - 1; i++)
        {
            if (randVector[i] > randVector[i + 1])
            {
                fprintf(stderr, "Wrongly sorted ====\ti: %d\n", i);
                for (int i = 0; i < randVector.size(); i++)
                {
                    printf("%d, ", randVector[i]);
                }
                printf("\n");
                for (int i = 0; i < backupVector.size(); i++)
                {
                    printf("%d, ", backupVector[i]);
                }
                printf("\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    printf("Tests passed!!!\n");
}

std::vector<int> getRandomVector(int N, int low, int high)
{
    std::vector<int> resVector;
    for (int i = 0; i < N; i++)
    {
        resVector.push_back(getRandom(low, high));
    }
    return resVector;
}

int getRandom(int low, int high)
{
    int delta = high - low;
    return (rand() % (delta + 1)) + low;
}
