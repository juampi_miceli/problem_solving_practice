#include "paintingSquaresV2.h"

/**
 * The first approach here is exactly the same as in the previous version. The only difference is in the validation function,
 * now we have to check that we do not have 3 squares with the same color in a row. But again that is O(N). Therefore, the time
 * complexity is still O(K^N * N).
*/
int paintingSquaresBF(int squares, int colors)
{
    std::vector<int> currentPainting;
    return recursivePaintingSquaresBF(squares, colors, currentPainting);
}

int recursivePaintingSquaresBF(int squares, int colors, std::vector<int> &currentPainting)
{
    int N = currentPainting.size();
    if (squares <= N)
    {
        //We painted all the squares
        if (isValidPainting(currentPainting))
        {
            return 1;
        }
        return 0;
    }

    int possibleWays = 0;

    for (int i = 0; i < colors; i++)
    {
        //Paint with every possible color
        currentPainting.push_back(i);
        possibleWays += recursivePaintingSquaresBF(squares, colors, currentPainting);
        currentPainting.pop_back();
    }
    return possibleWays;
}

bool isValidPainting(const std::vector<int> &painting)
{
    int N = painting.size();
    for (int i = 0; i < N - 2; i++)
    {
        if (painting[i] == painting[i + 1] && painting[i] == painting[i + 2])
        {
            return false;
        }
    }

    return true;
}

/**
 * To improve the complexity of solving this problem, we can think in a bottom up approach, starting with one square and increasing
 * this number by one each iteration. That is, if we realize that the number of possible ways is the sum of the solutions where the
 * last two colors are equal (we will call this solutions "bloqued"), and the ones where the last two colors are different 
 * ("nonBloqued").
 * At first we don't know this number for squares greater than 2, but we can build them as we said, using bottom up. When we have
 * only one square, of course that there are no "bloqued" squares, so the solution is only the number of the "nonBloqued" solutions,
 * that is one for every color (K).
 * Now let's what happen to this numbers when we add a square, for each "nonBloqued" square we will add, 1 "bloqued" square and 
 * (K-1) "nonBloqued" squares, and for each "bloqued" square we will get (K-1) "nonBloqued" squares.
 * Then, we can use 2 vectors as our memoization structures, one for the "bloqued" squares and one for the "nonBloqued" squares
 * and we will start two fill those vectors each iteration up two the numbers of squares that were requested.
 * This would take N iterations and would use 2 N sized vectors, therefore the time/space complexity would be O(N).
*/
int paintingSquares(int squares, int colors)
{
    if (squares == 0)
    {
        return 0;
    }
    std::vector<int> bloqued(squares, -1);
    std::vector<int> nonBloqued(squares, -1);

    bloqued[0] = 0;
    nonBloqued[0] = colors;

    for (int i = 1; i < squares; i++)
    {
        bloqued[i] = nonBloqued[i - 1];
        nonBloqued[i] = bloqued[i - 1] * (colors - 1) + nonBloqued[i - 1] * (colors - 1);
    }

    return bloqued[squares - 1] + nonBloqued[squares - 1];
}

int main()
{

    for (int squares = 1; squares < 8; squares++)
    {
        for (int colors = 1; colors < 10; colors++)
        {
            int res1 = paintingSquaresBF(squares, colors);
            int res2 = paintingSquares(squares, colors);
            if (res1 != res2)
            {
                fprintf(stderr, "squares: %d, colors: %d, BF: %d, cte: %d", squares, colors, res1, res2);
                exit(EXIT_FAILURE);
            }
        }
    }
    printf("Tests passed!!\n");

    return 0;
}