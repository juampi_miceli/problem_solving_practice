#include <bits/stdc++.h>

std::string isExpectedToPass(int N, int P, const std::vector<int> &q, const std::vector<double> &pc, const std::vector<double> &pw)
{

    double expectedPoints = 0;

    for (int i = 0; i < N; i++)
    {
        double pCorrect = 1.0 / (double)(q[i]);
        double pWrong = 1.0 - pCorrect;
        double expectedWinning = pCorrect * pc[i];
        double expectedLosing = pWrong * pw[i];

        expectedPoints += std::max(0.0, expectedWinning - expectedLosing);
    }

    if (P > expectedPoints)
    {
        return "NO";
    }
    return "YES";
}

int main()
{
    printf("%s\n", isExpectedToPass(2, 1, {2, 2}, {2, 2}, {1, 1}).c_str());
    printf("%s\n", isExpectedToPass(3, 2, {2, 4, 2}, {1, 4, 1}, {0.5, 1, 1}).c_str());
    return 0;
}