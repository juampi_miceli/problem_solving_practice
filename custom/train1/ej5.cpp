#include <bits/stdc++.h>

class Dishes
{

public:
    Dishes(int _maxSize) : maxSize(_maxSize), dishesStacks({std::stack<int>()}){};

    void add(int ID);
    int remove();
    int count(int stackNumber);

private:
    int maxSize;
    std::vector<std::stack<int>> dishesStacks;
};

void Dishes::add(int ID)
{
    int N = (int)(dishesStacks.size()) - 1;
    if ((int)(dishesStacks[N].size()) >= maxSize)
    {
        std::stack<int> newStack;
        dishesStacks.push_back(newStack);
        N++;
    }
    dishesStacks[N].push(ID);
}

int Dishes::remove()
{
    int N = dishesStacks.size();
    int resID = dishesStacks[0].top();
    dishesStacks[0].pop();

    for (int i = 0; i < N - 1; i++)
    {
        int ID = dishesStacks[i + 1].top();
        dishesStacks[i + 1].pop();
        dishesStacks[i].push(ID);
    }

    if (dishesStacks[N - 1].empty())
    {
        dishesStacks.pop_back();
    }

    return resID;
}

int Dishes::count(int stackNumber)
{
    int N = dishesStacks.size();
    if (stackNumber >= N)
    {
        return -1;
    }
    return dishesStacks[stackNumber].size();
}

int main()
{
    int maxSize = 2;
    Dishes *dishes = new Dishes(maxSize);

    dishes->add(13);
    dishes->add(7);
    dishes->add(17);
    printf("%d\n", dishes->count(0));
    printf("%d\n", dishes->count(1));
    dishes->add(2);
    printf("%d\n", dishes->count(1));
    dishes->add(47);
    printf("%d\n", dishes->remove());
    printf("%d\n", dishes->count(0));
    printf("%d\n", dishes->count(1));
    printf("%d\n", dishes->count(2));
    return 0;
}