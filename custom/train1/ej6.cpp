#include <bits/stdc++.h>

std::vector<int> zoo(const std::vector<int> &w, int cap)
{
    int N = w.size();
    std::unordered_map<int, int> needeed;
    std::vector<int> partner(N, -1);
    std::vector<int> res;

    int heaviest = 0;
    int heaviestIndex = -1;

    for (int i = 0; i < N; i++)
    {
        if (w[i] >= cap)
        {
            continue;
        }
        if (needeed.count(w[i]))
        {
            //We found a partner
            int partnerIndex = needeed[w[i]];
            partner[i] = partnerIndex;

            //Is one of us the heaviest?
            if (w[i] > heaviest)
            {
                heaviest = w[i];
                heaviestIndex = i;
            }
            if (w[partnerIndex] > heaviest)
            {
                heaviest = w[partnerIndex];
                heaviestIndex = partnerIndex;
            }
        }
        else
        {
            //We didnt find a partner, but we get online
            needeed[cap - w[i]] = i;
        }
    }

    if (heaviestIndex == -1)
    {
        return res;
    }
    res.push_back(w[heaviestIndex]);
    res.push_back(w[partner[heaviestIndex]]);

    return res;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}

int main()
{
    printVector(zoo({1, 5, 2, 7, 9, 3}, 10));
    printf("\n");
    printVector(zoo({1, 5, 2, 7, 9, 3}, 2));

    return 0;
}