#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

std::vector<int> kinderSquare(const std::vector<int> &v)
{
    int N = v.size();
    std::vector<int> res;

    for (int i = 0; i < N; i++)
    {
        int sq = v[i] * v[i];
        if (sq % 10 == 5 || sq % 10 == 6)
        {
            continue;
        }
        res.push_back(sq + 10);
    }

    return res;
}

int main()
{
    std::vector<int> input = {};
    printVector(kinderSquare(input));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
