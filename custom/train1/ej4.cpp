#include <bits/stdc++.h>

int fib(int f0, int f1, int term)
{

    std::vector<int> memo(std::max(term + 1, 2), 0);

    memo[0] = f0;
    memo[1] = f1;

    for (int i = 2; i <= term; i++)
    {
        memo[i] = memo[i - 1] + memo[i - 2];
    }

    return memo[term];
}

int JR(int j0, int j1, int j2, int term)
{

    std::vector<int> memo(std::max(term + 1, 3), 0);

    memo[0] = j0;
    memo[1] = j1;
    memo[2] = j2;

    for (int i = 3; i <= term; i++)
    {
        memo[i] = memo[i - 1] + memo[i - 2] + memo[i - 3];
    }

    return memo[term];
}

int main()
{
    printf("%d\n", fib(0, 1, 5));
    printf("%d\n", JR(1, 1, 1, 6));
    return 0;
}