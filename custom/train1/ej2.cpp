#include <bits/stdc++.h>

typedef std::pair<int, int> node;

bool isValid(const node &n, const node &end)
{
    if (std::get<0>(n) > std::get<0>(end))
    {
        return false;
    }
    if (std::get<1>(n) > std::get<1>(end))
    {
        return false;
    }
    return true;
}

bool equals(const node &n, const node &end)
{
    if (std::get<0>(n) == std::get<0>(end) && std::get<1>(n) == std::get<1>(end))
    {
        return true;
    }
    return false;
}

std::string findPath(int xStart, int yStart, int xEnd, int yEnd)
{
    node startNode = node(xStart, yStart);
    node endNode = node(xEnd, yEnd);
    std::queue<node> nextMoves;
    nextMoves.push(startNode);

    while (!nextMoves.empty())
    {
        node current = nextMoves.front();
        nextMoves.pop();
        node m1 = node(std::get<0>(current) + std::get<1>(current), std::get<1>(current));
        node m2 = node(std::get<0>(current), std::get<0>(current) + std::get<1>(current));

        if (equals(m1, endNode))
        {
            return "YES";
        }
        if (equals(m2, endNode))
        {
            return "YES";
        }
        if (isValid(m1, endNode))
        {
            nextMoves.push(m1);
        }
        if (isValid(m2, endNode))
        {
            nextMoves.push(m2);
        }
    }

    return "NO";
}

int main()
{
    printf("%s\n", findPath(1, 1, 2, 5).c_str());
    printf("%s\n", findPath(1, 1, 6, 3).c_str());
    return 0;
}