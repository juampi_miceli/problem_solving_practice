#include <bits/stdc++.h>
#define CENTER 0
#define LEFT 1
#define RIGHT 2

int findBookCorrectPosition(const std::vector<int> &v, int index)
{
    int N = v.size();

    if (index > 0)
    {
        if (v[index] <= v[index - 1])
        {
            return LEFT;
        }
    }
    if (index < N - 1)
    {
        if (v[index] >= v[index + 1])
        {
            return RIGHT;
        }
    }
    return CENTER;
}

int binarySearch(const std::vector<int> &v, int elem)
{
    int N = v.size();

    int pLeft = 0;
    int pRight = N - 1;

    while (pLeft < pRight - 1)
    {
        int mid = (pLeft + pRight) / 2;

        int correctPos = findBookCorrectPosition(v, mid);

        if (correctPos == CENTER)
        {
            //Normal binary search
            if (v[mid] < elem)
            {
                pLeft = mid;
            }
            else if (v[mid] > elem)
            {
                pRight = mid;
            }
            else
            {
                return mid;
            }
        }
        else if (correctPos == LEFT)
        {
            if (v[mid] < elem)
            {
                if (v[mid - 1] == elem)
                {
                    return mid - 1;
                }
                pLeft = mid;
            }
            else if (v[mid] > elem)
            {
                pRight = mid - 1;
            }
            else
            {
                return mid;
            }
        }
        else if (correctPos == RIGHT)
        {
            if (v[mid] < elem)
            {
                pLeft = mid + 1;
            }
            else if (v[mid] > elem)
            {
                if (v[mid + 1] == elem)
                {
                    return mid + 1;
                }
                pRight = mid;
            }
            else
            {
                return mid;
            }
        }
        else
        {
            exit(EXIT_FAILURE);
        }
    }

    if (v[pLeft] == elem)
    {
        return pLeft;
    }
    if (v[pRight] == elem)
    {
        return pRight;
    }

    return -1;
}

int main()
{
    printf("%d\n", binarySearch({2, 1, 4, 3, 15, 20, 27, 31}, 4));
    return 0;
}