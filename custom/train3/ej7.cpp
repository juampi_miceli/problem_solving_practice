#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

typedef std::pair<int, int> costumerLoc;
typedef std::pair<int, int> costumerStatus;

class Market
{
public:
    Market(int _nCashiers, std::vector<int> _cashiersPerformance) : nCashiers(_nCashiers), cashiersPerformance(_cashiersPerformance), queues(std::vector<std::queue<costumerStatus>>(nCashiers, std::queue<costumerStatus>())){};
    void sendToCashier(int ID, int products);
    void advanceTime(int units);
    costumerLoc findCostumer(int ID);

private:
    int nCashiers;
    std::vector<int> cashiersPerformance;
    std::vector<std::queue<costumerStatus>> queues;
};

std::vector<costumerStatus> queue2vector(std::queue<costumerStatus> q)
{
    int N = q.size();
    std::vector<costumerStatus> res;

    for (int i = 0; i < N; i++)
    {
        res.push_back(q.front());
        q.pop();
    }

    return res;
}

void Market::sendToCashier(int ID, int products)
{
    int shortestQueueSize = INT_MAX;
    int shortestQueueIndex = -1;
    for (int i = 0; i < nCashiers; i++)
    {

        int timeLeft = 0;
        std::vector<costumerStatus> queueVector = queue2vector(queues[i]);
        int qSize = queueVector.size();
        for (int i = 0; i < qSize; i++)
        {
            int p = std::get<1>(queueVector[i]) < cashiersPerformance[i] ? std::get<1>(queueVector[i]) : std::get<1>(queueVector[i]) - 1;
            timeLeft += (p / cashiersPerformance[i]) + 1;
        }
        int p = products < cashiersPerformance[i] ? products : products - 1;
        timeLeft += (p / cashiersPerformance[i]) + 1;
        if (timeLeft < shortestQueueSize)
        {
            shortestQueueSize = timeLeft;
            shortestQueueIndex = i;
        }
    }

    queues[shortestQueueIndex].push(costumerStatus(ID, products));
}

costumerLoc Market::findCostumer(int ID)
{
    for (int i = 0; i < nCashiers; i++)
    {
        std::vector<costumerStatus> queueVector = queue2vector(queues[i]);
        for (const costumerStatus &status : queueVector)
        {
            if (std::get<0>(status) == ID)
            {
                return costumerLoc(i, std::get<1>(status));
            }
        }
    }

    return costumerLoc(-1, 0);
}

void Market::advanceTime(int units)
{
    for (int i = 0; i < nCashiers; i++)
    {
        int procUnits = units * cashiersPerformance[i];
        queues[i].front().second -= procUnits;
        if (queues[i].front().second < 0)
        {
            queues[i].pop();
        }
    }
}

void printCostumerLoc(const costumerLoc &c)
{
    printf("%d %d\n", std::get<0>(c), std::get<1>(c));
}

int main()
{
    Market *m = new Market(3, {4, 4, 7});
    m->sendToCashier(1, 10);
    m->sendToCashier(2, 7);
    m->sendToCashier(3, 15);
    printCostumerLoc(m->findCostumer(3));
    m->advanceTime(1);
    m->sendToCashier(4, 4);
    printCostumerLoc(m->findCostumer(3));
    printCostumerLoc(m->findCostumer(4));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
