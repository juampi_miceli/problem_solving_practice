#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

typedef std::pair<int, int> fraction;

int getComunMultiplo(int n1, int n2)
{
    return n1 * n2;
}

fraction suma(const fraction &f1, const fraction &f2)
{

    int num = std::get<0>(f1) * std::get<1>(f2) + std::get<0>(f2) * std::get<1>(f1);

    int den = std::get<1>(f1) * std::get<1>(f2);

    if (num * den < 0)
    {
        den = abs(den);
        num = abs(num) * -1;
    }
    else if (num * den == 0)
    {
        num = 0;
        den = 1;
    }

    return fraction(num, den);
}

fraction resta(const fraction &f1, const fraction &f2)
{

    int num = std::get<0>(f1) * std::get<1>(f2) - std::get<0>(f2) * std::get<1>(f1);

    int den = std::get<1>(f1) * std::get<1>(f2);

    if (num * den < 0)
    {
        den = abs(den);
        num = abs(num) * -1;
    }
    else if (num * den == 0)
    {
        num = 0;
        den = 1;
    }

    return fraction(num, den);
}

void printFraction(const fraction &f)
{
    printf("%d %d\n", std::get<0>(f), std::get<1>(f));
}

void simpl(fraction &f)
{
    int divisor = 2;
    int num = std::get<0>(f);
    int den = std::get<1>(f);
    while (divisor < abs(num) || divisor < abs(den))
    {
        while (num % divisor == 0 && den % divisor == 0)
        {
            num /= divisor;
            den /= divisor;
        }
        divisor++;
    }

    f.first = num;
    f.second = den;
}

void ej2(const fraction &f1, const fraction &f2)
{
    fraction fSum = suma(f1, f2);
    fraction fSub = resta(f1, f2);

    simpl(fSum);
    simpl(fSub);

    printFraction(fSum);
    printFraction(fSub);
}

int main()
{
    ej2(fraction(2, 3), fraction(5, 6));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
