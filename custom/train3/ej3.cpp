#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

int ej3(const std::vector<int> &paths)
{
    int N = paths.size();

    int min = INT_MAX;
    for (int path : paths)
    {
        if (path < min)
        {
            min = path;
        }
    }

    // int nMins = 0;
    // for (int path : paths)
    // {
    //     if (path == min)
    //     {
    //         nMins++;
    //     }
    // }

    int expected = 0;
    // int pMinChoose = nMins / N;
    // int pMinNoChoose = 1 - pMinChoose;
    for (int path : paths)
    {
        expected += path * 2;
        if (path == min)
        {
            expected += 12;
        }
        else
        {
            expected -= 3;
        }
    }
    return expected / N;
}

int main()
{
    printf("%d\n", ej3({35, 10, 15}));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
