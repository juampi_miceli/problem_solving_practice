#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

int ej5(const std::vector<int> &jumps, int platforms)
{
    std::vector<int> memo(platforms + 1, 0);
    memo[platforms] = 1;

    for (int platform = platforms - 1; platform >= 0; platform--)
    {
        for (int jump : jumps)
        {
            if (platform + jump <= platforms)
            {
                memo[platform] += memo[platform + jump];
            }
        }
    }
    return memo[0];
}

int main()
{
    printf("%d\n", ej5({1, 2}, 5));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
