#include <bits/stdc++.h>

/**
 * 
 * ESTE NO ANDA
*/

void printVector(const std::vector<int> &v);

int sumaFea(const std::vector<int> &v)
{
    int N = v.size();
    int sum = 0;

    for (int i = 0; i < (N / 2) - 1; i++)
    {
        sum += abs(v[i] - v[N - i - 1]);
    }

    return sum;
}

int ej6(const std::vector<int> &v)
{
    int N = v.size();

    std::vector<int> res = v;

    for (int i = 1; i < N / 2; i++)
    {
        std::swap(res[N - i], res[i]);
    }

    return sumaFea(res);
}

int main()
{
    printf("%d\n", ej6({1, 3, 4, 5, 10, 17}));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
