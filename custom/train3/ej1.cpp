#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

bool ej1(const std::string &text)
{
    std::vector<bool> used('z' - 'a' + 1, false);

    for (char c : text)
    {
        if (c >= 'A' && c <= 'Z')
        {
            c = (char)(tolower(c));
        }
        if (c >= 'a' && c <= 'z')
        {
            used[c - 'a'] = true;
        }
    }

    for (bool b : used)
    {
        if (!b)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    printf("%d\n", ej1("The quick brown fox jumps over the lazy dog."));
    printf("%d\n", ej1("Yesterday, the last Friday of June, Maribel knowingly mislead the readers of the Zoolander newspaper."));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
