#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

int binarySearch(const std::vector<int> &v, int distance)
{
    int N = v.size();

    int left = 0;
    int right = N - 1;

    while (right - left > 1)
    {
        int mid = (left + right) / 2;
        if (v[mid] == distance * mid)
        {
            //Todavia nada
            left = mid;
        }
        else
        {
            //Me pase
            right = mid;
        }
    }

    if (v[left] > distance * left)
    {
        return distance * left;
    }
    if (v[right] > distance * right)
    {
        return distance * right;
    }

    return distance * N;
}

int main()
{
    printf("%d\n", binarySearch({0, 5, 10, 15, 20, 25, 30, 35}, 5));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
