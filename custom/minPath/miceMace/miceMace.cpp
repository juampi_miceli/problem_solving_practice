#include <bits/stdc++.h>

void updateDistances(const std::vector<std::vector<int>> &adyacent, std::vector<int> &distances, int v);

int visitCloserNode(const std::vector<int> &distances, std::vector<bool> &visited);

std::vector<int> dijkstra(const std::vector<std::vector<int>> &adyacent, int v);

int miceMace(const std::vector<std::vector<int>> &adyacent, int timerStart, int endNode);

/**
 * 
*/
int miceMace(const std::vector<std::vector<int>> &adyacent, int timerStart, int endNode)
{
    std::vector<int> distances = dijkstra(adyacent, endNode);
    int nMouses = 0;
    for (int distance : distances)
    {
        if (distance <= timerStart)
        {
            nMouses++;
        }
    }
    return nMouses;
}

int main()
{
    int T, N, M, endNode, timerStart;
    scanf("%d", &T);

    for (int j = 0; j < T; j++)
    {

        scanf("%d %d %d %d", &N, &endNode, &timerStart, &M);
        endNode--;
        std::vector<std::vector<int>> adyacent(N, std::vector<int>(N, INT_MAX));
        for (int i = 0; i < N; i++)
        {
            adyacent[i][i] = 0;
        }
        while (M > 0)
        {
            int n1, n2, val;
            scanf("%d %d %d", &n1, &n2, &val);
            //We store the value backwards
            adyacent[n2 - 1][n1 - 1] = val;
            M--;
        }
        printf("%d\n", miceMace(adyacent, timerStart, endNode));
        if (j < T - 1)
        {
            printf("\n");
        }
    }

    return 0;
}

void updateDistances(const std::vector<std::vector<int>> &adyacent, std::vector<int> &distances, int v)
{
    int N = distances.size();
    for (int i = 0; i < N; i++)
    {
        if (adyacent[v][i] == INT_MAX)
        {
            continue;
        }
        long prevDistance = distances[i];
        long newDistance = (long)(distances[v]) + (long)(adyacent[v][i]);
        distances[i] = std::min(prevDistance, newDistance);
    }
}

int visitCloserNode(const std::vector<int> &distances, std::vector<bool> &visited)
{
    int N = distances.size();

    int minDistance = INT_MAX;
    int winnerNode = -1;
    for (int i = 0; i < N; i++)
    {
        if (!visited[i] && distances[i] <= minDistance)
        {
            minDistance = distances[i];
            winnerNode = i;
        }
    }

    if (winnerNode >= 0)
    {
        visited[winnerNode] = true;
    }

    return winnerNode;
}

std::vector<int> dijkstra(const std::vector<std::vector<int>> &adyacent, int v)
{
    int N = adyacent.size();
    std::vector<int> distances(N, INT_MAX);
    std::vector<bool> visited(N, false);

    int currentNode = v;

    distances[currentNode] = 0;
    visited[currentNode] = true;

    for (int i = 1; i < N; i++)
    {
        if (currentNode < 0)
        {
            break;
        }
        updateDistances(adyacent, distances, currentNode);
        currentNode = visitCloserNode(distances, visited);
    }

    return distances;
}
