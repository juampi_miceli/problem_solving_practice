#include <bits/stdc++.h>

template <class T>
void printVector(const std::vector<T> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        std::cout << v[i] << ", ";
    }
    std::cout << "]";
}

template <class T>
void printMatrix(const std::vector<std::vector<T>> &mat)
{
    int N = mat.size();
    for (int i = 0; i < N; i++)
    {
        printVector(mat[i]);
        if (i < N - 1)
        {
            printf("\n");
        }
    }
}

std::vector<unsigned long> gregGraph(const std::vector<std::vector<unsigned long>> &adyacent, const std::vector<int> &removingOrder)
{
    int N = adyacent.size();
    std::vector<std::vector<unsigned long>> L(N, std::vector<unsigned long>(N, 0));

    std::vector<unsigned long> res(N, 0);

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            L[i][j] = adyacent[removingOrder[i]][removingOrder[j]];
        }
    }
    for (int k = 0; k < N; k++)
    {
        int actualK = N - k - 1;
        for (int i = actualK; i < N; i++)
        {
            for (int j = actualK; j < N; j++)
            {

                L[actualK][i] = std::min(L[actualK][i], L[actualK][j] + L[j][i]);
                L[i][actualK] = std::min(L[i][actualK], L[i][j] + L[j][actualK]);
            }
            res[actualK] += L[actualK][i] + L[i][actualK];
        }

        for (int i = actualK + 1; i < N; i++)
        {
            for (int j = actualK + 1; j < N; j++)
            {
                L[i][j] = std::min(L[i][j], L[i][actualK] + L[actualK][j]);
                res[actualK] += L[i][j];
            }
        }
    }
    // for (int k = 0; k < N; k++)
    // {
    //     for (int i = k; i >= 0; i--)
    //     {
    //         for (int j = k; j >= 0; j--)
    //         {
    //             L[i][j] = std::min(L[i][j], L[i][k] + L[k][j]);
    //             res[N - k - 1] += L[i][j];
    //         }
    //     }
    // }

    return res;
}

int main()
{
    int N;
    std::cin >> N;

    std::vector<std::vector<unsigned long>> adyacent(N, std::vector<unsigned long>(N, INT_MAX));
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            long val;
            std::cin >> val;
            adyacent[i][j] = val;
        }
    }

    std::vector<int> removingOrder(N);
    for (int i = 0; i < N; i++)
    {
        int val;
        std::cin >> val;
        removingOrder[i] = val - 1;
        // removingOrder[N - i - 1] = val - 1;
    }
    std::vector<unsigned long> res = gregGraph(adyacent, removingOrder);

    for (int i = 0; i < N; i++)
    {
        std::cout << res[i] << " ";
    }
    printf("\n");
    return 0;
}