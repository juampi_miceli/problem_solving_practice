#include <bits/stdc++.h>

typedef std::pair<int, int> edge;

void transpose(std::vector<std::vector<int>> &mat)
{

    int N = mat.size();
    for (int i = 0; i < N; i++)
    {
        for (int j = i; j < N; j++)
        {
            std::swap(mat[i][j], mat[j][i]);
        }
    }
}

int updateNeighbors(std::vector<int> &distances, std::vector<bool> &visited, const std::vector<std::vector<int>> &adyacent, int v, int current)
{
    int N = distances.size();
    int min = INT_MAX;
    int argMin = -1;
    for (int i = 0; i < N; i++)
    {
        long previousDistance = distances[i];
        long newDistance = (long)(distances[current]) + (long)(adyacent[current][i]);
        distances[i] = std::min(previousDistance, newDistance);

        if (!visited[i])
        {
            //Was not yet in the min path tree
            if (distances[i] <= min)
            {
                min = distances[i];
                argMin = i;
            }
        }
    }

    return argMin;
}

std::vector<int> dijkstra(const std::vector<std::vector<int>> &adyacent, int v)
{
    int N = adyacent.size();
    std::vector<int> distances(N, INT_MAX);
    std::vector<bool> visited(N, false);
    int current = v;
    distances[current] = 0;

    for (int i = 1; i < N; i++)
    {
        if (current == -1)
        {
            printf("Al lobby, N: %d, i: %d\n", N, i);
        }
        visited[current] = true;
        current = updateNeighbors(distances, visited, adyacent, v, current);
    }
    return distances;
}

int almostShortestPath(std::vector<std::vector<int>> &adyacent, std::vector<edge> &edges, int s, int t)
{
    int N = adyacent.size();
    std::vector<int> distancesFromS = dijkstra(adyacent, s);
    transpose(adyacent);
    std::vector<int> distancesFromT = dijkstra(adyacent, t);

    long shortestPathVal = distancesFromS[t];

    for (const edge &_edge : edges)
    {
        long currentDistance = (long)(adyacent[std::get<1>(_edge)][std::get<0>(_edge)]) + (long)(distancesFromS[std::get<0>(_edge)]) + (long)(distancesFromT[std::get<1>(_edge)]);
        if (currentDistance == shortestPathVal)
        {
            adyacent[std::get<1>(_edge)][std::get<0>(_edge)] = INT_MAX;
        }
    }

    transpose(adyacent);
    distancesFromS = dijkstra(adyacent, s);

    return distancesFromS[t] == INT_MAX ? -1 : distancesFromS[t];
}

int main()
{
    int N, M, s, t;

    while (true)
    {
        scanf("%d", &N);
        scanf("%d", &M);
        if (N == 0 && M == 0)
        {
            break;
        }
        scanf("%d", &s);
        scanf("%d", &t);

        std::vector<std::vector<int>> adyacent(N, std::vector<int>(N, INT_MAX));
        std::vector<edge> edges;

        for (int i = 0; i < M; i++)
        {
            int v1, v2, val;
            scanf("%d", &v1);
            scanf("%d", &v2);
            scanf("%d", &val);
            adyacent[v1][v2] = val;
            adyacent[v1][v1] = 0;
            adyacent[v2][v2] = 0;
            edges.push_back(edge(v1, v2));
        }

        int res = almostShortestPath(adyacent, edges, s, t);
        printf("%d\n", res);
    }

    return 0;
}