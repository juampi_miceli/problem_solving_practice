#include <bits/stdc++.h>

std::string decimalToBinaryString(int decimal, int digits);
std::string applySwitch(const std::string &state, const std::vector<bool> &_switch);
std::vector<std::string> getUnvisitedNeighborsAndUpdateDistance(std::string currentState, std::unordered_map<std::string, int> &states, const std::vector<std::vector<bool>> &switches, std::queue<std::string> &nextStates);
int switchTheLights(const std::vector<std::vector<bool>> &switches);

/**
 * N = number of lamps 
 * M = number of switches
 * 
 * We can think this as a minimum path problem where each node of the graph is a state of the lamps and two nodes are
 * connected if there is a switch that goes from one lamps state to the other. Then our problem is to go from the state
 * where all lamps are on, to the state where they are all off. We will model each state as a string with N chars
 * where char i will be '0' if lamp i is off, and '1' otherwise. Then, there exists 2^M different states. We could use
 * and array and store in position i the number of switches that we have to use to travel from state ALL_ON, to state
 * i. The problem with that is that we need to have in memory from the beginning a vector of size 2^M when we might 
 * get to the solution in just one switch. That's why we will use a hash map that will have as key all the different
 * strings representing states and as it values the number of switches needed to travel from ALL_ON to that state.
 * This will allow us to use just as much memory as we need and we will keep having access to the values in O(1).
 * Turning the switch will have a cost of O(N) as we need to know the impact of it in every lamp and every state
 * will have M neighbors. As our graph is not weighted we can use BFS to traverse it from ALL_ON to ALL_OFF, as 
 * that will tell us exactly the distance between the two nodes. If at the end of BFS we have reached the state
 * ALL_OFF is because we can reach that state in a finite number of switches. Otherwise, that will mean that 
 * there is no way of getting from one state to the other. BFS has a time complexity of O(N+M), so all in all
 * our total time complexity will be of O((N+M)*N) because of the time we lose calculating the switchs transitions. 
*/
int switchTheLights(const std::vector<std::vector<bool>> &switches)
{
    int Nlamps = switches[0].size();
    std::unordered_map<std::string, int> states;
    int Nstates = pow(2, Nlamps);
    std::string currentState = decimalToBinaryString(Nstates - 1, Nlamps);
    std::string finalState = decimalToBinaryString(0, Nlamps);
    states[currentState] = 0;

    std::queue<std::string> nextStates;
    nextStates.push(currentState);

    while (!nextStates.empty() && currentState != finalState)
    {
        currentState = nextStates.front();
        nextStates.pop();

        std::vector<std::string> neighbors = getUnvisitedNeighborsAndUpdateDistance(currentState, states, switches, nextStates);
    }

    return states.count(finalState) > 0 ? states[finalState] : -1;
}

int main()
{

    int Ntests, Nlamps, Nswitches;

    scanf("%d", &Ntests);
    for (int k = 0; k < Ntests; k++)
    {
        scanf("%d %d", &Nlamps, &Nswitches);

        std::vector<std::vector<bool>> switches(Nswitches);

        for (int i = 0; i < Nswitches; i++)
        {
            std::vector<bool> _switch(Nlamps, false);
            for (int j = 0; j < Nlamps; j++)
            {
                int val;
                scanf("%d", &val);
                if (val == 1)
                {
                    _switch[j] = true;
                }
            }
            switches[i] = _switch;
        }

        int res = switchTheLights(switches);
        if (res == -1)
        {
            printf("Case %d: IMPOSSIBLE\n", k + 1);
        }
        else
        {
            printf("Case %d: %d\n", k + 1, res);
        }
    }
    return 0;
}

std::string decimalToBinaryString(int decimal, int digits)
{
    std::string res;
    for (int i = 0; i < digits; i++)
    {
        int resto = decimal % 2;
        decimal = decimal >> 1;
        res.push_back(resto + '0');
    }
    std::reverse(res.begin(), res.end());
    return res;
}

std::string applySwitch(const std::string &state, const std::vector<bool> &_switch)
{
    int Nlamps = _switch.size();
    std::string res = state;

    for (int i = 0; i < Nlamps; i++)
    {
        if (_switch[i])
        {
            res[i] = state[i] == '0' ? '1' : '0';
        }
    }
    return res;
}

std::vector<std::string> getUnvisitedNeighborsAndUpdateDistance(std::string currentState, std::unordered_map<std::string, int> &states, const std::vector<std::vector<bool>> &switches, std::queue<std::string> &nextStates)
{
    std::vector<std::string> neighbors;
    int currentStateValue = states[currentState];
    for (const std::vector<bool> &_switch : switches)
    {
        std::string neighbor = applySwitch(currentState, _switch);
        if (states.count(neighbor) == 0)
        {
            neighbors.push_back(neighbor);
            states[neighbor] = currentStateValue + 1;
            nextStates.push(neighbor);
        }
    }
    return neighbors;
}
