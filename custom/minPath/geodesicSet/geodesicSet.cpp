#include <bits/stdc++.h>

typedef std::pair<int, int> edge;

void printVector(const std::vector<int> &v);

std::vector<int> getUnvisitedNeighbors(const std::vector<std::vector<bool>> &adyacent, const std::vector<bool> &visited, int v);

void pushAll(std::queue<int> &_queue, const std::vector<int> &elems);

void updateDistances(std::vector<int> &distances, const std::vector<int> &neighbors, int v);

void visitAll(std::vector<bool> &visited, const std::vector<int> &neighbors);

std::vector<int> bfs(const std::vector<std::vector<bool>> &adyacent, int v);

bool isGeodesic(const std::vector<std::vector<int>> &distances, const std::vector<int> &testNodes, int node);

bool geodesicSet(const std::vector<std::vector<bool>> &adyacent, const std::vector<int> &testNodes);

int main()
{
    int N;
    while ((scanf("%d", &N)) != EOF)
    {
        getchar();
        std::vector<std::vector<bool>> adyacent(N, std::vector<bool>(N, false));
        for (int i = 0; i < N; i++)
        {
            std::string line;
            getline(std::cin, line);
            std::istringstream iss(line);
            int node;
            while (iss >> node)
            {
                adyacent[i][node - 1] = true;
                adyacent[node - 1][i] = true;
            }
        }
        int Ntests;
        std::cin >> Ntests;
        getchar();
        for (int i = 0; i < Ntests; i++)
        {
            std::string line;
            getline(std::cin, line);
            std::istringstream iss(line);
            int node;
            std::vector<int> testNodes;
            while (iss >> node)
            {
                testNodes.push_back(node - 1);
            }
            if (geodesicSet(adyacent, testNodes))
            {
                printf("yes\n");
            }
            else
            {
                printf("no\n");
            }
        }
    }
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}

std::vector<int> getUnvisitedNeighbors(const std::vector<std::vector<bool>> &adyacent, const std::vector<bool> &visited, int v)
{
    int N = adyacent.size();
    std::vector<int> res;
    for (int i = 0; i < N; i++)
    {
        if (adyacent[i][v] && !visited[i])
        {
            //If the nodes are adyacent and the new node has not been visited
            res.push_back(i);
        }
    }

    return res;
}

void pushAll(std::queue<int> &_queue, const std::vector<int> &elems)
{
    for (int elem : elems)
    {
        _queue.push(elem);
    }
}

void updateDistances(std::vector<int> &distances, const std::vector<int> &neighbors, int v)
{
    for (int neighbor : neighbors)
    {
        distances[neighbor] = distances[v] + 1;
    }
}

void visitAll(std::vector<bool> &visited, const std::vector<int> &neighbors)
{
    for (int neighbor : neighbors)
    {
        visited[neighbor] = true;
    }
}

std::vector<int> bfs(const std::vector<std::vector<bool>> &adyacent, int v)
{
    int N = adyacent.size();
    std::queue<int> nextNodes;
    nextNodes.push(v);

    std::vector<int> distances(N, -1);
    distances[v] = 0;
    std::vector<bool> visited(N, false);
    visited[v] = true;

    while (!nextNodes.empty())
    {
        int currentNode = nextNodes.front();
        nextNodes.pop();

        std::vector<int> unvisitedNeighbors = getUnvisitedNeighbors(adyacent, visited, currentNode);
        visitAll(visited, unvisitedNeighbors);
        pushAll(nextNodes, unvisitedNeighbors);
        updateDistances(distances, unvisitedNeighbors, currentNode);
    }

    return distances;
}

bool isGeodesic(const std::vector<std::vector<int>> &distances, const std::vector<int> &testNodes, int node)
{
    int Ntests = testNodes.size();
    for (int i = 0; i < Ntests; i++)
    {
        for (int j = i + 1; j < Ntests; j++)
        {
            int minDist = distances[testNodes[i]][testNodes[j]];
            int iToNode = distances[testNodes[i]][node];
            int nodeToJ = distances[testNodes[j]][node];
            if (iToNode + nodeToJ == minDist)
            {
                return true;
            }
        }
    }
    return false;
}

bool geodesicSet(const std::vector<std::vector<bool>> &adyacent, const std::vector<int> &testNodes)
{
    int N = adyacent.size();
    int Ntests = testNodes.size();
    std::vector<std::vector<int>> distances;
    int pointer = 0;

    for (int i = 0; i < N && pointer < Ntests; i++)
    {

        if (testNodes[pointer] == i)
        {
            pointer++;
            distances.push_back(bfs(adyacent, i));
        }
        else
        {
            distances.push_back({});
        }
    }

    for (int i = 0; i < N; i++)
    {
        if (!isGeodesic(distances, testNodes, i))
        {
            return false;
        }
    }

    return true;
}