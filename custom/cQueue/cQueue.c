#include <stdlib.h>
#include <stdio.h>
#include "cQueue.h"

// int main()
// {
//     struct queue_t *queue = newQueue();
//     char input[10] = "555";
//     char *token = (char *)malloc(10);
//     for (int i = 0; i < 10; i++)
//     {
//         token[i] = input[i];
//     }
//     // char *token2 = (char *)(malloc(2));
//     // token2 = "2\0";
//     // char *token3 = (char *)(malloc(5));
//     // token3 = "3123\0";
//     queuePush(queue, token);
//     // queuePush(queue, token2);
//     // queuePush(queue, token3);
//     queuePrint(queue);
//     // queuePop(queue);
//     // queuePop(queue);
//     // queuePrint(queue);
//     // queuePop(queue);
//     // queuePrint(queue);
//     // queuePop(queue);
//     // queuePop(queue);
//     // queuePop(queue);
//     // queuePrint(queue);
//     deleteQueue(queue);
//     // free(token);
//     return 0;
// }

/**=============================
 * =           QUEUE           =
 * =============================
*/

struct queue_t *newQueue()
{
    struct queue_t *queue = (struct queue_t *)(malloc(sizeof(struct queue_t)));
    queue->first = NULL;
    queue->last = NULL;
    queue->size = 0;
    return queue;
}

char *queueTop(struct queue_t *queue)
{
    if (queue->size == 0)
    {
        return NULL;
    }
    return queue->first->value;
}

u_int16_t queuePush(struct queue_t *queue, char *value)
{
    struct queueNode_t *newNode = newQueueNode(value);
    newNode->front = queue->last;
    if (queue->size == 0)
    {
        queue->first = newNode;
    }
    else
    {
        queue->last->back = newNode;
    }

    queue->last = newNode;
    return ++(queue->size);
}

u_int16_t queuePop(struct queue_t *queue)
{
    if (queue->size == 0)
    {
        return 0;
    }
    struct queueNode_t *newFirst = queue->first->back;
    free(queue->first->value);
    free(queue->first);
    if (queue->size == 1)
    {
        queue->last = NULL;
    }
    else
    {
        newFirst->front = NULL;
    }

    queue->first = newFirst;
    return --(queue->size);
}

void deleteQueue(struct queue_t *queue)
{
    while (queue->size > 0)
    {
        queuePop(queue);
    }
    free(queue);
}

void queuePrint(struct queue_t *queue)
{
    if (queue == NULL)
    {
        return;
    }
    u_int16_t N = queue->size;
    struct queueNode_t *node = queue->first;
    while (node != NULL)
    {
        printf("%s ", node->value);
        node = node->back;
    }
    printf("\n");
}

/**==============================
 * =         QUEUE NODE         =
 * ==============================
*/

struct queueNode_t *newQueueNode(char *c)
{
    struct queueNode_t *newNode = (struct queueNode_t *)(malloc(sizeof(struct queueNode_t)));
    newNode->value = c;
    newNode->front = NULL;
    newNode->back = NULL;

    return newNode;
}