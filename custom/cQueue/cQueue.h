#include <stdlib.h>
#include <stdio.h>

#ifndef C_QUEUE_H
#define C_QUEUE_H

/**==============================
 * =         QUEUE NODE         =
 * ==============================
*/

struct queueNode_t
{
    char *value;
    struct queueNode_t *front;
    struct queueNode_t *back;
};

struct queueNode_t *newQueueNode(char *c);

/**=============================
 * =           QUEUE           =
 * =============================
*/

struct queue_t
{
    struct queueNode_t *first;
    struct queueNode_t *last;
    u_int16_t size;
};

struct queue_t *newQueue();

char *queueTop(struct queue_t *queue);
u_int16_t queuePush(struct queue_t *queue, char *value);
u_int16_t queuePop(struct queue_t *queue);
void deleteQueue(struct queue_t *queue);
void queuePrint(struct queue_t *queue);

#endif //C_QUEUE_H