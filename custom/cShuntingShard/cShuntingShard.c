#include <stdlib.h>
// #include <stdio.h>
#include "../cStack/cStack.h"
#include "../cQueue/cQueue.h"

#define RIGHT 0
#define LEFT 1
#define NAN 2
#define N_SYMBOLS 7

char symbols[N_SYMBOLS] = {'+', '-', '*', '/', '^', '(', ')'};
char precedence[N_SYMBOLS] = {2, 2, 3, 3, 4, 5, 5};
char associativity[N_SYMBOLS] = {LEFT, LEFT, LEFT, LEFT, RIGHT, NAN, NAN};

/**
 * Returns the size of the string including the '\0'
*/
u_int16_t stringSize(char *infixString)
{
    int N = 0;
    while (infixString[N] != '\0')
    {
        N++;
    }
    return N;
}

int isANumber(char c)
{
    return c >= '0' && c <= '9' ? 1 : 0;
}

int isASymbol(char c)
{
    for (int i = 0; i < N_SYMBOLS; i++)
    {
        if (symbols[i] == c)
        {
            return 1;
        }
    }
    return 0;
}

int isValid(char c)
{
    if (isANumber(c) == 1 || isASymbol(c) == 1)
    {
        return 1;
    }
    return 0;
}

int intSize(char *number, int position)
{
    int i = 0;
    char c = number[position];
    while (c != '\0' && isANumber(c))
    {
        i++;
        c = number[++position];
    }
    return i + 1;
}

char *parseInt(char *number, int *position)
{
    u_int16_t N = intSize(number, *position);
    char *token = (char *)(malloc(N));
    for (int i = 0; i < N - 1; i++)
    {
        token[i] = number[(*position)++];
    }
    token[N - 1] = '\0';
    return token;
}

void skipSpaces(char *text, int *position)
{
    char c = text[*position];

    while (c == ' ')
    {
        c = text[++(*position)];
    }
}

/**
 * Reads next token from text, starting from position in the token char pointer.
 * At the end of the function, the position integer is updated
*/
char *getNextToken(char *text, int *position)
{

    //Advance either to next token or to end of string
    skipSpaces(text, position);

    char c = text[*position];

    if (c == '\0')
    {
        (*position)++;
        return NULL;
    }
    if (!isValid(c))
    {
        // fprintf(stderr, "INVALID CHAR: %c AT POSITION: %d\n", c, *position);
        exit(EXIT_FAILURE);
    }
    if (isANumber(c))
    {
        return parseInt(text, position);
    }
    if (isASymbol(c))
    {
        (*position)++;

        char *token = (char *)(malloc(10));
        token[0] = c;
        token[1] = '\0';
        return token;
    }
    // fprintf(stderr, "PASO ALGO RARO CHAR: %c AT POSITION: %d\n", c, *position);
    exit(EXIT_FAILURE);
}

char getSymbolIndex(char c)
{
    for (char i = 0; i < N_SYMBOLS; i++)
    {
        if (c == symbols[i])
        {
            return i;
        }
    }
    return -1;
}

struct queue_t *shuntingShard(char *text)
{
    const u_int16_t N = stringSize(text);
    struct queue_t *polishNotation = newQueue();

    struct stack_t *stack = newStack();

    int position = 0;
    while (position < N)
    {
        char *token = getNextToken(text, &position);
        if (token == NULL)
        {
            break;
        }

        if (isANumber(token[0]))
        {
            queuePush(polishNotation, token);
        }
        else if (token[0] == '(')
        {
            stackPush(stack, token);
        }
        else if (token[0] == ')')
        {
            while (stack->size > 0 && stackTop(stack)[0] != '(')
            {
                char *newToken = (char *)(malloc(10));
                newToken[0] = stackTop(stack)[0];
                newToken[1] = stackTop(stack)[1];
                queuePush(polishNotation, newToken);
                stackPop(stack);
            }
            if (stack->size == 0)
            {
                // fprintf(stderr, "MISMATCH PARENTHESIS\n");
                exit(EXIT_FAILURE);
            }
            stackPop(stack);
            //This token is not going to
            free(token);
        }
        else
        {
            char c = token[0];
            char currentIndex = getSymbolIndex(c);
            char currentPrecedence = precedence[currentIndex];
            char currentAssociativity = associativity[currentIndex];

            while (stack->size > 0 && stackTop(stack)[0] != '(' && (precedence[getSymbolIndex(stackTop(stack)[0])] > currentPrecedence || (precedence[getSymbolIndex(stackTop(stack)[0])] == currentPrecedence && currentAssociativity == LEFT)))
            {
                char *newToken = (char *)(malloc(10));
                newToken[0] = stackTop(stack)[0];
                newToken[1] = stackTop(stack)[1];
                queuePush(polishNotation, newToken);
                stackPop(stack);
            }
            stackPush(stack, token);
        }
    }

    while (stack->size > 0)
    {
        queuePush(polishNotation, stackTop(stack));
        stackPop(stack);
    }

    deleteStack(stack);
    return polishNotation;
}

int main()
{
    char text[] = "1+1" /*"(5+256)*((2/3123)^10)"*/;
    printf("input: %s\n", text);
    struct queue_t *polishNotation = shuntingShard(text);
    printf("output: ");
    queuePrint(polishNotation);

    deleteQueue(polishNotation);
    return 0;
}