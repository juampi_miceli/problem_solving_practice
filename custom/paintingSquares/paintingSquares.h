#include <bits/stdc++.h>

int paintingSquaresConstant(int squares, int colors);
int paintingSquaresBF(int squares, int colors);
int recursivePaintingSquares(int squares, int colors);
int recursivePaintingSquaresBF(int squares, int colors, std::vector<int> &currentPainting);
bool isValidPainting(const std::vector<int> &painting);