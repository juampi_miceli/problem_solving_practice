#include "paintingSquares.h"

/**
 * The first algorithm we can think of is brute force. We will generate every single combination of paintings in order. Supposing
 * we have K colors and N squares. Then we have K options for each square, therefore we can paint the squares in K^N ways. For
 * each of this combinations we have to check if that solution is valid, we can do that in O(N), So the final time complexity
 * is O(K^N * N).
 * 
 * To generate all the combinations we will do a recursive algorithm, that will paint the squares from left to right. In the step
 * k, the algorithm will paint square k with a color j, and advance to the next square to find every possible combination with the
 * k-th square painted with color j. When the algorithm reaches the final square, it will determine the current combination is valid.
 * If yes, the number of possible ways increments in one.
*/
int paintingSquaresBF(int squares, int colors)
{
    std::vector<int> currentPainting;
    return recursivePaintingSquaresBF(squares, colors, currentPainting);
}

int recursivePaintingSquaresBF(int squares, int colors, std::vector<int> &currentPainting)
{
    int N = currentPainting.size();
    if (squares <= N)
    {
        //We painted all the squares
        if (isValidPainting(currentPainting))
        {
            return 1;
        }
        return 0;
    }

    int possibleWays = 0;

    for (int i = 0; i < colors; i++)
    {
        //Paint with every possible color
        currentPainting.push_back(i);
        possibleWays += recursivePaintingSquaresBF(squares, colors, currentPainting);
        currentPainting.pop_back();
    }
    return possibleWays;
}

bool isValidPainting(const std::vector<int> &painting)
{
    int N = painting.size();
    for (int i = 0; i < N - 1; i++)
    {
        if (painting[i] == painting[i + 1])
        {
            return false;
        }
    }

    return true;
}

/**
 * The algorithm shown above has a horrible time complexity, but running it a few times with small test cases helps us realize that
 * this problem can be solved in O(1), as the final result depends in a formula. The final formula is 
 *         
 * $ (K-1)^(N-1)*(K) $
 * 
 * Let's see why:
 * 
 * Supposing we have K colors and 2 squares, we can easily see that we have (K-1)*K possible ways. That's because we can leave a
 * fixed value for the first square, let's say color 1, and then we could paint the second square with colors 2 to K. Up to now
 * we have (K-1) ways. But that was fixing the color in square 1 to color 1. Is not difficult to see that it is the same if in 
 * the first position we use color 1 or color K, they are just symmetrical answers. Then we will have (K-1) ways for each of 
 * the K colors. Thus, the answer with 2 squares is (K-1)*K.
 * 
 * What happens if we add a third square? For each combination we had previously, we just add (K-1) new ways, as we can add
 * every color that is differen to the color in the last square. Then, every time we add a square we will have to multiply
 * the total solutions we had by (K-1). 
*/
int paintingSquaresConstant(int squares, int colors)
{
    return (int)(std::pow((colors - 1), squares - 1)) * colors;
}

/**
 * An alternative way of solving the problem is recursively. This way we will be constructing the formula above written step by step.
 * This would take more space and time because of the recursive calls, but is a bit clearer as how we get the final result.
 * It's time complexity is O(N).
*/
int recursivePaintingSquares(int squares, int colors)
{
    if (squares == 1)
    {
        return colors;
    }
    return (colors - 1) * recursivePaintingSquares(squares - 1, colors);
}

int main()
{
    for (int squares = 1; squares < 8; squares++)
    {
        for (int colors = 1; colors < 10; colors++)
        {
            int res1 = paintingSquaresBF(squares, colors);
            int res2 = recursivePaintingSquares(squares, colors);
            int res3 = paintingSquaresConstant(squares, colors);
            if (res1 != res2 || res1 != res3)
            {
                fprintf(stderr, "squares: %d, colors: %d, BF: %d, cte: %d", squares, colors, res1, res2);
                exit(EXIT_FAILURE);
            }
        }
    }
    printf("Tests passed!!\n");

    return 0;
}