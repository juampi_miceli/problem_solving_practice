#include <bits/stdc++.h>

using namespace std;

void printVector(const std::vector<int> &v);

int whereShouldBe(const std::vector<int> &v, int elem, bool inclusive)
{

    int N = v.size();
    if (v.empty())
    {
        return -1;
    }

    int left = 0;
    int right = v.size() - 1;

    while (right - left > 1)
    {
        int mid = (left + right) / 2;
        if (v[mid] < elem)
        {
            left = mid;
        }
        else if (v[mid] > elem)
        {
            right = mid;
        }
        else
        {
            if (inclusive)
            {
                while (mid < N && v[mid] == elem)
                {
                    mid++;
                }
                return mid;
            }
            else
            {
                while (mid >= 0 && v[mid] == elem)
                {
                    mid--;
                }
                return mid + 1;
            }
        }
    }

    if (v[left] == elem)
    {
        if (inclusive)
        {
            return left;
        }
        else
        {
            return left + 1;
        }
    }
    if (v[right] == elem)
    {
        if (inclusive)
        {
            return right;
        }
        else
        {
            return right + 1;
        }
    }

    return right;
}

int cyclists_in_picture(vector<int> speeds, int time, int cameraPosition, int cameraRange)
{
    // Write your code here
    int N = speeds.size();
    std::vector<int> positions;
    for (int i = 0; i < N; i++)
    {
        positions.push_back(time * speeds[i]);
    }

    int leftCamera = cameraPosition - cameraRange;
    int rightCamera = cameraPosition;

    int leftIndex = whereShouldBe(positions, leftCamera, false);
    int rightIndex = whereShouldBe(positions, rightCamera, true);
    return rightIndex - leftIndex;
}

int main()
{
    int res = cyclists_in_picture({1, 2, 3, 4, 4, 5, 6, 7}, 1, 4, 1);
    printf("%d\n", 2);
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}