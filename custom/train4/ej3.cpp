#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <iterator>
#include <tuple>
#include <cstring>
#include <iomanip>
#include <cmath>
using namespace std;

typedef vector<tuple<int, int>> tuple_list;

void printVector(const std::vector<int> &v);

double probability_of_activating_at_least_two_traps(tuple_list doors)
{
    // Write your code here
    int Ndoors = doors.size();
    double pNinguno = 1;
    for (int i = 0; i < Ndoors; i++)
    {
        double N = get<0>(doors[i]);
        double K = get<1>(doors[i]);
        pNinguno *= (N - K) / N;
    }
    double pUno = 1;
    for (int i = 0; i < Ndoors; i++)
    {
        for (int j = 0; j < Ndoors; j++)
        {
            double N = get<0>(doors[i]);
            double K = get<1>(doors[i]);
            if (i == j)
            {
                //Activa
                pUno += K / N;
            }
            else
            {
                //No activa
                pUno += (N - K) / N;
            }
        }
    }
    return 1 - pNinguno - pUno;
}

int main()
{
    printf("%d\n", 2);
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}