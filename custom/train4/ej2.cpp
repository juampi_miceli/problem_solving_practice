#include <bits/stdc++.h>

using namespace std;

void printVector(const std::vector<int> &v);

typedef std::pair<int, int> node;

bool equals(const node &n, const node &end)
{
    if (std::get<0>(n) == std::get<0>(end) && std::get<1>(n) == std::get<1>(end))
    {
        return true;
    }
    return false;
}

int fibBottomUp(int term, std::vector<int> &fibMemo)
{
    if (fibMemo[term] == -1)
    {
        fibMemo[term] = fibMemo[term - 1] + fibMemo[term - 2];
    }

    return fibMemo[term];
}

int manhattan(const node &n1, const node &n2)
{
    return abs(std::get<0>(n1) - std::get<0>(n2) + abs(std::get<1>(n1) > std::get<1>(n2)));
}

bool isObstacle(const node &n, const std::vector<node> &obs)
{
    int N = obs.size();
    for (int i = 0; i < N; i++)
    {
        if (equals(n, obs[i]))
        {
            return true;
        }
    }
    return false;
}

bool isValid(const node &n, const node &end, const std::vector<node> &obs)
{
    if (std::get<0>(n) > std::get<0>(end))
    {
        return false;
    }
    if (std::get<1>(n) > std::get<1>(end))
    {
        return false;
    }
    if (isObstacle(n, obs))
    {
        return false;
    }
    return true;
}

bool isPossible(int xStart, int yStart, int xEnd, int yEnd, vector<int> xObs, vector<int> yObs)
{

    node startNode = node(xStart, yStart);
    node endNode = node(xEnd, yEnd);
    std::queue<node> nextMoves;
    nextMoves.push(startNode);
    int step = 0;

    std::vector<int> fibMemo(manhattan(startNode, endNode) + 1, -1);
    fibMemo[0] = 1;
    fibMemo[1] = 1;

    int Nobs = xObs.size();
    std::vector<node> obsVector(Nobs);

    for (int i = 0; i < Nobs; i++)
    {
        obsVector[i] = node(xObs[i], yObs[i]);
    }

    while (!nextMoves.empty())
    {
        node current = nextMoves.front();
        nextMoves.pop();
        int moveDistance = fibBottomUp(step, fibMemo);
        node m1 = node(std::get<0>(current) + moveDistance, std::get<1>(current));
        node m2 = node(std::get<0>(current), std::get<1>(current) + moveDistance);

        if (equals(m1, endNode))
        {
            return true;
        }
        if (equals(m2, endNode))
        {
            return true;
        }
        if (isValid(m1, endNode, obsVector))
        {
            nextMoves.push(m1);
        }
        if (isValid(m2, endNode, obsVector))
        {
            nextMoves.push(m2);
        }
    }

    return false;
}

int main()
{
    printf("%d\n", 2);
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}