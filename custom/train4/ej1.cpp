#include <bits/stdc++.h>

using namespace std;

void printVector(const std::vector<int> &v);

bool isPositiveInteger(std::string text)
{
    std::string::const_iterator it = text.begin();
    while (it != text.end() && std::isdigit(*it))
        ++it;
    return !text.empty() && it == text.end();
}

int isDni(std::string &text, int from)
{
    if (from == 0 || text[from - 1] == '\n' || text[from - 1] == ' ')
    {
        int counter = 0;
        while (std::isdigit(text[from + counter]))
        {
            counter++;
        }
        if (text[from + counter] == '\0' || text[from + counter] == '\n' || text[from + counter] == ' ')
        {
            return counter;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        return -1;
    }
}

std::string censor(std::string text)
{
    // Write your code here
    int pointer = 0;
    int N = text.size();
    std::string res = "";
    while (pointer < N)
    {
        int dniLenght = isDni(text, pointer);
        if (dniLenght < 6 || dniLenght > 8)
        {
            res.push_back(text[pointer]);
            pointer++;
        }
        else
        {
            while (dniLenght > 0)
            {
                res.push_back('X');
                pointer++;
                dniLenght--;
            }
        }
    }
    return res;
}

int main()
{
    std::string input = "The current headmaster's DNI is 11111111 and his phone number is 1111-1111\nThe school ID for the oldest student is 12345 whereas the youngest one is 7654321DZ, and the second youngest is /123456. The best student's school ID is 7834617";
    std::string out = censor(input);
    printf("%s\n", out.c_str());
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}