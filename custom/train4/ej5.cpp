#include <bits/stdc++.h>

using namespace std;

void printVector(const std::vector<int> &v);

int main()
{
    printf("%d\n", 2);
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}