#include <bits/stdc++.h>

#define BOARD_WIDTH 8
#define BOARD_HEIGHT 8

typedef std::pair<int, int> boardPos;

int howManyMoves(const boardPos &initPos, const boardPos &finalPos);
int howManyMovesImproved(boardPos &initPos, boardPos &finalPos);

std::vector<boardPos> getAllMoves(const boardPos &pos);
std::vector<boardPos> getAllValidMoves(const boardPos &pos, int maxX, int maxY);
std::vector<boardPos> getAllFreeMoves(const std::vector<std::vector<int>> distances, const boardPos &pos);
int manhattan(const boardPos &p1, const boardPos &p2);
void fixPositions(boardPos &initPos, boardPos &finalPos);
boardPos realPos2MatrixPos(const boardPos &initPos, const boardPos &currentPos);
std::vector<boardPos> transformAndFilterPositions(std::vector<boardPos> &positions, const boardPos &initPos, int xSize, int ySize);

void pushAllPositions(const std::vector<boardPos> &values, std::queue<boardPos> &queue);
void setDistances(const std::vector<boardPos> &newPositions, std::vector<std::vector<int>> &distances, const boardPos &currentPos);

bool operator==(const boardPos &p1, const boardPos &p2);