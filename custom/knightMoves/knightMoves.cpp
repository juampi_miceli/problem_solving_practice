#include <bits/stdc++.h>
#include "knightMoves.h"

/**
 * If we see the board as a graph, we can use BFS to know the distance from every tile in the board to the desired position.
 * This will start from the horse position and ind the k-th iteration we will have found every position that can be reached 
 * in k moves. The algorithm is stopped either if we have reached the objective or if we can't move anymore (there is no path).
 * In the worst case we need to visit every tile in the board one time, thus the time complexity is O(width*height)
 */
int howManyMoves(const boardPos &initPos, const boardPos &finalPos)
{
    std::vector<std::vector<int>> distances(BOARD_HEIGHT, std::vector<int>(BOARD_WIDTH, -1));
    distances[std::get<0>(initPos)][std::get<1>(initPos)] = 0;

    std::queue<boardPos> nextPositions;
    nextPositions.push(initPos);

    while (distances[std::get<0>(finalPos)][std::get<1>(finalPos)] == -1)
    {
        if (nextPositions.empty())
        {
            fprintf(stderr, "There is no path from (%d,%d) to (%d,%d).", std::get<0>(initPos), std::get<1>(initPos), std::get<0>(finalPos), std::get<1>(finalPos));
            exit(EXIT_FAILURE);
        }
        boardPos currentPosition = nextPositions.front();
        nextPositions.pop();
        std::vector<boardPos> freePositions = getAllFreeMoves(distances, currentPosition);
        setDistances(freePositions, distances, currentPosition);
        pushAllPositions(freePositions, nextPositions);
    }

    return distances[std::get<0>(finalPos)][std::get<1>(finalPos)];
}

/**
 * Supossing the board is infinite, the above algorithm would have infinite time complexity, this algorithm seeks to improve this.
 * We will again do BFS, but in this case we will only take into account those tiles that do not walk away from the finalPos for
 * more than 3 squares (in manhattan distance). This is because, for the way the knigt moves, sometimes we may want to walk away a
 * little to move to some position, but if we walk away more than one move we are not doing anything more than taking a non optimal
 * route. With this in consideration, we can shrink our time complexity to the square formed by using initPos and finalPos as corners.
 * With that we mean O(abs(initPosX - finalPosX)*abs(initPosY - finalPosY)). (This square has 3 tiles offset for each side, but that
 * does not change the complexity)
*/
int howManyMovesImproved(boardPos &initPos, boardPos &finalPos)
{
    //Forces initPos to be top left and finalPos to be bottom right
    fixPositions(initPos, finalPos);

    int xDelta = abs(std::get<1>(initPos) - std::get<1>(finalPos));
    int yDelta = abs(std::get<0>(initPos) - std::get<0>(finalPos));
    std::vector<std::vector<int>> distances(yDelta + 6, std::vector<int>(xDelta + 6, -1));
    boardPos matrixInit = realPos2MatrixPos(initPos, initPos);
    boardPos matrixFinal = realPos2MatrixPos(initPos, finalPos);
    distances[std::get<0>(matrixInit)][std::get<1>(matrixInit)] = 0;

    std::queue<boardPos> nextPositions;
    nextPositions.push(matrixInit);

    while (distances[std::get<0>(matrixFinal)][std::get<1>(matrixFinal)] == -1)
    {
        if (nextPositions.empty())
        {
            fprintf(stderr, "There is no path from (%d,%d) to (%d,%d).", std::get<0>(initPos), std::get<1>(initPos), std::get<0>(finalPos), std::get<1>(finalPos));
            exit(EXIT_FAILURE);
        }
        boardPos currentPosition = nextPositions.front();
        nextPositions.pop();
        std::vector<boardPos> freePositions = getAllFreeMoves(distances, currentPosition);
        //Transform freePositions to it's matrix positions, they are filter if out of bounds
        // freePositions = transformAndFilterPositions(freePositions, initPos, distances[0].size(), distances.size());
        setDistances(freePositions, distances, currentPosition);
        pushAllPositions(freePositions, nextPositions);
    }

    return distances[std::get<0>(matrixFinal)][std::get<1>(matrixFinal)];
}

int main()
{

    for (int i = 0; i < BOARD_HEIGHT; i++)
    {
        printf("[");
        for (int j = 0; j < BOARD_WIDTH; j++)
        {
            int current = howManyMoves(boardPos(i, j), boardPos(0, 0));
            printf("%d", current);
            if (j < BOARD_WIDTH - 1)
            {
                printf(",");
            }
            else
            {
                printf("]\n");
            }
        }
    }

    printf("//////////////////////////////\n");

    for (int i = 0; i < BOARD_HEIGHT; i++)
    {
        printf("[");
        for (int j = 0; j < BOARD_WIDTH; j++)
        {
            boardPos initPos(0,0);
            boardPos finalPos(i,j);
            int current = howManyMovesImproved(initPos, finalPos);
            printf("%d", current);
            if (j < BOARD_WIDTH - 1)
            {
                printf(",");
            }
            else
            {
                printf("]\n");
            }
        }
    }
    return 0;
}

std::vector<boardPos> transformAndFilterPositions(const std::vector<boardPos> &positions, const boardPos &initPos, int xSize, int ySize)
{
    std::vector<boardPos> transformedPositions;

    for (const boardPos pos : positions)
    {
        boardPos matrixPos = realPos2MatrixPos(initPos, pos);
        int x = std::get<1>(matrixPos);
        int y = std::get<0>(matrixPos);

        if (x < 0 || x >= xSize || y < 0 || y >= ySize)
        {
            continue;
        }
        transformedPositions.push_back(matrixPos);
    }

    return transformedPositions;
}

void fixPositions(boardPos &initPos, boardPos &finalPos)
{
    int minX = std::min(std::get<1>(initPos), std::get<1>(finalPos));
    int minY = std::min(std::get<0>(initPos), std::get<0>(finalPos));
    int maxX = std::max(std::get<1>(initPos), std::get<1>(finalPos));
    int maxY = std::max(std::get<0>(initPos), std::get<0>(finalPos));

    initPos.first = minY;
    initPos.second = minX;
    finalPos.first = maxY;
    finalPos.second = maxX;
}

boardPos realPos2MatrixPos(const boardPos &initPos, const boardPos &currentPos)
{
    return boardPos(std::get<0>(currentPos) - std::get<0>(initPos) + 3, std::get<1>(currentPos) - std::get<1>(initPos) + 3);
}

int manhattan(const boardPos &p1, const boardPos &p2)
{
    return abs(std::get<1>(p1) - std::get<1>(p2)) + abs(std::get<0>(p1) - std::get<0>(p2));
}

void setDistances(const std::vector<boardPos> &newPositions, std::vector<std::vector<int>> &distances, const boardPos &currentPos)
{
    int currentDistance = distances[std::get<0>(currentPos)][std::get<1>(currentPos)];
    for (const boardPos pos : newPositions)
    {
        distances[std::get<0>(pos)][std::get<1>(pos)] = currentDistance + 1;
    }
}

void pushAllPositions(const std::vector<boardPos> &values, std::queue<boardPos> &queue)
{
    for (const boardPos &elem : values)
    {
        queue.push(elem);
    }
}

bool operator==(const boardPos &p1, const boardPos &p2)
{
    return std::get<0>(p1) == std::get<0>(p2) && std::get<1>(p1) == std::get<1>(p2);
}

std::vector<boardPos> getAllMoves(const boardPos &pos)
{
    std::vector<boardPos> allMoves;

    allMoves.push_back(boardPos(std::get<0>(pos) + 1, std::get<1>(pos) + 2));
    allMoves.push_back(boardPos(std::get<0>(pos) + 1, std::get<1>(pos) - 2));
    allMoves.push_back(boardPos(std::get<0>(pos) - 1, std::get<1>(pos) + 2));
    allMoves.push_back(boardPos(std::get<0>(pos) - 1, std::get<1>(pos) - 2));
    allMoves.push_back(boardPos(std::get<0>(pos) + 2, std::get<1>(pos) + 1));
    allMoves.push_back(boardPos(std::get<0>(pos) + 2, std::get<1>(pos) - 1));
    allMoves.push_back(boardPos(std::get<0>(pos) - 2, std::get<1>(pos) + 1));
    allMoves.push_back(boardPos(std::get<0>(pos) - 2, std::get<1>(pos) - 1));

    return allMoves;
}

std::vector<boardPos> getAllValidMoves(const boardPos &pos, int maxX, int maxY)
{
    std::vector<boardPos> allMoves = getAllMoves(pos);

    std::vector<boardPos> allValidMoves;

    for (const boardPos &move : allMoves)
    {
        int yPos = std::get<0>(move);
        int xPos = std::get<1>(move);
        if (xPos < 0 || xPos >= maxX || yPos < 0 || yPos >= maxY)
        {
            continue;
        }

        allValidMoves.push_back(move);
    }
    return allValidMoves;
}

std::vector<boardPos> getAllFreeMoves(const std::vector<std::vector<int>> distances, const boardPos &pos)
{
    std::vector<boardPos> allMoves = getAllValidMoves(pos, distances[0].size(), distances.size());

    std::vector<boardPos> allFreeMoves;

    for (const boardPos &move : allMoves)
    {
        int yPos = std::get<0>(move);
        int xPos = std::get<1>(move);
        if (distances[yPos][xPos] != -1)
        {
            continue;
        }

        allFreeMoves.push_back(move);
    }
    return allFreeMoves;
}