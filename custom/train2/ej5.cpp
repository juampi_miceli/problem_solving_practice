#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

class Cache
{

public:
    Cache(int _k) : k(_k), mem(std::vector<int>(k, 0)), N(0){};
    void insert(int elem);
    void del(int elem);
    bool contains(int elem);
    void pop();

private:
    int k;
    std::vector<int> mem;
    int N;
};

bool Cache::contains(int elem)
{
    for (int e : mem)
    {
        if (e == elem)
        {
            return true;
        }
    }
    return false;
}

void Cache::del(int elem)
{
    bool found = false;
    for (int i = 0; i < N; i++)
    {
        if (found)
        {
            if (i < N - 1)
            {
                mem[i] = mem[i + 1];
            }
        }
        else
        {
            if (mem[i] == elem)
            {
                found = true;
                i--;
            }
        }
    }

    N--;
}
void Cache::insert(int elem)
{
    if (this->contains(elem))
    {
        this->del(elem);
    }
    else
    {
        if (N >= k)
        {
            this->del(mem[0]);
        }
    }

    mem[N] = elem;

    N++;
}

int main()
{
    Cache *c = new Cache(2);
    c->insert(1);
    c->insert(2);
    c->insert(1);
    c->insert(3);
    printf("%d\n", c->contains(1));
    printf("%d\n", c->contains(2));
    printf("%d\n", c->contains(3));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}