#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

int leftChild(int elem)
{
    return (2 * elem) + 1;
}

int rightChild(int elem)
{
    return (2 * elem) + 2;
}

int father(int elem)
{
    return (elem - 1) / 2;
}

void findAllowed(const std::vector<bool> &allowed, int step, std::vector<int> &res)
{
    int N = allowed.size();
    if (step >= N)
    {
        return;
    }
    if (allowed[step])
    {
        res.push_back(step + 1);
        return;
    }
    findAllowed(allowed, leftChild(step), res);
    findAllowed(allowed, rightChild(step), res);
}

int ej6(int height, int K, const std::vector<int> &players)
{
    std::vector<bool> allowed((int)(pow(2, height)) - 1, true);

    for (int player : players)
    {
        while (allowed[player])
        {
            allowed[player] = false;
            player = father(player);
        }
    }
    std::vector<int> res;
    findAllowed(allowed, 0, res);
    sort(res.begin(), res.end());
    printVector(res);
    return 0;
}

int main()
{
    ej6(3, 1, {3});
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}