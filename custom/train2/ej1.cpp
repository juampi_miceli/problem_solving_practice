#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

int pairs(const std::vector<int> &v, int K)
{
    int N = v.size();
    int res = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = i + 1; j < N; j++)
        {
            if (v[i] + v[j] < K)
            {
                res++;
            }
        }
    }

    return res;
}

int main()
{
    printf("%d\n", pairs({3, 7, 2, 8}, 10));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}