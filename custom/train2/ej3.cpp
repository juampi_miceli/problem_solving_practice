#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

std::string ej3(int N, std::vector<std::string> msgs)
{
    std::string first = "It Is Up To You#####";
    std::string res = "";

    for (int i = 0; i < N - 1; i++)
    {
        res += first + msgs[i] + "\n";
    }
    res += first + msgs[N - 1];
    return res;
}

int main()
{
    printf("%s", ej3(4, {"Hey", "How", "Wanna", "Lets"}).c_str());
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}