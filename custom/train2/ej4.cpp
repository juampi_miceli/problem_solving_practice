#include <bits/stdc++.h>

typedef std::pair<int, int> chunk;

void printVector(const std::vector<int> &v);

int findMin(const std::vector<int> &v)
{
    int winner = v[0];
    for (int elem : v)
    {
        winner = std::min(winner, elem);
    }

    return winner;
}

int findMax(const std::vector<int> &v)
{
    int winner = v[0];
    for (int elem : v)
    {
        winner = std::max(winner, elem);
    }

    return winner;
}

int ej4(const std::vector<int> &starting, const std::vector<int> &ending)
{
    int N = starting.size();
    std::vector<chunk> freeChunks = {chunk(findMin(starting), findMax(ending))};

    for (int i = 0; i < N; i++)
    {
        int M = freeChunks.size();
        for (int j = 0; j < M; j++)
        {
            if (starting[i] <= std::get<0>(freeChunks[j]) && ending[i] >= std::get<1>(freeChunks[j]))
            {
                //free chunk is contained
                freeChunks.erase(freeChunks.begin() + j);
            }
            else if (starting[i] > std::get<0>(freeChunks[j]) && ending[i] < std::get<1>(freeChunks[j]))
            {
                //meet is contained
                chunk prevChunk = freeChunks[j];
                freeChunks.erase(freeChunks.begin() + j);
                freeChunks.insert(freeChunks.begin() + j, chunk(std::get<0>(prevChunk), starting[i]));
                freeChunks.insert(freeChunks.begin() + j, chunk(ending[i], std::get<1>(prevChunk)));
                j++;
            }
            else if (starting[i] <= std::get<0>(freeChunks[j]) && ending[i] < std::get<1>(freeChunks[j]))
            {
                //left contained
                freeChunks[j].first = ending[i];
            }
            else if (starting[i] > std::get<0>(freeChunks[j]) && ending[i] >= std::get<1>(freeChunks[j]))
            {
                //right contained
                freeChunks[j].second = starting[i];
            }
            else
            {
                printf("WTF\n");
                exit(EXIT_FAILURE);
            }
        }
    }

    return freeChunks.size();
}

int main()
{
    printf("%d\n", ej4({1, 6, 2}, {3, 7, 9}));
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}