#include <bits/stdc++.h>

#define TOO_BIG 0
#define TOO_SMALL 1
#define EXACT_SIZE 2

void printVector(const std::vector<int> &v);

int tryKey(int key, int K, int *time)
{

    if (key < K)
    {
        *time += 30;
        return TOO_SMALL;
    }
    if (key > K)
    {
        *time += 30;
        return TOO_BIG;
    }
    return EXACT_SIZE;
}

void printResults(int time, int key)
{
    printf("%d\n%d\n", key, time);
}

int ej2(const std::vector<int> &v, int K)
{
    int N = v.size();
    int smallestSize = 0;
    int biggestSize = INT_MAX;

    int time = 0;

    for (int i = 0; i < N; i++)
    {
        if (i == 0 || (v[i] > smallestSize && v[i] < biggestSize))
        {
            time++;
            int guess = tryKey(v[i], K, &time);
            if (guess == EXACT_SIZE)
            {
                printResults(time - 1, v[i]);
                return 0;
            }
            else if (guess == TOO_BIG)
            {
                smallestSize = 0;
                biggestSize = v[i];
            }
            else
            {
                smallestSize = v[i];
                biggestSize = INT_MAX;
            }
        }
    }
    return 0;
}

int main()
{
    ej2({6, 1, 5}, 6);
    ej2({1, 6, 5}, 6);
    ej2({1, 5, 6}, 6);
    return 0;
}

void printVector(const std::vector<int> &v)
{
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++)
    {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}