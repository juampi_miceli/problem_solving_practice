#include "ventanaQueContiene.h"

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <random>
#include <bits/stdc++.h>
#include <limits.h>

//We want an interval, as small as posible, that includes at least one element of every word given. Starting with two.
//If we abstract from the problem, we can imagine the line of real numbers, and for every array representing a word we can draw an interval starting in the first element of the array and reaching up to the last element. All the elements in between are not necessary for this problem. What we want is to choose our own interval that will be the smallest "ventana que contiene"(lo 100to no iba a traducir eso).
//To build this interval we can see that if we reach the left most interval and the right most interval we will be reaching every interval(word) required.
//If we call the opening part of a tuple the left one and the closing part of it the right one, we can see that all we need to do is to reach the smallest closing part of all tuples and the greatest opening one.

//As all we want is to find a maximum and a minimum we can do this in a linear form in O(n) with n as number of integers in the arrays.

//We define "n" as the number of elements in the array of arrays, and "m" as the number of different words we want in the window. In other words, "m" is the total amount of sub-arrays in the input.

/**
 * The easiest solution is brute force. To solve this problem using this method, we will generate every single window in O(n^2)
 * and we will keep the smallest window that contains all the words we want, we can check this in O(n). 
 * All in all this solution is O(n^3) for the general case, and O(n^2) when we restrict the problem only for 2 words. 
*/

std::vector<int> ventanaBruteForce(const std::vector<std::vector<int>> &wordsPositionsInput)
{

    //We will copy all the elements to a single array for easier iterating
    std::vector<int> wordsPositions = mergeAllArrays(wordsPositionsInput);

    size_t N = wordsPositions.size();
    std::vector<int> winnerTuple(2);
    int smallestSize = INT_MAX;

    for (size_t i = 0; i < N - 1; i++)
    {
        for (size_t j = i + 1; j < N; j++)
        {
            int start = std::min(wordsPositions[i], wordsPositions[j]);
            int end = std::max(wordsPositions[i], wordsPositions[j]);
            int windowSize = end - start;

            if (windowSize < smallestSize && isValidWindow(start, end, wordsPositionsInput))
            {
                smallestSize = windowSize;
                winnerTuple[0] = start;
                winnerTuple[1] = end;
            }
        }
    }

    return winnerTuple;
}

/**
 * For solving the 2 word restriction of this problem, we could take advantage of the fact that the word positions are sorted
 * in ascending order. Then we have two sorted lists and we could create a valid window placing the start in one list and the
 * end in the other one. 
 * 
 * We can place this pointers at the start of each list, and we then know a window size which we can use as our best solution yet.
 * Furthermore, we know the only way to get a better solution is to advance the smallest pointer, because advancing the biggest
 * one will only make the window bigger.
 * 
 * We will reach the solution when we get to the end of one of the lists. That would mean the unfinished list has only numbers
 * that would increase the window size. Then, we will get the solution in O(n)
 */
std::vector<int> ventanaFor2(const std::vector<std::vector<int>> &wordsPositions)
{
    size_t p1 = 0;
    size_t p2 = 0;
    size_t N1 = wordsPositions[0].size();
    size_t N2 = wordsPositions[1].size();
    std::vector<int> res(2);
    int smallestSize = INT_MAX;

    while (p1 < N1 && p2 < N2)
    {
        int word1Position = wordsPositions[0][p1];
        int word2Position = wordsPositions[1][p2];

        int currentSize = abs(word2Position - word1Position);
        if (currentSize < smallestSize)
        {
            smallestSize = currentSize;
            res[0] = std::min(word1Position, word2Position);
            res[1] = std::max(word1Position, word2Position);
        }

        if (word1Position < word2Position)
        {
            //p1 should advance
            p1++;
        }
        else if (word1Position > word2Position)
        {
            //p2 should advance
            p2++;
        }
        else
        {
            //wordPos1 = wordPos2 should not happen!
            fprintf(stderr, "Invalid input");
            exit(EXIT_FAILURE);
        }
    }

    return res;
}

/**
 * We will have one pointer for each different word, every pointer start at the end of it's array.
 * If we take the smallest and the biggest value from all the words pointed, we will have a valid window
 * we can compare that window to the best one up to this moment. To get the next valid window we have to
 * move back the biggest element pointed and calculate again the smallest and the biggest values.
 * We can do this in O(lg(m)) if we store all the elements in a max heap. 
 * Then, the total complexity is O(n*lg(m))
*/

std::vector<int> ventanaForN(const std::vector<std::vector<int>> &wordsPositionsInput)
{

    //Same as the input but every position has which word does it belong
    std::vector<std::vector<std::pair<int, int>>> wordsPositions = addArrayInfo(wordsPositionsInput);

    std::vector<int> pointers(wordsPositionsInput.size(), 0);

    int smallestSize = INT_MAX;
    std::vector<int> winnerWindow(2);

    std::vector<std::pair<int, int>> currentWords;
    int currentMin = INT_MAX;
    int currentMax = 0;

    for (int i = 0; i < wordsPositions.size(); i++)
    {
        pointers[i] = wordsPositions[i].size() - 1;

        currentWords.push_back(wordsPositions[i][wordsPositions[i].size() - 1]);
        currentMin = std::min(currentMin, std::get<0>(wordsPositions[i][wordsPositions[i].size() - 1]));
    }

    while (true)
    {
        std::make_heap(currentWords.begin(), currentWords.end());
        std::pair<int, int> maxPair = currentWords.front();
        std::pop_heap(currentWords.begin(), currentWords.end());
        currentWords.pop_back();
        int currentMax = std::get<0>(maxPair);
        int arrayOfMin = std::get<1>(maxPair);

        int windowSize = currentMax - currentMin;

        if (windowSize < smallestSize)
        {
            smallestSize = windowSize;
            winnerWindow = {currentMin, currentMax};
        }

        if (--pointers[arrayOfMin] < 0)
        {
            break;
        }

        std::pair<int, int> newElem = wordsPositions[arrayOfMin][pointers[arrayOfMin]];
        std::push_heap(currentWords.begin(), currentWords.end());
        currentWords.push_back(newElem);

        currentMin = std::min(currentMin, std::get<0>(newElem));
    }

    return winnerWindow;
}

std::vector<std::vector<std::pair<int, int>>> addArrayInfo(const std::vector<std::vector<int>> &wordsPositionsInput)
{
    std::vector<std::vector<std::pair<int, int>>> result;
    for (int i = 0; i < wordsPositionsInput.size(); i++)
    {
        std::vector<std::pair<int, int>> wordPositionsPair;
        for (int j = 0; j < wordsPositionsInput[i].size(); j++)
        {
            wordPositionsPair.push_back(std::make_pair(wordsPositionsInput[i][j], i));
        }
        result.push_back(wordPositionsPair);
    }

    return result;
}

/**
 * Find max will look for the biggest value in the words array, looking in the position associated with
 * vector "pointers". It returns the value and the array where it was found.
 */
std::vector<int> findMax(const std::vector<std::vector<int>> &wordsPositionsInput, const std::vector<int> &pointers)
{
    std::vector<int> result(2);
    result[0] = 0;
    for (int i = 0; i < wordsPositionsInput.size(); i++)
    {
        std::vector<int> wordPositions = wordsPositionsInput[i];
        if (wordPositions[pointers[i]] > result[0])
        {
            result[0] = wordPositions[pointers[i]];
            result[1] = i;
        }
    }

    return result;
}

/**
 * Find min will look for the smallest value in the words array, looking in the position associated with
 * vector "pointers". It returns the value and the array where it was found.
 */
std::vector<int> findMin(const std::vector<std::vector<int>> &wordsPositionsInput, const std::vector<int> &pointers)
{
    std::vector<int> result(2);
    result[0] = INT_MAX;
    for (int i = 0; i < wordsPositionsInput.size(); i++)
    {
        std::vector<int> wordPositions = wordsPositionsInput[i];
        if (wordPositions[pointers[i]] < result[0])
        {
            result[0] = wordPositions[pointers[i]];
            result[1] = i;
        }
    }

    return result;
}

int main()
{
    //Example
    std::vector<std::vector<int>> exampleTest = {{0, 4}, {7, 10, 11}};
    std::vector<int> expectedExample = ventanaBruteForce(exampleTest);
    std::vector<int> actualExample = ventanaFor2(exampleTest);

    if (!vectorEquals(expectedExample, actualExample))
    {
        fprintf(stderr, "Example test failed. Expected:(%d,%d), Actual(%d,%d)\n",
                expectedExample[0], expectedExample[1], actualExample[0], actualExample[1]);
    }

    //Random inputs N = 2
    for (int i = 0; i < 100; i++)
    {

        std::vector<std::vector<int>> randomInput = generateRandomInput();

        std::vector<int> expectedResult = ventanaBruteForce(randomInput);
        std::vector<int> actualResult = ventanaFor2(randomInput);

        if (!vectorEquals(expectedResult, actualResult))
        {
            fprintf(stderr, "Failed at iteration %d. Expected:(%d,%d), Actual(%d,%d)\n",
                    i, expectedResult[0], expectedResult[1], actualResult[0], actualResult[1]);
            exit(EXIT_FAILURE);
        }
    }

    std::vector<std::vector<int>> manualInput = {{1, 13, 20, 40}, {5, 12, 25}, {7, 41}, {8, 23, 32}};
    std::vector<int> manualRes = ventanaForN(manualInput);

    //Random inputs N > 2
    for (int i = 0; i < 100; i++)
    {

        std::vector<std::vector<int>> randomInput = generateRandomInputGeneral(5);

        std::vector<int> expectedResult = ventanaBruteForce(randomInput);
        std::vector<int> actualResult = ventanaForN(randomInput);

        if (!vectorEquals(expectedResult, actualResult))
        {
            fprintf(stderr, "Failed at iteration %d. Expected:(%d,%d), Actual(%d,%d)\n",
                    i, expectedResult[0], expectedResult[1], actualResult[0], actualResult[1]);
            exit(EXIT_FAILURE);
        }
    }

    printf("============\n");
    printf("=  PASSED  =\n");
    printf("============\n");

    return 0;
}

std::vector<int> mergeAllArrays(const std::vector<std::vector<int>> &arrayOfArrays)
{
    std::vector<int> resultingArray;

    for (const std::vector<int> &array : arrayOfArrays)
    {
        for (int elem : array)
        {
            resultingArray.push_back(elem);
        }
    }

    return resultingArray;
}

/**
 * We need to reach for at least one repetition of the word so we can check if any element of every word is contained in
 * the resulting tuple.
*/
bool isValidWindow(int start, int end, const std::vector<std::vector<int>> &wordsPositionsInput)
{

    for (const std::vector<int> &wordPositions : wordsPositionsInput)
    {
        bool contained = false;

        for (int position : wordPositions)
        {
            if (start <= position && position <= end)
            {
                contained = true;
                break;
            }
        }

        if (!contained)
        {
            return false;
        }
    }
    return true;
}

bool vectorEquals(const std::vector<int> &v1, const std::vector<int> &v2)
{
    if (v1.size() != v2.size())
    {
        return false;
    }

    for (size_t i = 0; i < v1.size(); i++)
    {
        if (v1[i] != v2[i])
        {
            return false;
        }
    }
    return true;
}

std::vector<std::vector<int>> generateRandomInput()
{
    //Create random array with elements between 0 and 19999.
    int N = 20000;
    std::vector<int> arange(N);
    for (int j = 0; j < N; j++)
    {
        arange[j] = j;
    }
    auto rng = std::default_random_engine{};
    shuffle(arange.begin(), arange.end(), rng);

    std::vector<std::vector<int>> randomInput(2, std::vector<int>(0));
    //Insert 50 numbers in each list. To ensure they do not repeat we take them out from the arange
    for (int j = 0; j < 100; j++)
    {
        int randomNumber = arange[arange.size() - 1];
        randomInput[j % 2].push_back(randomNumber);
        arange.pop_back();
    }
    std::sort(randomInput[0].begin(), randomInput[0].end());
    std::sort(randomInput[1].begin(), randomInput[1].end());

    return randomInput;
}

std::vector<std::vector<int>> generateRandomInputGeneral(int m)
{
    //Create random array with elements between 0 and 49999.
    int N = 50000;
    std::vector<int> arange(N);
    for (int j = 0; j < N; j++)
    {
        arange[j] = j;
    }
    auto rng = std::default_random_engine{};
    shuffle(arange.begin(), arange.end(), rng);

    std::vector<std::vector<int>> randomInput(m, std::vector<int>(0));
    //Insert 500/m numbers in each list. To ensure they do not repeat we take them out from the arange
    for (int j = 0; j < 500; j++)
    {
        int randomNumber = arange[arange.size() - 1];
        randomInput[j % m].push_back(randomNumber);
        arange.pop_back();
    }
    for (int j = 0; j < m; j++)
    {
        std::sort(randomInput[j].begin(), randomInput[j].end());
    }

    return randomInput;
}