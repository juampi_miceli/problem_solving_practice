#include <stdlib.h>
#include <stdio.h>
#include <vector>

//Brute force
std::vector<int> mergeAllArrays(const std::vector<std::vector<int>> &arrayOfArrays);
bool isValidWindow(int start, int end, const std::vector<std::vector<int>> &wordsPositionsInput);
std::vector<int> ventanaBruteForce(const std::vector<std::vector<int>> &wordsPositionsInput);

//N=2
std::vector<int> ventanaFor2(const std::vector<std::vector<int>> &wordsPositions);

//N>2
std::vector<int> ventanaForN(const std::vector<std::vector<int>> &wordsPositionsInput);
std::vector<int> findMax(const std::vector<std::vector<int>> &wordsPositionsInput, const std::vector<int> &pointers);
std::vector<int> findMin(const std::vector<std::vector<int>> &wordsPositionsInput, const std::vector<int> &pointers);
std::vector<std::vector<std::pair<int, int>>> addArrayInfo(const std::vector<std::vector<int>> &wordsPositionsInput);
//Testing
bool vectorEquals(const std::vector<int> &v1, const std::vector<int> &v2);
std::vector<std::vector<int>> generateRandomInput();
std::vector<std::vector<int>> generateRandomInputGeneral(int m);