#include <bits/stdc++.h>
#include "maxTorusSum.h"

/**
 * To get the maximum sum of the torus, we could first calculate all of the possible sums the torus has.
 * It other words, grab every sub-matrix in the torus, and get that sub-matrix sum. If we call N, the total
 * elements in the torus, we would have N^2 possible sub-matrices. If then we want to calculate the sum of 
 * each sub-matrix, we would have in the worst case O(N) time. In the end, this algorithm takes O(N^3).
 * 
 * To do this we will have to points to define the sub-matrix. A topLeft position and a bottomRight position,
 * as its names indicates this will be the top left and bottom right corners of the sub-matrix. If the top
 * left corner is further to the left and up than the right bottom corner, the we are in front of a non wrapping
 * matrix and we can calculate the sum as the sum of every element in between those points. If this does
 * not happen and we have a wrapped matrix, we must calculate the sum of all its sub matrices, we can see 
 * that visually in the drawings below, there we can found every possible wrapping. 
 * (creo que en una entrevista haria estos dibujos rapidito en paint) 
 * 
 * Horizontal wrapping
 *  ______________
 * |____      ___|
 * |mat|     |mat|
 * |_1_|     |_2_|
 * |_____________|
 * 
 * Vertical wrapping
 *  ______________
 * |  |      |   |
 * |  | mat1 |   |
 * |  |______|   |
 * |   _______   |
 * |  | mat2 |   |
 * |__|______|___|
 * 
 * Horizontal and vertical wrapping
 *  _________________
 * | mat1 |  | mat2 |
 * |______|  |______|
 * |                |
 * |_______   ______|
 * | mat3 |  | mat4 |
 * |______|__|______|
 * 
 * 
*/
int maxTorusSum(const std::vector<std::vector<int>> &matrix)
{
    if (matrix.empty() || matrix[0].empty())
    {
        return 0;
    }

    int xSize = matrix[0].size();
    int ySize = matrix.size();

    int winner = INT_MIN;

    boardPos topLeft;
    boardPos bottomRight;

    for (int i = 0; i < xSize; i++)
    {
        for (int j = 0; j < ySize; j++)
        {
            topLeft.first = j;
            topLeft.second = i;

            for (int x = 0; x < xSize; x++)
            {
                for (int y = 0; y < ySize; y++)
                {
                    bottomRight.first = y;
                    bottomRight.second = x;

                    winner = std::max(winner, getSum(matrix, topLeft, bottomRight));
                }
            }
        }
    }

    return winner;
}

/**
 * We can improve the performance of the previous algorithm by not doing innecesary sub-matrix sums each iteration. This 
 * is made by using a dynamic programming algorithm.
 * We could calculate every submatrix in a way that we start with the smaller matrices, and then to calculate a big matrix 
 * we could use the value of two smaller matrices already calculated and stored in a memoization structure. In that way
 * each matrix sum would be in O(1). We will divide this sub-matrices in two groups, the ones that are made without wrapping
 * and the ones wrapped. Let's start with the non wrapping ones: 
 * To calculate the sum of a non wrapping matrix we will do the following:
 *  _____________     ______________
 * |            |    |             |
 * | SUB MATRIX |    |   TOP LEFT  |
 * |            |  = |_____________|
 * |____________|    |__LEFT___|VAL|
 * 
 * The sum of the total sub-matrix is equal to the sum of the "top left part" + the "left part" + the bottom right cell. In 
 * that way we could find an order of calculation that allows us to always have already calculated "top left" and "left" 
 * when we want to calculate the sub-matrix sum. 
 * Moving on to the wrapped ones: Those matrices will look something like this:
 * 
 *  ______________
 * |____      ___|
 * |mat|     |mat|
 * |_1_|     |_2_|
 * |_____________|
 * 
 *  ______________
 * |  |      |   |
 * |  | mat1 |   |
 * |  |______|   |
 * |   _______   |
 * |  | mat2 |   |
 * |__|______|___|
 * 
 * 
 *  _________________
 * | mat1 |  | mat2 |
 * |______|  |______|
 * |                |
 * |_______   ______|
 * | mat3 |  | mat4 |
 * |______|__|______|
 * 
 * If we calculate this at the end of the algorithm we will already have those sub-matrices calculated, thus we could sum
 * those two values in O(1).
 * 
 * To store this sums we will use a 4 dimension vector (j,i,y,x) that will have the sum of the sub-matrix formed by the 
 * corners (i, j), (x, y). This will allow us to have an organiced way of accessing each sum in O(1).
 * 
 * All in all, the total time complexity is O(N^2), as we need to try every possible sub-matrix in the original matrix,
 * and we already said there are N^2 of them, but in this ocassion the process of getting its sum is O(1).
 * 
 * We talked about a calculation order but we never specify which order we will use. We are going to calculate first
 * every possible matrix of height 1, in increasing width from left to right, that would allow us to always know the
 * value of the left matrix. Then we will increase the height one tile at the time, and repeat the process of increasing
 * the width, in that way we are sure we have already calculated both the left matrix and the top left one.
 * When we finish that calculation we can move on to the wrapped matrices, in that point we already have every non wrapping 
 * matrix so we will have no problem.    
*/

int maxTorusSumImproved(const std::vector<std::vector<int>> &matrix)
{
    if (matrix.empty() || matrix[0].empty())
    {
        return 0;
    }

    int xSize = matrix[0].size();
    int ySize = matrix.size();
    std::vector<std::vector<std::vector<std::vector<int>>>> memo(ySize, std::vector<std::vector<std::vector<int>>>(xSize, std::vector<std::vector<int>>(ySize, std::vector<int>(xSize, INT_MIN))));

    int winner = INT_MIN;

    //Non torus sums
    for (int k = 0; k < ySize; k++)
    {
        for (int i = 0; i < xSize; i++)
        {
            for (int j = k; j < ySize; j++)
            {
                for (int x = i; x < xSize; x++)
                {
                    int topLeft = 0;
                    int left = 0;
                    if (x > i)
                    {
                        left = memo[j][i][j][x - 1];
                    }

                    if (k > 0)
                    {
                        topLeft = memo[j - k][i][j - 1][x];
                    }
                    int val = left + topLeft + matrix[j][x];
                    memo[j - k][i][j][x] = val;
                    winner = std::max(winner, memo[j - k][i][j][x]);
                }
            }
        }
    }

    //all torus sums
    for (int leftX = 0; leftX < xSize; leftX++)
    {
        for (int leftY = 0; leftY < ySize; leftY++)
        {
            for (int rightX = 0; rightX < xSize; rightX++)
            {
                for (int rightY = 0; rightY < ySize; rightY++)
                {
                    //mixed wrapping
                    if (leftX > rightX && leftY > rightY)
                    {
                        int topLeft = memo[0][0][rightY][rightX];
                        int topRight = memo[0][leftX][rightY][xSize - 1];
                        int bottomLeft = memo[leftY][0][ySize - 1][rightX];
                        int bottomRight = memo[leftY][leftX][ySize - 1][xSize - 1];

                        memo[leftY][leftX][rightY][rightX] = topLeft + topRight + bottomLeft + bottomRight;
                        winner = std::max(winner, memo[leftY][leftX][rightY][rightX]);
                    }
                    else if (leftY > rightY)
                    { //only vertical
                        int top = memo[0][leftX][rightY][rightX];
                        int bottom = memo[leftY][leftX][ySize - 1][rightX];

                        memo[leftY][leftX][rightY][rightX] = top + bottom;
                        winner = std::max(winner, memo[leftY][leftX][rightY][rightX]);
                    }
                    else if (leftX > rightX)
                    { //Only horizontal
                        int left = memo[leftY][leftX][rightY][xSize - 1];
                        int right = memo[leftY][0][rightY][rightX];

                        memo[leftY][leftX][rightY][rightX] = left + right;
                        winner = std::max(winner, memo[leftY][leftX][rightY][rightX]);
                    }
                }
            }
        }
    }

    return winner;
}

int main()
{
    test(5000);
    // int testCases;
    // scanf("%d", &testCases);
    // for (int i = 0; i < testCases; i++)
    // {
    //     int matSize;
    //     scanf("%d", &matSize);

    //     std::vector<std::vector<int>> mat(matSize, std::vector<int>(matSize, 0));
    //     for (int y = 0; y < matSize; y++)
    //     {
    //         for (int x = 0; x < matSize; x++)
    //         {
    //             int val;
    //             scanf("%d", &val);
    //             mat[y][x] = val;
    //         }
    //     }
    //     printf("%d\n", maxTorusSumImproved(mat));
    // }
    return 0;
}

/**
 * For testing this solution we will make 500 random matrices of different widths and heights and will compare the brute force
 * solution and the DP solution, if all the results are equal we will say the algorithms work. An important part of this check
 * is that we already know this algorithm works with the test cases given by UVA, so we can have at least 2 cases in which our
 * algorithms give the correct value.
*/
void test(int N)
{
    for (int i = 0; i < N; i++)
    {
        std::vector<std::vector<int>> randMatrix = getRandomMatrix(getRandom(1, 15), getRandom(1, 15));
        int expected = maxTorusSum(randMatrix);
        int actual = maxTorusSumImproved(randMatrix);
        if (expected != actual)
        {
            fprintf(stderr, "i: %d, expected: %d, actual: %d\n", i, expected, actual);
            for (int i = 0; i < randMatrix.size(); i++)
            {
                for (int j = 0; j < randMatrix[0].size(); j++)
                {
                    printf("%d, ", randMatrix[i][j]);
                }
                printf("\n");
            }
            exit(EXIT_FAILURE);
        }
    }
    printf("Tests passed!!!\n");
}

int getRandom(int low, int high)
{
    int delta = high - low;
    return (rand() % (delta + 1)) + low;
}

std::vector<std::vector<int>> getRandomMatrix(int width, int height)
{
    std::vector<std::vector<int>> matrix(height, std::vector<int>(width, 0));

    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            matrix[j][i] = getRandom(-100, 100);
        }
    }

    return matrix;
}

int getSum(std::vector<std::vector<int>> matrix, const boardPos &topLeft, const boardPos &bottomRight)
{
    int xLeft = std::get<1>(topLeft);
    int yLeft = std::get<0>(topLeft);
    int xRight = std::get<1>(bottomRight);
    int yRight = std::get<0>(bottomRight);

    int xSize = matrix[0].size();
    int ySize = matrix.size();

    if (xLeft < 0 || xLeft >= xSize || xRight < 0 || xRight >= xSize || yLeft < 0 || yLeft >= ySize || yRight < 0 || yRight >= ySize)
    {
        fprintf(stderr, "topLeft or bottomRight were out of bounds topLeft: (%d,%d), bottomRight: (%d,%d)", xLeft, yLeft, xRight, yRight);
        exit(EXIT_FAILURE);
    }

    int sum = 0;

    if (xLeft <= xRight && yLeft <= yRight)
    {
        //No wrapping
        for (int x = xLeft; x <= xRight; x++)
        {
            for (int y = yLeft; y <= yRight; y++)
            {
                sum += matrix[y][x];
            }
        }
        return sum;
    }

    //Double wrapping
    if (yLeft > yRight && xLeft > xRight)
    {
        //Top left
        for (int x = 0; x <= xRight; x++)
        {
            for (int y = 0; y <= yRight; y++)
            {
                sum += matrix[y][x];
            }
        }

        //Top right
        for (int x = xLeft; x < xSize; x++)
        {
            for (int y = 0; y <= yRight; y++)
            {
                sum += matrix[y][x];
            }
        }

        //Bottom left
        for (int x = 0; x <= xRight; x++)
        {
            for (int y = yLeft; y < ySize; y++)
            {
                sum += matrix[y][x];
            }
        }

        //Bottom right
        for (int x = xLeft; x < xSize; x++)
        {
            for (int y = yLeft; y < ySize; y++)
            {
                sum += matrix[y][x];
            }
        }

        return sum;
    }

    //Horizontal wrapping
    if (xLeft > xRight)
    {
        //Left side sum
        for (int x = 0; x <= xRight; x++)
        {
            for (int y = yLeft; y <= yRight; y++)
            {
                sum += matrix[y][x];
            }
        }

        //Right side sum
        for (int x = xLeft; x < xSize; x++)
        {
            for (int y = yLeft; y <= yRight; y++)
            {
                sum += matrix[y][x];
            }
        }

        return sum;
    }

    //Vertical wrapping
    //top sum
    for (int x = xLeft; x <= xRight; x++)
    {
        for (int y = 0; y <= yRight; y++)
        {
            sum += matrix[y][x];
        }
    }

    //bottom sum
    for (int x = xLeft; x <= xRight; x++)
    {
        for (int y = yLeft; y < ySize; y++)
        {
            sum += matrix[y][x];
        }
    }

    return sum;
}