#include <bits/stdc++.h>

typedef std::pair<int, int> boardPos;

int maxTorusSum(const std::vector<std::vector<int>> &matrix);
int maxTorusSumImproved(const std::vector<std::vector<int>> &matrix);

int getSum(std::vector<std::vector<int>> matrix, const boardPos &topLeft, const boardPos &bottomRight);

std::vector<std::vector<int>> getRandomMatrix(int width, int height);
int getRandom(int low, int high);
void test(int N);