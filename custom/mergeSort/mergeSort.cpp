#include "mergeSort.h"

std::vector<int> merge(const std::vector<int> &v1, const std::vector<int> &v2)
{
    int p1 = 0;
    int p2 = 0;
    int N1 = v1.size();
    int N2 = v2.size();

    std::vector<int> res;

    while (p1 < N1 && p2 < N2)
    {
        if (v1[p1] <= v2[p2])
        {
            res.push_back(v1[p1++]);
        }
        else
        {
            res.push_back(v2[p2++]);
        }
    }

    while (p1 < N1)
    {
        res.push_back(v1[p1++]);
    }

    while (p2 < N2)
    {
        res.push_back(v2[p2++]);
    }

    return res;
}

std::vector<int> mergeSort(const std::vector<int> &v)
{
    if (v.size() <= 1)
    {
        return v;
    }

    std::vector<int> sortedLeft = mergeSort(std::vector<int>(v.begin(), v.begin() + v.size() / 2));
    std::vector<int> sortedRight = mergeSort(std::vector<int>(v.begin() + v.size() / 2, v.end()));

    return merge(sortedLeft, sortedRight);
}

int main()
{
    test(1000);
    return 0;
}

void test(int N)
{
    int low = -50;
    int high = 50;
    for (int i = 0; i < N; i++)
    {
        std::vector<int> randVector = getRandomVector(getRandom(0, 50), low, high);
        randVector = mergeSort(randVector);
        for (int i = 0; i < (int)randVector.size() - 1; i++)
        {
            if (randVector[i] > randVector[i + 1])
            {
                fprintf(stderr, "Wrongly sorted ====\ti: %d\n", i);
                for (int i = 0; i < randVector.size(); i++)
                {
                    printf("%d, ", randVector[i]);
                }
                printf("\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    printf("Tests passed!!!\n");
}

std::vector<int> getRandomVector(int N, int low, int high)
{
    std::vector<int> resVector;
    for (int i = 0; i < N; i++)
    {
        resVector.push_back(getRandom(low, high));
    }
    return resVector;
}

int getRandom(int low, int high)
{
    int delta = high - low;
    return (rand() % (delta + 1)) + low;
}
