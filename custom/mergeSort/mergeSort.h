#include <bits/stdc++.h>

std::vector<int> merge(const std::vector<int> &v1, const std::vector<int> &v2);
std::vector<int> mergeSort(const std::vector<int> &v);

void test(int N);
std::vector<int> getRandomVector(int N, int low, int high);
int getRandom(int low, int high);
