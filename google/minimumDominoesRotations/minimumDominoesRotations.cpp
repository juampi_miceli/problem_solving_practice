#include <bits/stdc++.h>

int minimumDominoesRotations(std::vector<int> &tops, std::vector<int> &bottoms) {
    size_t N = tops.size();

    std::vector<int> rowValuesCount = {0, 0, 0, 0, 0, 0};
    std::vector<int> topValuesCount = {0, 0, 0, 0, 0, 0};
    std::vector<int> bottomValuesCount = {0, 0, 0, 0, 0, 0};
    size_t M = topValuesCount.size();

    for (size_t i = 0; i < N; i++) {
        topValuesCount[tops[i] - 1]++;
        bottomValuesCount[bottoms[i] - 1]++;

        rowValuesCount[bottoms[i] - 1]++;
        if (tops[i] != bottoms[i]) {
            rowValuesCount[tops[i] - 1]++;
        }
    }

    for (size_t i = 0; i < M; i++) {
        if (rowValuesCount[i] >= N) {
            return std::min(topValuesCount[i], bottomValuesCount[i]) - (topValuesCount[i] + bottomValuesCount[i] - N);
        }
    }

    return -1;
}

int main() {
    return 0;
}