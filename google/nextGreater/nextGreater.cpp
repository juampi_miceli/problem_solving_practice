#include <bits/stdc++.h>

std::vector<int> nextGreaterElement(const std::vector<int>& nums1, const std::vector<int>& nums2) {
    size_t M = nums1.size();
    size_t N = nums2.size();
    std::unordered_map<int, size_t> valueIndex;

    for (size_t i = 0; i < N; i++) {
        valueIndex[nums2[i]] = i;
    }

    std::vector<int> nextGreater(N, -1);
    std::stack<int> decreasing;
    decreasing.push(nums2[0]);
    for (size_t i = 1; i < N; i++) {
        int elem = nums2[i];
        while (!decreasing.empty() && decreasing.top() < elem) {
            int removed = decreasing.top();
            decreasing.pop();
            nextGreater[valueIndex[removed]] = elem;
        }
        decreasing.push(elem);
    }

    std::vector<int> res(M);

    for (size_t i = 0; i < M; i++) {
        res[i] = nextGreater[valueIndex[nums1[i]]];
    }

    return res;
}

int main() {
    nextGreaterElement({4, 1, 2}, {1, 3, 4, 2});
    return 0;
}