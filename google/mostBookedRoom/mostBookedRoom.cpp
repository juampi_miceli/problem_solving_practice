#include <bits/stdc++.h>

std::string mostBookedRoom(const std::vector<std::string> &bookingHistory) {
    std::unordered_map<std::string, size_t> bookingCounter;

    std::string winnerRoom;
    int winner = -1;

    for (const std::string &booking : bookingHistory) {
        if (booking[0] == '-') {
            continue;
        }
        std::string room(booking.begin() + 1, booking.end());

        int current;

        if (bookingCounter.count(room)) {
            bookingCounter[room]++;
        } else {
            bookingCounter[room] = 1;
        }

        current = bookingCounter[room];

        if (current > winner) {
            winner = current;
            winnerRoom = room;
        }
    }
    return winnerRoom;
}

int main() {
    printf("%s\n", mostBookedRoom({"+1A", "+3E", "-1A", "+4F", "+1A", "-3E", "+3E", "-3E", "+3E"}).c_str());
    return 0;
}