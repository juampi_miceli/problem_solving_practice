#include <bits/stdc++.h>

int waysTosplitString(const std::string &input) {
    size_t N = input.size();
    std::unordered_map<char, int> leftUnique;
    std::unordered_map<char, int> rightUnique;

    //First fill
    for (char c : input) {
        if (rightUnique.count(c)) {
            rightUnique[c]++;
        } else {
            rightUnique[c] = 1;
        }
    }

    int ways = 0;
    size_t leftSize = leftUnique.size();
    size_t rightSize = rightUnique.size();
    for (size_t i = 0; i < N; i++) {
        if (leftSize > rightSize) {
            break;
        }

        if (leftSize == rightSize) {
            ways++;
        }

        char currentChar = input[i];

        if (leftUnique.count(currentChar)) {
            leftUnique[currentChar]++;
        } else {
            leftUnique[currentChar] = 1;
        }

        if (rightUnique[currentChar] == 1) {
            rightUnique.erase(currentChar);
        } else {
            rightUnique[currentChar]--;
        }

        leftSize = leftUnique.size();
        rightSize = rightUnique.size();
    }
    printf("input: %s\t ways: %d\n", input.c_str(), ways);
    return ways;
}

int main() {
    std::string input1 = "aaaa";
    std::string input2 = "bac";
    std::string input3 = "ababa";
    waysTosplitString(input1);
    waysTosplitString(input2);
    waysTosplitString(input3);
    return 0;
}