#include <bits/stdc++.h>

std::string maximumTime(const std::string &input) {
    std::string res = input;
    res[4] = res[4] == '?' ? '9' : res[4];
    res[3] = res[3] == '?' ? '5' : res[3];

    if (res[0] == '?' && res[1] == '?') {
        res[0] = '2';
        res[1] = '3';
    } else if (res[0] == '?') {
        res[0] = res[1] < '4' ? '2' : '1';
    } else if (res[1] == '?') {
        res[1] = res[0] == '2' ? '3' : '9';
    }

    printf("input: %s\t res: %s\n", input.c_str(), res.c_str());
    return res;
}

int main() {
    maximumTime("23:5?");  // 23:59
    maximumTime("2?:22");  // 23:22
    maximumTime("0?:??");  // 09:59
    maximumTime("1?:??");  // 19:59
    maximumTime("?4:??");  // 14:59
    maximumTime("?3:??");  // 23:59
    maximumTime("??:??");  // 23:59
    maximumTime("?4:5?");  //14:59
    maximumTime("?4:??");  //14:59
    maximumTime("?3:??");  //23:59
    maximumTime("23:5?");  //23:59
    maximumTime("2?:22");  //23:22
    maximumTime("0?:??");  //09:59
    maximumTime("1?:??");  //19:59
    maximumTime("?4:0?");  //14:09
    maximumTime("?9:4?");  //19:49
    return 0;
}