#include <bits/stdc++.h>

template <class T>
void printVector(const std::vector<T> &v);

typedef std::pair<int, int> letterAppearances_t;

std::string solution(std::string input) {
    size_t N = input.size();
    size_t M = 'z' - 'a' + 1;
    std::vector<letterAppearances_t> letters(M, letterAppearances_t(-1, -1));

    size_t pLeft = 0;
    size_t pRight = N - 1;

    for (size_t i = 0; i < N; i++) {
        size_t leftLetterIndex = (size_t)(input[pLeft]) - 'a';
        size_t rightLetterIndex = (size_t)(input[pRight]) - 'a';
        if (letters.at(leftLetterIndex).first == -1) {
            letters.at(leftLetterIndex).first = pLeft;
        }
        if (letters.at(rightLetterIndex).second == -1) {
            letters.at(rightLetterIndex).second = pRight;
        }
        pLeft++;
        pRight--;
    }

    int winnerIndex = -1;
    int winnerDelta = -1;

    for (size_t i = 0; i < N; i++) {
        size_t letterIndex = (size_t)(input[i]) - 'a';
        if (letters.at(letterIndex).second == -1 || letters.at(letterIndex).first == -1) {
            continue;
        }
        int currentDelta = letters.at(letterIndex).second - letters.at(letterIndex).first;
        if (currentDelta > winnerDelta) {
            winnerDelta = currentDelta;
            winnerIndex = letterIndex;
        }
    }
    int resStart = letters[winnerIndex].first;
    int resEnd = letters[winnerIndex].second + 1;
    return std::string(input.begin() + resStart, input.begin() + resEnd);
}

int main() {
    std::string input1 = "cbaabaab";
    std::string input2 = "performance";
    printf("i: %s, o: %s\n", input1.c_str(), solution(input1).c_str());
    printf("i: %s, o: %s\n", input2.c_str(), solution(input2).c_str());
    return 0;
}

template <class T>
void printVector(const std::vector<T> &v) {
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++) {
        std::cout << v[i] << ", ";
    }
    std::cout << "]";
}
