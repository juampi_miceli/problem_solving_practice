#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);

bool dpSubsetSum(std::vector<std::vector<int>> &memo, const std::vector<int> &input, int k, size_t step) {
    size_t N = input.size();
    if (k == 0) {
        return 1;
    }

    if (k < 0 || step >= N) {
        return 0;
    }

    if (memo[k][step] == -1) {
        memo[k][step] = dpSubsetSum(memo, input, k, step + 1) == 1 || dpSubsetSum(memo, input, k - input[step], step + 1) == 1 ? 1 : 0;
    }

    return memo[k][step];
}

bool subsetSum(const std::vector<int> &input, int k) {
    size_t N = input.size();
    std::vector<std::vector<int>> memo(k + 1, std::vector<int>(N, -1));

    return dpSubsetSum(memo, input, k, 0);
}

int recursiveSolution(const std::vector<int> &A, size_t step, int server1Load, int server2Load) {
    size_t N = A.size();

    if (step >= N) {
        return abs(server2Load - server1Load);
    }

    int minLoadIfCurrentToS1 = recursiveSolution(A, step + 1, server1Load + A[step], server2Load);
    int minLoadIfCurrentToS2 = recursiveSolution(A, step + 1, server1Load, server2Load + A[step]);

    return std::min(minLoadIfCurrentToS1, minLoadIfCurrentToS2);
}

int solution(std::vector<int> A) {
    // Put your solution here
    int sum = 0;

    for (int elem : A) {
        sum += elem;
    }
    int goal = sum / 2;

    while (goal >= 0 && !subsetSum(A, goal)) {
        goal--;
    }
    int complement = sum - goal;
    return abs(goal - complement);
}

int main() {
    std::vector<int> input = {1, 2, 3, 4, 5};
    // std::vector<int> input = {95, 4, 60, 32, 23, 72, 80, 62, 65, 46, 55, 10, 47, 5, 4, 50, 8, 61, 85, 87};
    printf("%d\n", solution(input));
    return 0;
}

void printVector(const std::vector<int> &v) {
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++) {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}
