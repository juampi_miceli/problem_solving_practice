#include <bits/stdc++.h>

template <class T>
void printVector(const std::vector<T> &v);

int getPairsAddingTo(std::vector<int> input, int goalSum) {
    std::unordered_map<int, size_t> complementMap;  // <upToGoalSum, quantity>
    size_t N = input.size();
    int totalPairs = 0;

    for (size_t i = 0; i < N; i++) {
        int current = input[i];
        if (current >= goalSum) continue;

        if (complementMap.count(current)) {
            // I found a pair
            totalPairs++;
            if (complementMap[current] == 1) {
                complementMap.erase(current);
            } else {
                complementMap[current]--;
            }

        } else {
            // I didn't find a pair
            int complement = goalSum - current;
            if (complementMap.count(complement)) {
                complementMap[complement]++;
            } else {
                complementMap[complement] = 1;
            }
        }
    }

    return totalPairs;
}

int solution(std::vector<int> input) {
    int secondMaxNumber = INT_MIN;
    int maxNumber = INT_MIN;

    for (int elem : input) {
        if (elem > maxNumber) {
            secondMaxNumber = maxNumber;
            maxNumber = elem;
        }
    }

    int maxSum = maxNumber + secondMaxNumber;
    int minSum = 2;

    int maxPairs = -1;

    for (int i = minSum; i <= maxSum; i++) {
        int pairsAddingToI = getPairsAddingTo(input, i);
        maxPairs = std::max(maxPairs, pairsAddingToI);
    }
    return maxPairs;
}

int main() {
    std::vector<int> input1 = {1, 9, 8, 100, 2};
    std::vector<int> input2 = {2, 2, 2, 3};
    std::vector<int> input3 = {1, 2, 3, 4, 5, 6, 7, 8};

    printf("%d\n", solution(input1));
    printf("%d\n", solution(input2));
    printf("%d\n", solution(input3));
    return 0;
}

template <class T>
void printVector(const std::vector<T> &v) {
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++) {
        std::cout << v[i] << ", ";
    }
    std::cout << "]";
}
