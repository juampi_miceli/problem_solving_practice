#include <bits/stdc++.h>

template <class T>
void printVector(const std::vector<T> &v);

int solution(std::vector<int> heights) {
    size_t N = heights.size();
    std::multiset<int> minHeight;

    minHeight.insert(heights[0]);

    for (size_t i = 1; i < N; i++) {
        int currentHeight = heights[i];
        int currentMinHeight = *(minHeight.rbegin());
        if (currentHeight < currentMinHeight) {
            minHeight.erase((++minHeight.rbegin()).base());
        }
        minHeight.insert(currentHeight);
    }

    return (int)(minHeight.size());
}

int main() {
    std::vector<int> input = {5, 3, 8, 1, 2, 9, 7, 2, 6, 4, 82, 164, 489, 4651, 15};
    printf("%d\n", solution(input));
    return 0;
}

template <class T>
void printVector(const std::vector<T> &v) {
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++) {
        std::cout << v[i] << ", ";
    }
    std::cout << "]";
}
