#include <bits/stdc++.h>

typedef std::pair<int, int> guest_t;

int first(const guest_t &o) {
    return std::get<0>(o);
}

int second(const guest_t &o) {
    return std::get<1>(o);
}

int minChairs(const std::vector<int> &inTime, const std::vector<int> &outTime) {
    size_t N = inTime.size();
    std::vector<guest_t> times;

    for (size_t i = 0; i < N; i++) {
        times.push_back(std::make_pair(inTime[i], outTime[i]));
    }

    std::sort(times.begin(), times.end());

    size_t pIn = 0;
    size_t pOut = 0;
    int minChairs = 0;
    int currentChairs = 0;

    while (pIn < N && pOut < N) {
        if (first(times[pIn]) < second(times[pOut])) {
            pIn++;
            currentChairs++;
            minChairs = std::max(minChairs, currentChairs);
        } else if (first(times[pIn]) > second(times[pOut])) {
            pOut++;
            currentChairs--;
        } else {
            pIn++;
            pOut++;
        }
    }

    while (pIn < N) {
        pIn++;
        currentChairs++;
        minChairs = std::max(minChairs, currentChairs);
    }

    while (pOut < N) {
        pOut++;
        currentChairs--;
    }

    printf("min chairs: %d\n", minChairs);
    return minChairs;
}

int main() {
    minChairs({1, 2, 6, 5, 3}, {5, 5, 7, 6, 8});
    return 0;
}