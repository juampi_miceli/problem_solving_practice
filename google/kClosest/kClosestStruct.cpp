#include <bits/stdc++.h>

typedef std::vector<int> point_t;

typedef struct myPair {
    int x;
    int y;

    myPair(int _x, int _y) : x(_x), y(_y){};

    bool operator<(const myPair& o) {
        if (x < o.x) {
            return true;
        }
        if (x == o.x && y < o.y) {
            return true;
        }
        return false;
    }

    bool operator==(const myPair& o) {
        return x == o.x && y == o.y;
    }
} myPair;

size_t squaredDistanceToCenter(const myPair& p) {
    return (p.x * p.x) + (p.y * p.y);
}

std::vector<point_t> kClosest(const std::vector<std::vector<int>>& points, int k) {
    std::multimap<size_t, myPair> distanceMap;

    for (size_t i = 0; i < points.size(); i++) {
        myPair point(points[i][0], points[i][1]);
        size_t currentDistance = squaredDistanceToCenter(point);

        if (distanceMap.size() == (size_t)(k)) {
            size_t maxDistance = std::get<0>(*(distanceMap.rbegin()));
            if (currentDistance < maxDistance) {
                distanceMap.erase((++distanceMap.rbegin()).base());
            } else {
                continue;
            }
        }
        distanceMap.insert(std::make_pair(currentDistance, point));
    }

    std::vector<point_t> closestPoints;

    for (auto kv : distanceMap) {
        myPair point = std::get<1>(kv);
        closestPoints.push_back({point.x, point.y});
    }

    return closestPoints;
}

int main() {
    kClosest({{-95, 76}, {17, 7}, {-55, -58}, {53, 20}, {-69, -8}, {-57, 87}, {-2, -42}, {-10, -87}, {-36, -57}, {97, -39}, {97, 49}}, 5);
    return 0;
}
