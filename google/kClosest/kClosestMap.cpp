#include <bits/stdc++.h>

typedef std::vector<int> point_t;

size_t squaredDistanceToCenter(const point_t& p) {
    return (p[0] * p[0]) + (p[1] * p[1]);
}

std::vector<point_t> kClosest(const std::vector<std::vector<int>>& points, int k) {
    std::multimap<size_t, point_t> distanceMap;

    for (const std::vector<int>& point : points) {
        size_t currentDistance = squaredDistanceToCenter(point);

        if (distanceMap.size() == (size_t)(k)) {
            size_t maxDistance = std::get<0>(*(distanceMap.rbegin()));
            if (currentDistance < maxDistance) {
                distanceMap.erase((++distanceMap.rbegin()).base());
            } else {
                continue;
            }
        }
        distanceMap.insert(std::make_pair(currentDistance, point));
    }

    std::vector<point_t> closestPoints;

    for (auto kv : distanceMap) {
        closestPoints.push_back(std::get<1>(kv));
    }

    return closestPoints;
}

int main() {
    kClosest({{-95, 76}, {17, 7}, {-55, -58}, {53, 20}, {-69, -8}, {-57, 87}, {-2, -42}, {-10, -87}, {-36, -57}, {97, -39}, {97, 49}}, 5);
    return 0;
}
