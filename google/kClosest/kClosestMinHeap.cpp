#include <bits/stdc++.h>

typedef std::vector<int> point_t;
typedef std::pair<int, point_t> heapNode;

int squaredDistanceToCenter(const point_t& p) {
    return (p[0] * p[0]) + (p[1] * p[1]);
}

std::vector<point_t> kClosest(const std::vector<std::vector<int>>& points, int k) {
    std::priority_queue<heapNode, std::vector<heapNode>> maxHeap;

    for (const std::vector<int>& point : points) {
        int currentDistance = squaredDistanceToCenter(point);

        if (maxHeap.size() == (size_t)(k)) {
            if (currentDistance < std::get<0>(maxHeap.top())) {
                maxHeap.pop();
            } else {
                continue;
            }
        }
        maxHeap.push(heapNode(currentDistance, point));
    }

    std::vector<point_t> closestPoints;

    while (!maxHeap.empty()) {
        closestPoints.push_back(std::get<1>(maxHeap.top()));
        maxHeap.pop();
    }

    return closestPoints;
}

int main() {
    kClosest({{-95, 76}, {17, 7}, {-55, -58}, {53, 20}, {-69, -8}, {-57, 87}, {-2, -42}, {-10, -87}, {-36, -57}, {97, -39}, {97, 49}}, 5);
    return 0;
}