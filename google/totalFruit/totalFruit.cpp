#include <bits/stdc++.h>

int totalFruit(std::vector<int>& fruits) {
    size_t N = fruits.size();
    int collectedFruits = 1;
    int maxCollectedFruits = 1;
    int fruitType1 = fruits[0];
    int fruitType2 = -1;
    int reps = 1;
    for (size_t i = 1; i < N; i++) {
        int prevFruitType = fruits[i - 1];
        int currentFruitType = fruits[i];
        if (fruitType1 == currentFruitType || fruitType2 == currentFruitType) {
            collectedFruits++;
        } else if (fruitType2 == -1) {
            collectedFruits++;
            fruitType2 = currentFruitType;
        } else {
            fruitType1 = prevFruitType;
            fruitType2 = -1;
            collectedFruits = reps;
            reps = 1;
            i--;
        }
        if (prevFruitType == currentFruitType) {
            reps++;
        } else {
            reps = 1;
        }
        maxCollectedFruits = std::max(maxCollectedFruits, collectedFruits);
    }
    return maxCollectedFruits;
}