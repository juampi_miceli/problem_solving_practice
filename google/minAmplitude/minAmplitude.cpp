#include <bits/stdc++.h>

void printVector(const std::vector<int> &v);
std::vector<int> firstKMins(std::vector<int> &input, const int K);
std::vector<int> firstKMaxs(std::vector<int> &input, const int K);
int minAmplitude(std::vector<int> &input);

int minAmplitude(std::vector<int> &input) {
    const int K = 3;
    if (input.size() <= K + 1) {
        return 0;
    }

    std::vector<int> mins = firstKMins(input, K + 1);
    std::vector<int> maxs = firstKMaxs(input, K + 1);

    int winner = INT_MAX;
    for (int i = 0; i < K + 1; i++) {
        int newMin = mins[i];
        int newMax = maxs[K - i];
        winner = std::min(winner, newMax - newMin);
    }

    return winner;
}

int main() {
    std::vector<int> input = {-1, 2, -2, 8, 5, 4};
    int res = minAmplitude(input);
    printf("res: %d\n", res);
    return 0;
}

std::vector<int> firstKMins(std::vector<int> &input, const int K) {
    int N = input.size();

    for (int i = 0; i < std::min(K, N); i++) {
        int winner = INT_MAX;
        for (int j = i; j < N; j++) {
            if (input[j] < winner) {
                winner = input[j];
                std::swap(input[j], input[i]);
            }
        }
    }

    return std::vector<int>(input.begin(), input.begin() + K);
}

std::vector<int> firstKMaxs(std::vector<int> &input, const int K) {
    int N = input.size();

    for (int i = 0; i < std::min(K, N); i++) {
        int winner = INT_MIN;
        for (int j = i; j < N; j++) {
            if (input[j] > winner) {
                winner = input[j];
                std::swap(input[j], input[i]);
            }
        }
    }

    return std::vector<int>(input.begin(), input.begin() + K);
}

void printVector(const std::vector<int> &v) {
    int N = v.size();
    std::cout << "[";
    for (int i = 0; i < N; i++) {
        printf("%d, ", v[i]);
    }
    std::cout << "]";
}