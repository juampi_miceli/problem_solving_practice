#include <bits/stdc++.h>

size_t timeToType(const std::string &keyboard, const std::string &text) {
    size_t N = keyboard.size();
    size_t M = text.size();
    std::unordered_map<char, size_t> keyboardMap;
    size_t totalTime = 0;
    size_t fingerPosition = 0;

    for (size_t i = 0; i < N; i++) {
        keyboardMap[keyboard[i]] = i;
    }

    for (size_t i = 0; i < M; i++) {
        size_t nextPosition = keyboardMap[text[i]];
        totalTime += abs(nextPosition - fingerPosition);
        fingerPosition = nextPosition;
    }
    std::cout << totalTime << std::endl;
    return totalTime;
}

int main() {
    timeToType("abcdefghijklmnopqrstuvwxyz", "cba");
    return 0;
}