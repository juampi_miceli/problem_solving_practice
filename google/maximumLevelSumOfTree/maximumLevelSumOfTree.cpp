#include <bits/stdc++.h>

typedef struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
} TreeNode;

typedef std::pair<int, TreeNode *> queueNode;

int maximumLevelSumOfTree(TreeNode *root) {
    int currentLevel = 1;
    int currentSum = 0;
    int maximalSum = INT_MIN;
    int winnerLevel = 0;

    std::queue<queueNode> nextNodes;
    nextNodes.push(std::make_pair(currentLevel, root));

    while (!nextNodes.empty()) {
        queueNode currentQueueNode = nextNodes.front();
        int currentNodeLevel = std::get<0>(currentQueueNode);
        TreeNode *currentNode = std::get<1>(currentQueueNode);
        nextNodes.pop();

        if (currentNodeLevel == currentLevel) {
            currentSum += currentNode->val;
        } else {
            if (currentSum > maximalSum) {
                maximalSum = currentSum;
                winnerLevel = currentLevel;
            }
            currentLevel++;
            currentSum = currentNode->val;
        }

        if (currentNode->left != nullptr) {
            nextNodes.push(queueNode(currentNodeLevel + 1, currentNode->left));
        }
        if (currentNode->right != nullptr) {
            nextNodes.push(queueNode(currentNodeLevel + 1, currentNode->right));
        }
    }

    if (currentSum > maximalSum) {
        maximalSum = currentSum;
        winnerLevel = currentLevel;
    }

    return winnerLevel;
}

int main() {
    TreeNode *root = new TreeNode(1);
    TreeNode *l1 = new TreeNode(7);
    TreeNode *r1 = new TreeNode(0);
    TreeNode *l21 = new TreeNode(7);
    TreeNode *l22 = new TreeNode(-8);

    root->left = l1;
    root->right = r1;
    l1->left = l21;
    l1->right = l22;

    printf("%d\n", maximumLevelSumOfTree(root));

    return 0;
}